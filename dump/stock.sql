-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 08, 2018 at 02:53 PM
-- Server version: 10.2.17-MariaDB
-- PHP Version: 7.1.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `u629788952_stock`
--

-- --------------------------------------------------------

--
-- Table structure for table `t_appartement`
--

CREATE TABLE `t_appartement` (
  `id` int(11) NOT NULL,
  `nom` varchar(45) DEFAULT NULL,
  `superficie` decimal(10,2) DEFAULT NULL,
  `prix` decimal(10,2) DEFAULT NULL,
  `niveau` varchar(45) DEFAULT NULL,
  `facade` varchar(45) DEFAULT NULL,
  `nombrePiece` varchar(45) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `cave` varchar(45) DEFAULT NULL,
  `idProjet` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_appartement`
--

INSERT INTO `t_appartement` (`id`, `nom`, `superficie`, `prix`, `niveau`, `facade`, `nombrePiece`, `status`, `cave`, `idProjet`) VALUES
(5, 'AP2', '120.00', '1955000.00', 'RC', 'A1', '6', 'Oui', 'Avec', 5),
(6, 'AP3', '96.67', '750000.00', '1', 'FC2', '5', 'Oui', 'Avec', 5),
(7, 'AP23', '78.00', '560000.00', '3', 'FC11', '5', 'Oui', 'Sans', 5),
(8, 'ap22', '63.00', '550000.00', '4', 'A', '3', 'Non', 'Avec', 5);

-- --------------------------------------------------------

--
-- Table structure for table `t_bien`
--

CREATE TABLE `t_bien` (
  `id` int(11) NOT NULL,
  `numero` varchar(45) DEFAULT NULL,
  `etage` varchar(45) DEFAULT NULL,
  `superficie` decimal(10,2) DEFAULT NULL,
  `facade` varchar(45) DEFAULT NULL,
  `reserve` varchar(10) DEFAULT NULL,
  `idProjet` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_bien`
--

INSERT INTO `t_bien` (`id`, `numero`, `etage`, `superficie`, `facade`, `reserve`, `idProjet`) VALUES
(1, '1', '1', '67.00', 'RUE 30', 'FARID KADI', 1),
(2, '2', '4', '63.00', 'RUE 18', 'SAID ILHA', 1),
(3, '3', '3', '65.00', 'INTERIEUR', 'DISPONIBLE', 1),
(4, 'Bien4', '2', '85.00', '18', 'oui', 1),
(5, 'Bien5', '2', '75.00', 'A', 'oui', 1),
(6, 'Bien6', '2', '60.00', 'A', 'oui', 1),
(7, 'Bien7', '3', '72.00', 'B1', 'oui', 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_caisse_entrees`
--

CREATE TABLE `t_caisse_entrees` (
  `id` int(11) NOT NULL,
  `montant` decimal(12,2) DEFAULT NULL,
  `designation` varchar(255) DEFAULT NULL,
  `dateOperation` date DEFAULT NULL,
  `utilisateur` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_caisse_entrees`
--

INSERT INTO `t_caisse_entrees` (`id`, `montant`, `designation`, `dateOperation`, `utilisateur`) VALUES
(1, '12000.00', 'Projet Mekka Trav', '2015-02-27', ''),
(2, '12000.00', 'Projet Mekka Trav', '2015-02-27', ''),
(3, '12000.00', 'Projet Mekka Trav', '2015-02-27', 'admin'),
(4, '20000.00', '', '2015-02-28', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `t_caisse_sorties`
--

CREATE TABLE `t_caisse_sorties` (
  `id` int(11) NOT NULL,
  `montant` decimal(12,2) DEFAULT NULL,
  `designation` varchar(255) DEFAULT NULL,
  `dateOperation` date DEFAULT NULL,
  `destination` varchar(255) DEFAULT NULL,
  `utilisateur` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_caisse_sorties`
--

INSERT INTO `t_caisse_sorties` (`id`, `montant`, `designation`, `dateOperation`, `destination`, `utilisateur`) VALUES
(1, '1000.00', 'Essence', '2015-02-27', 'Bureau', 'admin'),
(2, '1000.00', 'Ciment Holcim', '2015-02-28', '5', 'admin'),
(3, '2000.00', 'Frais de d&eacute;placements', '2015-02-28', 'Bureau', 'admin'),
(4, '1000.00', '', '2015-02-28', '5', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `t_client`
--

CREATE TABLE `t_client` (
  `id` int(11) NOT NULL,
  `nom` varchar(100) DEFAULT NULL,
  `adresse` varchar(255) DEFAULT NULL,
  `telephone` varchar(45) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `created` date DEFAULT NULL,
  `code` text DEFAULT NULL,
  `numeroTva` varchar(255) DEFAULT NULL,
  `numeroRegistre` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_client`
--

INSERT INTO `t_client` (`id`, `nom`, `adresse`, `telephone`, `email`, `created`, `code`, `numeroTva`, `numeroRegistre`) VALUES
(1, 'Talal Talal', 'Rue Markech Nador', '0675010203', 'tala@gmail.com', '2014-05-17', NULL, NULL, NULL),
(2, 'Jamal Jamal', 'Rue Tokyo Nador', '0611223355', 'jama@gmail.com', '2014-05-17', NULL, NULL, NULL),
(3, 'Amin Amin', 'Rue Jibal Jerrada', '0613452211', 'amin1976@gmail.com', '2014-05-17', NULL, NULL, NULL),
(4, 'Loubna Litout', 'Rue Laayoun Oujda', '0536758491', '', '2014-05-17', NULL, NULL, NULL),
(5, 'Youssra Fikcho', 'Quartier des ouvriers Twisit', '0679456622', 'youssra.f@gmail.com', '2014-05-17', NULL, NULL, NULL),
(6, 'Deemach Deemach', 'Rue Slama Nador', '0536000001', 'deemach@gmail.com', '2014-05-17', NULL, NULL, NULL),
(7, 'SAID KAZDIR', 'Rue Toyor Oujda', '0613554422', 'touliz112@gmail.com', '2014-05-17', NULL, NULL, NULL),
(8, 'Mouaad AASSOU', 'Rue Oujda', '0536606072', 'mouaad.aassou@gmail.com', '2015-02-07', '54d65918d66ba', NULL, NULL),
(9, 'Tazi Amin', 'Rue 3 Mars Nador', '0536600666', '', '2015-02-11', '54db819620fe7', NULL, NULL),
(10, 'Mimoun Semlali', 'Rue AARID', '', '', '2015-02-12', '54dc9875ddc4f', NULL, NULL),
(11, 'Chalili Chafik', 'Rue Tiraqaa', '', '', '2015-02-12', '54dcbe3bc9d55', NULL, NULL),
(12, 'mohamed naboulsi attrari', 'RUE SALAM', NULL, '', '2015-02-17', '54e306a4ea2a7', 'lsfdlksdjf3213', '321321qsdqsdqsd'),
(16, 'Majidi Anouar', 'Rue Patrice Lumumba Rabat', '0537889921', 'majidi_comp@gmail.com', '2015-04-08', '552528cda00a120150408151037', '123PDO', '09ZERJJ'),
(14, 'Jellouli Latifa', 'Rue Salam Nador', NULL, '', '2015-02-18', '54e4f2d7165a9', '123466lll', 'lpo15428'),
(15, 'Chemlal Tarik', 'Rue Jeddah 123', NULL, '', '2015-03-24', '55114ff26da0820150324125218', 'FDJF8234R', '2349FKFKFK');

-- --------------------------------------------------------

--
-- Table structure for table `t_commande`
--

CREATE TABLE `t_commande` (
  `id` int(11) NOT NULL,
  `client` varchar(255) DEFAULT NULL,
  `dateCommande` date DEFAULT NULL,
  `produit` varchar(255) DEFAULT NULL,
  `quantite` int(11) DEFAULT NULL,
  `designation` text DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_commande`
--

INSERT INTO `t_commande` (`id`, `client`, `dateCommande`, `produit`, `quantite`, `designation`, `code`) VALUES
(12, '15', '2015-04-03', '1', NULL, NULL, '551e6303d2ba020150403115307'),
(14, '15', '2015-04-03', '2', NULL, NULL, '551e648703bb020150403115935'),
(15, '15', '2015-04-03', '2', NULL, NULL, '551e69e52d22e20150403122229'),
(16, '15', '2015-04-03', '3', NULL, NULL, '551e6a867d83a20150403122510'),
(17, '8', '2015-04-08', '2', NULL, NULL, '55251ec18a94120150408142745'),
(18, '16', '2015-05-18', '3', NULL, NULL, '5559f76d20e9620150518163005'),
(19, '16', '2015-05-18', '2', NULL, NULL, '5559fd661165820150518165534');

-- --------------------------------------------------------

--
-- Table structure for table `t_config`
--

CREATE TABLE `t_config` (
  `id` int(11) NOT NULL,
  `indexContent` int(12) DEFAULT NULL,
  `sliderType` int(12) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `createdBy` varchar(50) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `updatedBy` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_config`
--

INSERT INTO `t_config` (`id`, `indexContent`, `sliderType`, `created`, `createdBy`, `updated`, `updatedBy`) VALUES
(1, 0, 0, '2016-01-30 00:00:00', 'admin', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `t_conge_employe_projet`
--

CREATE TABLE `t_conge_employe_projet` (
  `id` int(11) NOT NULL,
  `dateDebut` date DEFAULT NULL,
  `dateFin` date DEFAULT NULL,
  `idEmploye` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_conge_employe_projet`
--

INSERT INTO `t_conge_employe_projet` (`id`, `dateDebut`, `dateFin`, `idEmploye`) VALUES
(5, '2015-02-25', '2015-02-25', 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_conge_employe_societe`
--

CREATE TABLE `t_conge_employe_societe` (
  `id` int(11) NOT NULL,
  `dateDebut` date DEFAULT NULL,
  `dateFin` date DEFAULT NULL,
  `idEmploye` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_conge_employe_societe`
--

INSERT INTO `t_conge_employe_societe` (`id`, `dateDebut`, `dateFin`, `idEmploye`) VALUES
(1, '2015-02-25', '2015-02-28', 4),
(2, '2015-01-01', '2015-01-10', 4),
(4, '2015-02-25', '2015-02-26', 3),
(5, '2015-02-25', '2015-03-25', 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_contrat`
--

CREATE TABLE `t_contrat` (
  `id` int(11) NOT NULL,
  `dateCreation` date DEFAULT NULL,
  `prixVente` decimal(12,2) DEFAULT NULL,
  `avance` decimal(12,2) DEFAULT NULL,
  `modePaiement` varchar(255) DEFAULT NULL,
  `dureePaiement` int(11) DEFAULT NULL,
  `echeance` decimal(12,2) DEFAULT NULL,
  `note` text DEFAULT NULL,
  `idClient` int(11) DEFAULT NULL,
  `idProjet` int(11) DEFAULT NULL,
  `idBien` int(11) DEFAULT NULL,
  `typeBien` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_contrat`
--

INSERT INTO `t_contrat` (`id`, `dateCreation`, `prixVente`, `avance`, `modePaiement`, `dureePaiement`, `echeance`, `note`, `idClient`, `idProjet`, `idBien`, `typeBien`, `code`, `status`) VALUES
(1, '2014-05-17', '650000.00', '12000.00', NULL, NULL, '20140617.00', NULL, 7, 1, 1, NULL, NULL, 'actif'),
(2, '2014-05-17', '650000.00', NULL, NULL, NULL, '20140617.00', NULL, 7, 1, 2, NULL, NULL, 'actif'),
(3, '2014-05-17', '750000.00', NULL, NULL, NULL, '20140617.00', NULL, 6, 1, 3, NULL, NULL, 'actif'),
(4, '2014-05-17', '550000.00', NULL, NULL, NULL, '20140617.00', NULL, 5, 1, 4, NULL, NULL, 'actif'),
(5, '2014-05-17', '550000.00', NULL, NULL, NULL, '20140625.00', NULL, 4, 1, 5, NULL, NULL, 'actif'),
(6, '2014-05-07', '3500000.00', NULL, NULL, NULL, '0.00', NULL, 7, 1, 7, NULL, NULL, 'actif'),
(7, '2014-05-01', '3500000.00', '200000.00', NULL, NULL, '20140508.00', NULL, 6, 1, 6, NULL, NULL, 'actif'),
(8, '2015-02-10', '110000.00', '17000.00', 'Esp&egrave;ces', 24, '3875.00', 'Ajouter une fen&ecirc;tre 2 portes', 8, 5, 2, 'localCommercial', '54d9f1fd8ff9e', 'actif'),
(11, '2015-02-17', '300000.00', '150000.00', '', 0, '0.00', 'Utiliser la peinture rose pour la chambre des enfants+ Changer le sanitaire', 12, 5, 5, 'appartement', '54e307721ebb6', 'annulle'),
(12, '2015-02-18', '1200000.00', '400000.00', 'Versement', 24, '33333.00', 'Ajouter une 2&eacute;me porte', 13, 3, 4, 'localCommercial', '54e4bf0b5522d', 'annulle'),
(13, '2015-02-18', '750000.00', '120000.00', 'Esp&egrave;ces', 24, '26250.00', 'Rien', 13, 5, 6, 'appartement', '54e4eed7aae7a', 'actif'),
(14, '2015-02-18', '118000.00', '90000.00', 'Versement', 36, '778.00', 'Rien de plus', 14, 5, 3, 'localCommercial', '54e4f30409cc4', 'annulle'),
(15, '2015-03-07', '1880000.00', '20000.00', 'Ch&egrave;que', 12, '155000.00', 'Diviser la pi&egrave;ce 1 en 2', 9, 5, 5, 'appartement', '54fae7fb0397c', 'actif'),
(16, '2015-03-07', '110000.00', '12000.00', 'Esp&egrave;ces', 24, '4083.00', '', 12, 5, 3, 'localCommercial', '54fae84507cce', 'actif'),
(17, '2015-03-11', '730000.00', '300000.00', 'Esp&egrave;ces', 12, '35833.00', '', 9, 5, 6, 'appartement', '5500209c43535', 'actif');

-- --------------------------------------------------------

--
-- Table structure for table `t_employe_projet`
--

CREATE TABLE `t_employe_projet` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `cin` varchar(100) DEFAULT NULL,
  `photo` text DEFAULT NULL,
  `telephone` varchar(45) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `etatCivile` varchar(45) DEFAULT NULL,
  `dateDebut` date DEFAULT NULL,
  `dateSortie` date DEFAULT NULL,
  `idProjet` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_employe_projet`
--

INSERT INTO `t_employe_projet` (`id`, `nom`, `cin`, `photo`, `telephone`, `email`, `etatCivile`, `dateDebut`, `dateSortie`, `idProjet`) VALUES
(1, 'Adghouch Mohamedi', 'S232009', '', '0666098763', '', 'Mari&eacute;', '2014-09-17', '2015-02-05', 5),
(2, 'Touhtouh Jalal', 'S90425', '', '', '', 'C&eacute;libataire', '2015-02-25', '0000-00-00', 5),
(3, 'TouhtouJalal', '', '', '', '', 'C&eacute;libataire', '2015-03-11', '0000-00-00', 5);

-- --------------------------------------------------------

--
-- Table structure for table `t_employe_societe`
--

CREATE TABLE `t_employe_societe` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `cin` varchar(100) DEFAULT NULL,
  `photo` text DEFAULT NULL,
  `telephone` varchar(45) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `etatCivile` varchar(45) DEFAULT NULL,
  `dateDebut` date DEFAULT NULL,
  `dateSortie` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_employe_societe`
--

INSERT INTO `t_employe_societe` (`id`, `nom`, `cin`, `photo`, `telephone`, `email`, `etatCivile`, `dateDebut`, `dateSortie`) VALUES
(1, 'Adghouch Mohamed', 'O9809', '', '', '', 'Mari&eacute;', '2014-09-17', '2015-02-05'),
(2, 'Amhemad Abdelhak', 'D345654', '../MerlaTravERP/photo_employes_societe/54eca86f21b8bdrmahmoud.jpg', '0613234456', 'amhemad2@gmail.com', 'Mari&eacute;', '2015-02-24', '2015-02-24'),
(3, 'Tarik Meddah', 'F90867', '../MerlaTravERP/photo_employes_societe/54ec8c90dd354photo1.jpg', '0624657849', 't.meddah@gmail.com', 'Mari&eacute;', '2015-02-24', '0000-00-00'),
(4, 'AASSOU Abdelilah', 'S234432', '../MerlaTravERP/photo_employes_societe/54eca4ab1d4b0me.jpg', '0613234456', 'aassou.abdelilah@gmail.com', 'C&eacute;libataire', '2015-02-24', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `t_fournisseur`
--

CREATE TABLE `t_fournisseur` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `adresse` varchar(255) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `telephone1` varchar(45) DEFAULT NULL,
  `telephone2` varchar(45) DEFAULT NULL,
  `fax` varchar(45) DEFAULT NULL,
  `dateCreation` date DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_fournisseur`
--

INSERT INTO `t_fournisseur` (`id`, `nom`, `adresse`, `email`, `telephone1`, `telephone2`, `fax`, `dateCreation`, `code`) VALUES
(19, 'Soci&eacute;t&eacute; Chellah', 'Rue Chamal', '', '0536606069', '0674883399', '', '2015-03-15', '5505a52746e88'),
(2, 'Chellaho Miloudi', 'Rue Casa Nador', 'chellaho@gmail.com', '0613131313', '0654216532', '0536662211', '2014-05-17', NULL),
(3, 'St&Atilde;&copy; Trawri', 'Rue El Amal Nador', 'trawri.ste@gmail.com', '0615425511', '0678451255', '', '2014-05-17', NULL),
(4, 'Miksomik SARL', 'Rue Silaour Oujda', 'miksomik@hotmail.com', '0536456789', '0536794613', '05366001254', '2014-05-17', NULL),
(5, 'Soci&eacute;t&eacute; Sotrajan', 'Rue Taouima', 'sotrajan@gmail.com', '0536600078', '0623990042', '0536330900', '2015-02-10', '54e2200025812'),
(16, 'Mohamadi Abdellah', 'Appt 20 Imm 730 Rue Fellah Oujda', 'm.bd@gmail.com', '9999999999999', '0654216532', '05366300000', '2015-03-14', '55042088a5ac2'),
(7, 'Soci&eacute;t&eacute; Afak', 'Rue Sakia Al Hamra', 'afak-distrubtion@gmail.com', '0536609870', '', '', '2015-02-19', '54e5c2a39219f'),
(8, 'Soci&eacute;t&eacute; Briconad', 'Rue Laari Chikh', '', '0536609980', '', '', '2015-03-02', '54f447429a2ae'),
(9, 'Soci&eacute;t&eacute; JimilCo', 'Rue Salam Nador', '', '0536600098', '', '', '2015-03-02', '54f44ea0c26c1'),
(10, 'mimoun chattou', '', '', '9999999999999', '88888888', '', '2015-03-09', '54fd66ec03d13'),
(15, 'Amezian Soufiane', 'Hay Jbal Sidi Maafa Oujda', 'aassou.abdelilah@gmail.com', '0613064330', '0611223355', '05366300000', '2015-03-14', '55041f308dd31'),
(20, 'so com', 'aaa', '', '087777777', '097765555', '', '2015-03-15', '5505b804aa3d8');

-- --------------------------------------------------------

--
-- Table structure for table `t_image`
--

CREATE TABLE `t_image` (
  `id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `url` text DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `idProjet` int(12) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `createdBy` varchar(50) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `updatedBy` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_image`
--

INSERT INTO `t_image` (`id`, `name`, `url`, `description`, `idProjet`, `created`, `createdBy`, `updated`, `updatedBy`) VALUES
(65, 'Fa&ccedil;ade', '/images/projects/58c972e4e319fCHOP-8.jpg', 'Fa&ccedil;ade 1 projet Annahda 1', 1, '2017-03-15 04:59:16', 'admin', NULL, NULL),
(66, 'SAlon', '/images/projects/58c9730da7d85IMG-20160510-WA0063-1024x767.jpg', 'Salon Marocain projet Annahda 1', 1, '2017-03-15 04:59:57', 'admin', NULL, NULL),
(67, 'Couloir', '/images/projects/58c973823f342IMG-20160510-WA0067-767x1024.jpg', 'Couloir  Projet Annahda', 1, '2017-03-15 05:01:54', 'admin', NULL, NULL),
(68, 'Cuisine ', '/images/projects/58c973a2408a6CHOP-454f083180cd3e.jpg', 'Cuisine Projet Annahda ', 1, '2017-03-15 05:02:26', 'admin', NULL, NULL),
(69, 'SAlon 2', '/images/projects/58c973f105dbaCHOP-554f083177af8e.jpg', 'Salon 2 Projet Annahda', 1, '2017-03-15 05:03:45', 'admin', NULL, NULL),
(70, 'Couloir 2', '/images/projects/58c974986de03IMG-20160510-WA0071-1024x767.jpg', 'Couloir 2 Projet Annahda', 1, '2017-03-15 05:06:32', 'admin', NULL, NULL),
(94, 'Couloir', '/images/projects/58c9b2e33eb7120170205_103713-576x1024.jpg', 'Couloir Projet Khattabi', 4, '2017-03-15 09:32:19', 'admin', NULL, NULL),
(93, 'toilette', '/images/projects/58c9b2c6324eb20160507_155506-768x1024.jpg', 'Toilette Projet Khattabi', 4, '2017-03-15 09:31:50', 'admin', NULL, NULL),
(92, 'Cuisine 1', '/images/projects/58c9b2a139be1IMG-20170130-WA0041-1024x768.jpg', 'Cuisine 1 Projet Khattabi', 4, '2017-03-15 09:31:13', 'admin', NULL, NULL),
(91, 'Salon 2', '/images/projects/58c9b265c6652IMG-20161205-WA0042-1024x616.jpg', 'Salon 2 Projet Khattabi', 4, '2017-03-15 09:30:13', 'admin', NULL, NULL),
(90, 'Salon 1', '/images/projects/58c9b23626388IMG-20161205-WA0046-1024x616.jpg', 'Salon 1 Projet Khattabi', 4, '2017-03-15 09:29:26', 'admin', NULL, NULL),
(89, 'Cuisine 1', '/images/projects/58c9b20fdce83IMG-20161205-WA0042-1024x616.jpg', 'Cuisine 1 Projet Khattabi', 4, '2017-03-15 09:28:47', 'admin', NULL, NULL),
(88, 'Porte Pricipale', '/images/projects/58c9a80082b2020170205_103417-576x1024.jpg', 'Porte Principale Projet Khattabi', 4, '2017-03-15 08:45:52', 'admin', NULL, NULL),
(87, 'Fa&ccedil;ade 2', '/images/projects/58c9a7988422dIMG-20161205-WA0020-1024x614.jpg', 'Fa&ccedil;ade 2 Projet Khattabi', 4, '2017-03-15 08:44:08', 'admin', NULL, NULL),
(86, 'Fa&ccedil;ade 1', '/images/projects/58c9a497c1c1aIMG-20161205-WA0063.jpg', 'Fa&ccedil;ade Projet Khattabi', 4, '2017-03-15 08:31:19', 'admin', NULL, NULL),
(85, 'Salon', '/images/projects/58c9a3bd811fdchop-20-3.jpg', 'Salon Projet Rafah', 3, '2017-03-15 08:27:41', 'admin', NULL, NULL),
(84, 'Couloir', '/images/projects/58c9a3991198echop-19-768x1024.jpg', 'Couloir Projet Rafah', 3, '2017-03-15 08:27:05', 'admin', NULL, NULL),
(83, 'Toilettes', '/images/projects/58c9a334a22c7IMG-20160502-WA0017-576x1024.jpg', 'Toilettes Projet Rafah', 3, '2017-03-15 08:25:24', 'admin', NULL, NULL),
(82, 'Cuisine 2', '/images/projects/58c9a3117e7a1IMG-20160524-WA0025-1024x576.jpg', 'Cuisine 2 Projet Rafah', 3, '2017-03-15 08:24:49', 'admin', NULL, NULL),
(81, 'Cuisine ', '/images/projects/58c9a2f3f2ac7IMG-20160615-WA0218.jpg', 'Cuisine 1 Projet Rafah', 3, '2017-03-15 08:24:19', 'admin', NULL, NULL),
(80, 'Porte principale', '/images/projects/58c9a2c1a9bf6IMG-20161018-WA0010-1024x768.jpg', 'Porte principale Projet Rafah', 3, '2017-03-15 08:23:29', 'admin', NULL, NULL),
(79, 'Fa&ccedil;ade', '/images/projects/58c9a1a2f041fIMG-20160803-WA0009.png', 'Fa&ccedil;ade Projet Rafah', 3, '2017-03-15 08:18:42', 'admin', NULL, NULL),
(78, 'Cuisine', '/images/projects/58c9798894187chop-10.jpg', 'Cuisine Projet Gaza', 2, '2017-03-15 05:27:36', 'admin', NULL, NULL),
(77, 'Salon ', '/images/projects/58c9795079560IMG-20170110-WA0004-768x1024.jpg', 'Salon Projet Gaza', 2, '2017-03-15 05:26:40', 'admin', NULL, NULL),
(76, 'Couloir', '/images/projects/58c9792b98970chop-13 (1).jpg', 'Couloir Projet Gaza', 2, '2017-03-15 05:26:03', 'admin', NULL, NULL),
(75, 'Escaliers 2', '/images/projects/58c978c4b175bchop-9.jpg', 'Escaliers 2 Projet Gaza', 2, '2017-03-15 05:24:20', 'admin', NULL, NULL),
(74, 'Escaliers', '/images/projects/58c9786b4ac3achop-16.jpg', 'Escaliers Projet Gaza', 2, '2017-03-15 05:22:51', 'admin', NULL, NULL),
(73, 'Porte ', '/images/projects/58c9782ce1ebfchop-1-2-1-768x1024.jpg', 'Porte principale Projet Gaza', 2, '2017-03-15 05:21:48', 'admin', NULL, NULL),
(71, 'Chambre Enfant', '/images/projects/58c974c76edcdCHOP-1354f08318dbf31.jpg', 'Chambre des Enfants  Projet Annahda', 1, '2017-03-15 05:07:19', 'admin', NULL, NULL),
(72, 'Fa&ccedil;ade', '/images/projects/58c977c30d56bchop-13.jpg', 'Fa&ccedil;ade Projet Gaza', 2, '2017-03-15 05:20:03', 'admin', NULL, NULL),
(95, 'Cuisine 2', '/images/projects/58c9b303ef18fIMG-20170110-WA0018-1024x576.jpg', 'Cuisine 2 Projet Khattabi ', 4, '2017-03-15 09:32:51', 'admin', NULL, NULL),
(96, 'Fa&ccedil;ade', '/images/projects/58c9baddf2394IMG-20160329-WA0005.jpg', 'Fa&ccedil;ade Projet Kamar', 5, '2017-03-15 10:06:21', 'admin', NULL, NULL),
(97, 'couloir', '/images/projects/58c9bb1a0d5c8IMG-20160426-WA0076-1024x768.jpg', 'Couloir Projet Kamar', 5, '2017-03-15 10:07:22', 'admin', NULL, NULL),
(98, 'Salon', '/images/projects/58c9bc5eefdc1IMG-20160506-WA0062-1024x768.jpg', 'Salon Projet Kamar', 5, '2017-03-15 10:12:46', 'admin', NULL, NULL),
(99, 'Portes', '/images/projects/58c9bcd6836c4IMG-20160426-WA0077-768x1024.jpg', 'Portes Projet Kamar', 5, '2017-03-15 10:14:46', 'admin', NULL, NULL),
(100, 'Chambre', '/images/projects/58c9bde7cc016chop-4-5-4.jpg', 'Chambre Projet Kamar', 5, '2017-03-15 10:19:19', 'admin', NULL, NULL),
(101, 'Fa&ccedil;ade ', '/images/projects/58c9c0cdd3afechop-6-2.png', 'Fa&ccedil;ade Projet Ahlam', 6, '2017-03-15 10:31:41', 'admin', NULL, NULL),
(102, 'Porte ', '/images/projects/58c9c0f21b9d3chop-1-6-1.jpg', 'Porte Projet Ahlam', 6, '2017-03-15 10:32:18', 'admin', NULL, NULL),
(103, 'Escaliers', '/images/projects/58c9c1a43d6eachop-3-6-768x1024.jpg', 'Escaliers Projet Ahlam', 6, '2017-03-15 10:35:16', 'admin', NULL, NULL),
(104, 'Chambre', '/images/projects/58c9c1c6462a7chop-3655dd8b8b309f0.jpg', 'Chambre Projet Ahlam', 6, '2017-03-15 10:35:50', 'admin', NULL, NULL),
(105, 'Couloir', '/images/projects/58c9c1e898cf2chop-2655d1b8581ee58.jpg', 'Couloir Projet Ahlam', 6, '2017-03-15 10:36:24', 'admin', NULL, NULL),
(106, 'Cuisine', '/images/projects/58c9c213b1d58chop-6-13.jpg', 'Cuisine Projet Ahlam', 6, '2017-03-15 10:37:07', 'admin', NULL, NULL),
(107, 'Fa&ccedil;ade', '/images/projects/58c9c3930426eIMG-20161011-WA0005.png', 'Fa&ccedil;ade Projet Riham', 7, '2017-03-15 10:43:31', 'admin', NULL, NULL),
(108, 'Porte ', '/images/projects/58c9c41f971d5IMG-20170218-WA0047-616x1024.jpg', 'Porte Projet Riham', 7, '2017-03-15 10:45:51', 'admin', NULL, NULL),
(109, 'Cuisine 1', '/images/projects/58c9c44399896IMG-20160502-WA0013.jpg', 'Cuisine 1 Projet Riham', 7, '2017-03-15 10:46:27', 'admin', NULL, NULL),
(110, 'Cuisine 2', '/images/projects/58c9c45ca49b1IMG-20160524-WA0025-1024x576.jpg', 'Cuisine 2 Projet Riham', 7, '2017-03-15 10:46:52', 'admin', NULL, NULL),
(111, 'Cuisine 3', '/images/projects/58c9c472aedcdIMG-20160502-WA0014-1024x576.jpg', 'Cuisine 3 Projet Riham', 7, '2017-03-15 10:47:14', 'admin', NULL, NULL),
(112, 'Salon', '/images/projects/58c9c5551068aIMG-20161017-WA0095-1024x768.jpg', 'Salon Projet Riham', 7, '2017-03-15 10:51:01', 'admin', NULL, NULL),
(113, 'Fa&ccedil;ade 1', '/images/projects/58c9c8f207b6cIMG-20160715-WA0025.png', 'Fa&ccedil;ade 1 Projet Sajed', 8, '2017-03-15 11:06:26', 'admin', NULL, NULL),
(114, 'Fa&ccedil;ade 2', '/images/projects/58c9c990bfc00IMG-20160630-WA0062-576x1024.jpg', 'Fa&ccedil;ade 2 Projet Sajed', 8, '2017-03-15 11:09:04', 'admin', NULL, NULL),
(115, 'Fa&ccedil;ade 3', '/images/projects/58c9c9b7a2ecfIMG-20160628-WA0157-1024x576.jpg', 'Fa&ccedil;ade 3 Projet Sajed', 8, '2017-03-15 11:09:43', 'admin', NULL, NULL),
(116, 'Salon', '/images/projects/58c9c9ef03e87IMG-20160419-WA0024-1-1024x767.jpg', 'Salon Projet Sajed', 8, '2017-03-15 11:10:39', 'admin', NULL, NULL),
(117, 'Chambre', '/images/projects/58c9ca0b24ffdIMG-20160419-WA0026-1024x767.jpg', 'Chambre Projet Sajed', 8, '2017-03-15 11:11:07', 'admin', NULL, NULL),
(118, 'Balcon', '/images/projects/58c9ca24ee7bbIMG-20160415-WA0044-767x1024.jpg', 'Balcon Projet Sajed', 8, '2017-03-15 11:11:32', 'admin', NULL, NULL),
(119, 'Fa&ccedil;ade', '/images/projects/58c9cc4ade757IMG-20161011-WA0011.png', 'Fa&ccedil;ade Projet Ghofran', 9, '2017-03-15 11:20:42', 'admin', NULL, NULL),
(120, 'Cuisine', '/images/projects/58c9cd7ccb119IMG-20160615-WA0218.jpg', 'Cuisine Projet Ghofran', 9, '2017-03-15 11:25:48', 'admin', NULL, NULL),
(121, 'Salon 1', '/images/projects/58c9cd9ebe290IMG-20161006-WA0007-1024x614.jpg', 'Salon 1 Projet Ghofran', 9, '2017-03-15 11:26:22', 'admin', NULL, NULL),
(122, 'Salon 2', '/images/projects/58c9cdb5d2e0eIMG-20161006-WA0008-1024x614.jpg', 'Salon 2 Projet Ghofran', 9, '2017-03-15 11:26:45', 'admin', NULL, NULL),
(123, 'Salon 3', '/images/projects/58c9cdd3cf6dfIMG-20161006-WA0009-1024x614.jpg', 'Salon 3 Projet Ghofran', 9, '2017-03-15 11:27:15', 'admin', NULL, NULL),
(124, 'Fa&ccedil;ade', '/images/projects/58c9cf905482a20170304_140600.png', 'Fa&ccedil;ade Projet Badr', 10, '2017-03-15 11:34:40', 'admin', NULL, NULL),
(125, 'Porte', '/images/projects/58c9cfdd7f95620170105_080428-768x1024.jpg', 'Porte Projet Badr', 10, '2017-03-15 11:35:57', 'admin', NULL, NULL),
(126, 'Escaliers 1', '/images/projects/58c9d052cce2020170304_134058-576x1024.jpg', 'Escaliers 1 Projet Badr', 10, '2017-03-15 11:37:54', 'admin', NULL, NULL),
(127, 'Escaliers 2', '/images/projects/58c9d06bf10d320170304_134045-576x1024.jpg', 'Escaliers 2 Projet Badr', 10, '2017-03-15 11:38:19', 'admin', NULL, NULL),
(128, 'Couloir', '/images/projects/58c9d0c0f0b8220170304_134147-1024x576.jpg', 'Couloir Projet Badr', 10, '2017-03-15 11:39:44', 'admin', NULL, NULL),
(129, 'Couloir 2', '/images/projects/58c9d0dddcf2615781347_1810951749161239_656093389353128342_n-1.jpg', 'Couloir 2 Projet Badr', 10, '2017-03-15 11:40:13', 'admin', NULL, NULL),
(130, 'Salon 1', '/images/projects/58c9d0f84837915781641_1810951705827910_3365561680787064724_n-1.jpg', 'Salon 1 Projet Badr', 10, '2017-03-15 11:40:40', 'admin', NULL, NULL),
(131, 'Salon 2', '/images/projects/58c9d10acdb6d15781683_1810952109161203_5275293170351699066_n-1.jpg', 'Salon 2 Projet Badr', 10, '2017-03-15 11:40:58', 'admin', NULL, NULL),
(132, 'Salon 3', '/images/projects/58c9d124620a815825866_1810951732494574_3046192416678380317_n-1.jpg', 'Salon 3 Projet Badr', 10, '2017-03-15 11:41:24', 'admin', NULL, NULL),
(133, 'Cuisine', '/images/projects/58c9d1414ee4515823201_1810951599161254_757351236605699025_n-1.jpg', 'Cuisine Projet Badr', 10, '2017-03-15 11:41:53', 'admin', NULL, NULL),
(134, 'Toilette', '/images/projects/58c9d15c0558a15871576_1810951989161215_5230860452962666546_n-1.jpg', 'Toilette Projet Badr', 10, '2017-03-15 11:42:20', 'admin', NULL, NULL),
(135, 'Balcon', '/images/projects/58c9d178edda8IMG-20170215-WA0115-1024x576.jpg', 'Balcon Projet Badr', 10, '2017-03-15 11:42:48', 'admin', NULL, NULL),
(136, 'Fa&ccedil;ade', '/images/projects/58c9d28d848f220170304_132417.jpg', 'Fa&ccedil;ade Projet Al Aksa', 11, '2017-03-15 11:47:25', 'admin', NULL, NULL),
(137, 'Porte', '/images/projects/58c9d37304dd0IMG-20170218-WA0048-616x1024.jpg', 'Porte Projet Al Aksa', 11, '2017-03-15 11:51:15', 'admin', NULL, NULL),
(138, 'Chambre', '/images/projects/58c9d38e0823320161119_111131-768x1024.jpg', 'Chambre Projet Al Aksa', 11, '2017-03-15 11:51:42', 'admin', NULL, NULL),
(139, 'Chambre 2', '/images/projects/58c9d3a55570d20161119_112350-768x1024.jpg', 'Chambre 2 Projet Al Aksa', 11, '2017-03-15 11:52:05', 'admin', NULL, NULL),
(140, 'Toilette', '/images/projects/58c9d3c61032920170304_132900-1024x576.jpg', 'Toilette Projet Al Aksa', 11, '2017-03-15 11:52:38', 'admin', NULL, NULL),
(141, 'Fa&ccedil;ade 3D', '/images/projects/58c9d616cbf6cSans-titre-2.jpg', 'Fa&ccedil;ade 3D Projet Lina', 12, '2017-03-16 12:02:30', 'admin', NULL, NULL),
(142, 'Fa&ccedil;ade 2', '/images/projects/58c9d7043102cIMG-20160715-WA0022-1024x767.jpg', 'Fa&ccedil;ade 2 Projet Al Aksa', 12, '2017-03-16 12:06:28', 'admin', NULL, NULL),
(143, 'Salon', '/images/projects/58c9d7bb449ecIMG-20170203-WA0002-1024x576.jpg', 'Salon Projet Al Aksa', 12, '2017-03-16 12:09:31', 'admin', NULL, NULL),
(144, 'Salon', '/images/projects/58c9d7cf54925IMG-20170207-WA0050-576x1024.jpg', 'Salon Projet Al Aksa', 12, '2017-03-16 12:09:51', 'admin', NULL, NULL),
(145, 'Carrelage', '/images/projects/58c9d7f3c2785IMG-20170204-WA0082-576x1024.jpg', 'Carrelage Projet Al Aksa', 12, '2017-03-16 12:10:27', 'admin', NULL, NULL),
(146, 'Fa&ccedil;ade', '/images/projects/58c9da230acc2IMG-20160318-WA0031-1024x819.jpg', 'Fa&ccedil;ade 1 Projet Haj Najib', 13, '2017-03-16 12:19:47', 'admin', NULL, NULL),
(147, '3D 1', '/images/projects/58c9da4f5a788apart-1-4-1024x683.jpg', '3D 1 Projet Haj Najib', 13, '2017-03-16 12:20:31', 'admin', NULL, NULL),
(148, '3D 2', '/images/projects/58c9da6bae187IMG-20170213-WA0019-1024x575.jpg', '3D 2 Projet Haj Najib', 13, '2017-03-16 12:20:59', 'admin', NULL, NULL),
(149, '3D 3', '/images/projects/58c9da875477bapart-1-1024x683.jpg', '3D 3 Projet Haj Najib', 13, '2017-03-16 12:21:27', 'admin', NULL, NULL),
(150, 'Chantier', '/images/projects/58c9daa76b2dbIMG-20170213-WA0022-1024x575.jpg', 'Chantier 1 Projet Haj Najib', 13, '2017-03-16 12:21:59', 'admin', NULL, NULL),
(151, '3D 3', '/images/projects/58c9dad05e1e3apart-2-1024x683.jpg', '3D 3 Projet Haj Najib', 13, '2017-03-16 12:22:40', 'admin', NULL, NULL),
(152, 'Chantier 3', '/images/projects/58c9daf4d9248IMG-20170214-WA0100-1024x616.jpg', 'Chantier 3 Projet Haj Najib', 13, '2017-03-16 12:23:16', 'admin', NULL, NULL),
(153, '3D 4', '/images/projects/58c9db24e8a6bapart-3-1024x683.jpg', '3D 4 Projet Haj Najib', 13, '2017-03-16 12:24:04', 'admin', NULL, NULL),
(154, 'Chantier 4', '/images/projects/58c9db51620c8IMG-20170228-WA0018-1024x575.jpg', 'Chantier 4 Projet Haj Najib', 13, '2017-03-16 12:24:49', 'admin', NULL, NULL),
(155, '3D 5', '/images/projects/58c9db9a3a011apart-4-1024x683.jpg', '3D 5 Projet Haj Najib', 13, '2017-03-16 12:26:02', 'admin', NULL, NULL),
(156, 'Chantier 6', '/images/projects/58c9dbc421ad1IMG-20170228-WA0018-1024x575.jpg', 'Chantier 6 Projet Haj Najib', 13, '2017-03-16 12:26:44', 'admin', NULL, NULL),
(157, '3D 6', '/images/projects/58c9dbe47796aapart-5-1024x683.jpg', '3D 6 Projet Haj Najib', 13, '2017-03-16 12:27:16', 'admin', NULL, NULL),
(158, 'Fa&ccedil;ade 1', '/images/projects/58c9dc2fa6fa3Sans-titre-1.png', 'Fa&ccedil;ade 1 Projet Jasmin', 14, '2017-03-16 12:28:31', 'admin', NULL, NULL),
(159, '3D 1', '/images/projects/58c9dc5c9d66e006-1024x725.jpg', '3D 1 Projet Jasmin', 14, '2017-03-16 12:29:16', 'admin', NULL, NULL),
(160, 'Chantier 1', '/images/projects/58c9df5cf253320170304_140429-576x1024.jpg', 'Chantier 1 Projet Jasmin', 14, '2017-03-16 12:42:04', 'admin', NULL, NULL),
(161, '3D 2', '/images/projects/58c9dfb5c1159007-1024x725.jpg', '3D 2 Projet Jasmin', 14, '2017-03-16 12:43:33', 'admin', NULL, NULL),
(162, 'Chantier 2', '/images/projects/58c9dfd96e85520170304_140440-1024x576.jpg', 'Chantier 2 Projet Jasmin', 14, '2017-03-16 12:44:09', 'admin', NULL, NULL),
(163, '3D 3', '/images/projects/58c9e00792fd4008-1024x725.jpg', '3D 3 Projet Jasmin', 14, '2017-03-16 12:44:55', 'admin', NULL, NULL),
(164, 'Chantier 4', '/images/projects/58c9e0352fab8IMG-20170303-WA0005-1024x575.jpg', 'Chantier 4 Projet Jasmin', 14, '2017-03-16 12:45:41', 'admin', NULL, NULL),
(165, '3D 4', '/images/projects/58c9e053b10cb009-1024x725.jpg', '3D 4 Projet Jasmin', 14, '2017-03-16 12:46:11', 'admin', NULL, NULL),
(170, 'Annahda14_1', '/images/projects/Annahda14_1.jpg', 'Annahda 14_1', 15, '2018-01-15 13:44:36', 'admin', NULL, NULL),
(167, 'Annahda15_1', '/images/projects/Annahda15_1.jpg', 'Annahda 15_1', 16, '2018-01-15 13:29:45', 'admin', NULL, NULL),
(168, 'Annahda15_2', '/images/projects/Annahda15_2.jpg', 'Annahda 15_2', 16, '2018-01-15 13:30:42', 'admin', NULL, NULL),
(169, 'Annahda15_3', '/images/projects/Annahda15_3.jpg', 'Annahda 15_3', 16, '2018-01-15 13:30:42', 'admin', NULL, NULL),
(171, 'Annahda14_2', '/images/projects/Annahda14_2.jpg', 'Annahda 14_2', 15, '2018-01-15 13:44:36', 'admin', NULL, NULL),
(172, 'Annahda14_3', '/images/projects/Annahda14_3.jpg', 'Annahda 14_3', 15, '2018-01-15 13:44:36', 'admin', NULL, NULL),
(173, 'Annahda14_4', '/images/projects/Annahda14_4.jpg', 'Annahda 14_4', 15, '2018-01-15 13:44:36', 'admin', NULL, NULL),
(174, 'Annahda14_5', '/images/projects/Annahda14_5.jpg', 'Annahda 14_5', 15, '2018-01-15 13:44:36', 'admin', NULL, NULL),
(175, 'Annahda14_6', '/images/projects/Annahda14_6.jpg', 'Annahda 14_6', 15, '2018-01-15 13:44:36', 'admin', NULL, NULL),
(176, 'Annahda14_7', '/images/projects/Annahda14_7.jpg', 'Annahda 14_7', 15, '2018-01-15 13:44:36', 'admin', NULL, NULL),
(177, 'Annahda14_8', '/images/projects/Annahda14_8.jpg', 'Annahda 14_8', 15, '2018-01-15 13:44:36', 'admin', NULL, NULL),
(178, 'Annahda14_9', '/images/projects/Annahda14_9.jpg', 'Annahda 14_9', 15, '2018-01-15 13:44:36', 'admin', NULL, NULL),
(179, 'Annahda14_10', '/images/projects/Annahda14_10.jpg', 'Annahda 14_10', 15, '2018-01-15 13:44:36', 'admin', NULL, NULL),
(180, 'Annahda13_1', '/images/projects/Annahda13_1.jpg', 'Annahda 13_1', 14, '2018-01-15 13:48:05', 'admin', NULL, NULL),
(181, 'Annahda13_2', '/images/projects/Annahda13_2.jpg', 'Annahda 13_2', 14, '2018-01-15 13:48:05', 'admin', NULL, NULL),
(182, 'Annahda13_3', '/images/projects/Annahda13_3.jpg', 'Annahda 13_3', 14, '2018-01-15 13:48:05', 'admin', NULL, NULL),
(183, 'Annahda13_4', '/images/projects/Annahda13_4.jpg', 'Annahda 13_4', 14, '2018-01-15 13:48:05', 'admin', NULL, NULL),
(184, 'Annahda13_5', '/images/projects/Annahda13_5.jpg', 'Annahda 13_5', 14, '2018-01-15 13:48:05', 'admin', NULL, NULL),
(185, 'Annahda13_6', '/images/projects/Annahda13_6.jpg', 'Annahda 13_6', 14, '2018-01-15 13:48:05', 'admin', NULL, NULL),
(186, 'Annahda13_7', '/images/projects/Annahda13_7.jpg', 'Annahda 13_7', 14, '2018-01-15 13:48:05', 'admin', NULL, NULL),
(187, 'Annahda13_8', '/images/projects/Annahda13_8.jpg', 'Annahda 13_8', 14, '2018-01-15 13:48:05', 'admin', NULL, NULL),
(188, 'Annahda12_1', '/images/projects/Annahda12_1.jpg', 'Annahda 12_1', 13, '2018-01-15 14:25:48', 'admin', NULL, NULL),
(189, 'Annahda12_2', '/images/projects/Annahda12_2.jpg', 'Annahda 12_2', 13, '2018-01-15 14:25:48', 'admin', NULL, NULL),
(190, 'Annahda12_3', '/images/projects/Annahda12_3.jpg', 'Annahda 12_3', 13, '2018-01-15 14:25:48', 'admin', NULL, NULL),
(191, 'Annahda12_4', '/images/projects/Annahda12_4.jpg', 'Annahda 12_4', 13, '2018-01-15 14:25:48', 'admin', NULL, NULL),
(192, 'Annahda12_5', '/images/projects/Annahda12_5.jpg', 'Annahda 12_5', 13, '2018-01-15 14:25:48', 'admin', NULL, NULL),
(193, 'Annahda12_6', '/images/projects/Annahda12_6.jpg', 'Annahda 12_6', 13, '2018-01-15 14:25:48', 'admin', NULL, NULL),
(194, 'Annahda12_7', '/images/projects/Annahda12_7.jpg', 'Annahda 12_7', 13, '2018-01-15 14:25:48', 'admin', NULL, NULL),
(195, 'Annahda12_8', '/images/projects/Annahda12_8.jpg', 'Annahda 12_8', 13, '2018-01-15 14:25:48', 'admin', NULL, NULL),
(196, 'Annahda12_9', '/images/projects/Annahda12_9.jpg', 'Annahda 12_9', 13, '2018-01-15 14:25:48', 'admin', NULL, NULL),
(197, 'Annahda12_10', '/images/projects/Annahda12_10.jpg', 'Annahda 12_10', 13, '2018-01-15 14:25:48', 'admin', NULL, NULL),
(198, 'Annahda12_11', '/images/projects/Annahda12_11.jpg', 'Annahda 12_11', 13, '2018-01-15 14:25:48', 'admin', NULL, NULL),
(199, 'Annahda12_12', '/images/projects/Annahda12_12.jpg', 'Annahda 12_12', 13, '2018-01-15 14:25:48', 'admin', NULL, NULL),
(200, 'Annahda12_13', '/images/projects/Annahda12_13.jpg', 'Annahda 12_13', 13, '2018-01-15 14:25:48', 'admin', NULL, NULL),
(201, 'Annahda12_14', '/images/projects/Annahda12_14.jpg', 'Annahda 12_14', 13, '2018-01-15 14:25:48', 'admin', NULL, NULL),
(202, 'Annahda12_15', '/images/projects/Annahda12_15.jpg', 'Annahda 12_15', 13, '2018-01-15 14:25:48', 'admin', NULL, NULL),
(203, 'Annahda12_16', '/images/projects/Annahda12_16.jpg', 'Annahda 12_16', 13, '2018-01-15 14:25:48', 'admin', NULL, NULL),
(204, 'Annahda12_17', '/images/projects/Annahda12_17.jpg', 'Annahda 12_17', 13, '2018-01-15 14:25:48', 'admin', NULL, NULL),
(205, 'Annahda12_18', '/images/projects/Annahda12_18.jpg', 'Annahda 12_18', 13, '2018-01-15 14:25:48', 'admin', NULL, NULL),
(206, 'Annahda12_19', '/images/projects/Annahda12_19.jpg', 'Annahda 12_19', 13, '2018-01-15 14:25:48', 'admin', NULL, NULL),
(207, 'Annahda12_20', '/images/projects/Annahda12_20.jpg', 'Annahda 12_20', 13, '2018-01-15 14:25:48', 'admin', NULL, NULL),
(208, 'Annahda12_21', '/images/projects/Annahda12_21.jpg', 'Annahda 12_21', 13, '2018-01-15 14:25:48', 'admin', NULL, NULL),
(209, 'Annahda12_22', '/images/projects/Annahda12_22.jpg', 'Annahda 12_22', 13, '2018-01-15 14:25:48', 'admin', NULL, NULL),
(210, 'Annahda12_23', '/images/projects/Annahda12_23.jpg', 'Annahda 12_23', 13, '2018-01-15 14:25:48', 'admin', NULL, NULL),
(211, 'Annahda12_24', '/images/projects/Annahda12_24.jpg', 'Annahda 12_24', 13, '2018-01-15 14:25:48', 'admin', NULL, NULL),
(212, 'Annahda12_25', '/images/projects/Annahda12_25.jpg', 'Annahda 12_25', 13, '2018-01-15 14:25:48', 'admin', NULL, NULL),
(213, 'Annahda12_26', '/images/projects/Annahda12_26.jpg', 'Annahda 12_26', 13, '2018-01-15 14:25:48', 'admin', NULL, NULL),
(214, 'Annahda11_1', '/images/projects/Annahda11_1.jpg', 'Annahda 11_1', 12, '2018-01-15 14:40:04', 'admin', NULL, NULL),
(215, 'Annahda11_2', '/images/projects/Annahda11_2.jpg', 'Annahda 11_2', 12, '2018-01-15 14:40:04', 'admin', NULL, NULL),
(216, 'Annahda11_3', '/images/projects/Annahda11_3.jpg', 'Annahda 11_3', 12, '2018-01-15 14:40:04', 'admin', NULL, NULL),
(217, 'Annahda11_4', '/images/projects/Annahda11_4.jpg', 'Annahda 11_4', 12, '2018-01-15 14:40:04', 'admin', NULL, NULL),
(218, 'Annahda11_5', '/images/projects/Annahda11_5.jpg', 'Annahda 11_5', 12, '2018-01-15 14:40:04', 'admin', NULL, NULL),
(219, 'Annahda11_6', '/images/projects/Annahda11_6.jpg', 'Annahda 11_6', 12, '2018-01-15 14:40:04', 'admin', NULL, NULL),
(220, 'Annahda11_7', '/images/projects/Annahda11_7.jpg', 'Annahda 11_7', 12, '2018-01-15 14:40:04', 'admin', NULL, NULL),
(221, 'Annahda11_8', '/images/projects/Annahda11_8.jpg', 'Annahda 11_8', 12, '2018-01-15 14:40:04', 'admin', NULL, NULL),
(222, 'Annahda11_9', '/images/projects/Annahda11_9.jpg', 'Annahda 11_9', 12, '2018-01-15 14:40:04', 'admin', NULL, NULL),
(223, 'Annahda11_10', '/images/projects/Annahda11_10.jpg', 'Annahda 11_10', 12, '2018-01-15 14:40:04', 'admin', NULL, NULL),
(224, 'Annahda11_11', '/images/projects/Annahda11_11.jpg', 'Annahda 11_11', 12, '2018-01-15 14:40:04', 'admin', NULL, NULL),
(225, 'Annahda11_12', '/images/projects/Annahda11_12.jpg', 'Annahda 11_12', 12, '2018-01-15 14:40:04', 'admin', NULL, NULL),
(226, 'Annahda11_13', '/images/projects/Annahda11_13.jpg', 'Annahda 11_13', 12, '2018-01-15 14:40:04', 'admin', NULL, NULL),
(227, 'Annahda11_14', '/images/projects/Annahda11_14.jpg', 'Annahda 11_14', 12, '2018-01-15 14:40:04', 'admin', NULL, NULL),
(228, 'Annahda11_15', '/images/projects/Annahda11_15.jpg', 'Annahda 11_15', 12, '2018-01-15 14:40:04', 'admin', NULL, NULL),
(229, 'Annahda11_16', '/images/projects/Annahda11_16.jpg', 'Annahda 11_16', 12, '2018-01-15 14:40:04', 'admin', NULL, NULL),
(230, 'Annahda11_17', '/images/projects/Annahda11_17.jpg', 'Annahda 11_17', 12, '2018-01-15 14:40:04', 'admin', NULL, NULL),
(231, 'Annahda11_18', '/images/projects/Annahda11_18.jpg', 'Annahda 11_18', 12, '2018-01-15 14:40:04', 'admin', NULL, NULL),
(232, 'Annahda11_19', '/images/projects/Annahda11_19.jpg', 'Annahda 11_19', 12, '2018-01-15 14:40:04', 'admin', NULL, NULL),
(233, 'Annahda11_20', '/images/projects/Annahda11_20.jpg', 'Annahda 11_20', 12, '2018-01-15 14:40:04', 'admin', NULL, NULL),
(234, 'Annahda11_21', '/images/projects/Annahda11_21.jpg', 'Annahda 11_21', 12, '2018-01-15 14:40:04', 'admin', NULL, NULL),
(235, 'Annahda11_22', '/images/projects/Annahda11_22.jpg', 'Annahda 11_22', 12, '2018-01-15 14:40:04', 'admin', NULL, NULL),
(236, 'Annahda10_1', '/images/projects/Annahda10_1.jpg', 'Annahda 10_1', 11, '2018-01-15 14:49:00', 'admin', NULL, NULL),
(237, 'Annahda10_2', '/images/projects/Annahda10_2.jpg', 'Annahda 10_2', 11, '2018-01-15 14:49:00', 'admin', NULL, NULL),
(238, 'Annahda10_3', '/images/projects/Annahda10_3.jpg', 'Annahda 10_3', 11, '2018-01-15 14:49:00', 'admin', NULL, NULL),
(239, 'Annahda10_4', '/images/projects/Annahda10_4.jpg', 'Annahda 10_4', 11, '2018-01-15 14:49:00', 'admin', NULL, NULL),
(240, 'Annahda10_5', '/images/projects/Annahda10_5.jpg', 'Annahda 10_5', 11, '2018-01-15 14:49:00', 'admin', NULL, NULL),
(241, 'Annahda10_6', '/images/projects/Annahda10_6.jpg', 'Annahda 10_6', 11, '2018-01-15 14:49:00', 'admin', NULL, NULL),
(242, 'Annahda10_7', '/images/projects/Annahda10_7.jpg', 'Annahda 10_7', 11, '2018-01-15 14:49:00', 'admin', NULL, NULL),
(243, 'Annahda10_8', '/images/projects/Annahda10_8.jpg', 'Annahda 10_8', 11, '2018-01-15 14:49:00', 'admin', NULL, NULL),
(244, 'Annahda10_9', '/images/projects/Annahda10_9.jpg', 'Annahda 10_9', 11, '2018-01-15 14:49:00', 'admin', NULL, NULL),
(245, 'Annahda10_10', '/images/projects/Annahda10_10.jpg', 'Annahda 10_10', 11, '2018-01-15 14:49:00', 'admin', NULL, NULL),
(246, 'Annahda10_11', '/images/projects/Annahda10_11.jpg', 'Annahda 10_11', 11, '2018-01-15 14:49:00', 'admin', NULL, NULL),
(247, 'Annahda10_12', '/images/projects/Annahda10_12.jpg', 'Annahda 10_12', 11, '2018-01-15 14:49:00', 'admin', NULL, NULL),
(248, 'Annahda10_13', '/images/projects/Annahda10_13.jpg', 'Annahda 10_13', 11, '2018-01-15 14:49:00', 'admin', NULL, NULL),
(249, 'Annahda10_14', '/images/projects/Annahda10_14.jpg', 'Annahda 10_14', 11, '2018-01-15 14:49:00', 'admin', NULL, NULL),
(250, 'Annahda9_1', '/images/projects/Annahda9_1.jpg', 'Annahda 9_1', 10, '2018-01-15 15:19:30', 'admin', NULL, NULL),
(251, 'Annahda9_2', '/images/projects/Annahda9_2.jpg', 'Annahda 9_2', 10, '2018-01-15 15:19:30', 'admin', NULL, NULL),
(252, 'Annahda9_3', '/images/projects/Annahda9_3.jpg', 'Annahda 9_3', 10, '2018-01-15 15:19:30', 'admin', NULL, NULL),
(253, 'Annahda9_4', '/images/projects/Annahda9_4.jpg', 'Annahda 9_4', 10, '2018-01-15 15:19:30', 'admin', NULL, NULL),
(254, 'Annahda9_5', '/images/projects/Annahda9_5.jpg', 'Annahda 9_5', 10, '2018-01-15 15:19:30', 'admin', NULL, NULL),
(255, 'Annahda9_6', '/images/projects/Annahda9_6.jpg', 'Annahda 9_6', 10, '2018-01-15 15:19:30', 'admin', NULL, NULL),
(256, 'Annahda9_7', '/images/projects/Annahda9_7.jpg', 'Annahda 9_7', 10, '2018-01-15 15:19:30', 'admin', NULL, NULL),
(257, 'Annahda9_8', '/images/projects/Annahda9_8.jpg', 'Annahda 9_8', 10, '2018-01-15 15:19:30', 'admin', NULL, NULL),
(258, 'Annahda9_9', '/images/projects/Annahda9_9.jpg', 'Annahda 9_9', 10, '2018-01-15 15:19:30', 'admin', NULL, NULL),
(259, 'Annahda9_10', '/images/projects/Annahda9_10.jpg', 'Annahda 9_10', 10, '2018-01-15 15:19:30', 'admin', NULL, NULL),
(260, 'Annahda9_11', '/images/projects/Annahda9_11.jpg', 'Annahda 9_11', 10, '2018-01-15 15:19:30', 'admin', NULL, NULL),
(261, 'Annahda9_12', '/images/projects/Annahda9_12.jpg', 'Annahda 9_12', 10, '2018-01-15 15:19:30', 'admin', NULL, NULL),
(262, 'Annahda9_13', '/images/projects/Annahda9_13.jpg', 'Annahda 9_13', 10, '2018-01-15 15:19:30', 'admin', NULL, NULL),
(263, 'Annahda9_14', '/images/projects/Annahda9_14.jpg', 'Annahda 9_14', 10, '2018-01-15 15:19:30', 'admin', NULL, NULL),
(351, 'Annahda9_58', '/images/projects/Annahda9_58.jpg', 'Annahda 9_58', 10, '2018-01-15 15:27:39', 'admin', NULL, NULL),
(350, 'Annahda9_57', '/images/projects/Annahda9_57.jpg', 'Annahda 9_57', 10, '2018-01-15 15:27:39', 'admin', NULL, NULL),
(349, 'Annahda9_56', '/images/projects/Annahda9_56.jpg', 'Annahda 9_56', 10, '2018-01-15 15:27:39', 'admin', NULL, NULL),
(348, 'Annahda9_55', '/images/projects/Annahda9_55.jpg', 'Annahda 9_55', 10, '2018-01-15 15:27:39', 'admin', NULL, NULL),
(347, 'Annahda9_54', '/images/projects/Annahda9_54.jpg', 'Annahda 9_54', 10, '2018-01-15 15:27:39', 'admin', NULL, NULL),
(346, 'Annahda9_53', '/images/projects/Annahda9_53.jpg', 'Annahda 9_53', 10, '2018-01-15 15:27:39', 'admin', NULL, NULL),
(345, 'Annahda9_52', '/images/projects/Annahda9_52.jpg', 'Annahda 9_52', 10, '2018-01-15 15:27:39', 'admin', NULL, NULL),
(344, 'Annahda9_51', '/images/projects/Annahda9_51.jpg', 'Annahda 9_51', 10, '2018-01-15 15:27:39', 'admin', NULL, NULL),
(343, 'Annahda9_50', '/images/projects/Annahda9_50.jpg', 'Annahda 9_50', 10, '2018-01-15 15:27:39', 'admin', NULL, NULL),
(342, 'Annahda9_49', '/images/projects/Annahda9_49.jpg', 'Annahda 9_49', 10, '2018-01-15 15:27:39', 'admin', NULL, NULL),
(341, 'Annahda9_48', '/images/projects/Annahda9_48.jpg', 'Annahda 9_48', 10, '2018-01-15 15:27:39', 'admin', NULL, NULL),
(340, 'Annahda9_47', '/images/projects/Annahda9_47.jpg', 'Annahda 9_47', 10, '2018-01-15 15:27:39', 'admin', NULL, NULL),
(339, 'Annahda9_46', '/images/projects/Annahda9_46.jpg', 'Annahda 9_46', 10, '2018-01-15 15:27:39', 'admin', NULL, NULL),
(338, 'Annahda9_45', '/images/projects/Annahda9_45.jpg', 'Annahda 9_45', 10, '2018-01-15 15:27:39', 'admin', NULL, NULL),
(337, 'Annahda9_44', '/images/projects/Annahda9_44.jpg', 'Annahda 9_44', 10, '2018-01-15 15:27:39', 'admin', NULL, NULL),
(336, 'Annahda9_43', '/images/projects/Annahda9_43.jpg', 'Annahda 9_43', 10, '2018-01-15 15:27:39', 'admin', NULL, NULL),
(335, 'Annahda9_42', '/images/projects/Annahda9_42.jpg', 'Annahda 9_42', 10, '2018-01-15 15:27:39', 'admin', NULL, NULL),
(334, 'Annahda9_41', '/images/projects/Annahda9_41.jpg', 'Annahda 9_41', 10, '2018-01-15 15:27:39', 'admin', NULL, NULL),
(333, 'Annahda9_40', '/images/projects/Annahda9_40.jpg', 'Annahda 9_40', 10, '2018-01-15 15:27:39', 'admin', NULL, NULL),
(332, 'Annahda9_39', '/images/projects/Annahda9_39.jpg', 'Annahda 9_39', 10, '2018-01-15 15:27:39', 'admin', NULL, NULL),
(331, 'Annahda9_38', '/images/projects/Annahda9_38.jpg', 'Annahda 9_38', 10, '2018-01-15 15:27:39', 'admin', NULL, NULL),
(330, 'Annahda9_37', '/images/projects/Annahda9_37.jpg', 'Annahda 9_37', 10, '2018-01-15 15:27:39', 'admin', NULL, NULL),
(329, 'Annahda9_36', '/images/projects/Annahda9_36.jpg', 'Annahda 9_36', 10, '2018-01-15 15:27:39', 'admin', NULL, NULL),
(328, 'Annahda9_35', '/images/projects/Annahda9_35.jpg', 'Annahda 9_35', 10, '2018-01-15 15:27:39', 'admin', NULL, NULL),
(327, 'Annahda9_34', '/images/projects/Annahda9_34.jpg', 'Annahda 9_34', 10, '2018-01-15 15:27:39', 'admin', NULL, NULL),
(326, 'Annahda9_33', '/images/projects/Annahda9_33.jpg', 'Annahda 9_33', 10, '2018-01-15 15:27:39', 'admin', NULL, NULL),
(325, 'Annahda9_32', '/images/projects/Annahda9_32.jpg', 'Annahda 9_32', 10, '2018-01-15 15:27:39', 'admin', NULL, NULL),
(324, 'Annahda9_31', '/images/projects/Annahda9_31.jpg', 'Annahda 9_31', 10, '2018-01-15 15:27:39', 'admin', NULL, NULL),
(323, 'Annahda9_30', '/images/projects/Annahda9_30.jpg', 'Annahda 9_30', 10, '2018-01-15 15:27:39', 'admin', NULL, NULL),
(322, 'Annahda9_29', '/images/projects/Annahda9_29.jpg', 'Annahda 9_29', 10, '2018-01-15 15:27:39', 'admin', NULL, NULL),
(321, 'Annahda9_28', '/images/projects/Annahda9_28.jpg', 'Annahda 9_28', 10, '2018-01-15 15:27:39', 'admin', NULL, NULL),
(320, 'Annahda9_27', '/images/projects/Annahda9_27.jpg', 'Annahda 9_27', 10, '2018-01-15 15:27:39', 'admin', NULL, NULL),
(319, 'Annahda9_26', '/images/projects/Annahda9_26.jpg', 'Annahda 9_26', 10, '2018-01-15 15:27:39', 'admin', NULL, NULL),
(318, 'Annahda9_25', '/images/projects/Annahda9_25.jpg', 'Annahda 9_25', 10, '2018-01-15 15:27:39', 'admin', NULL, NULL),
(317, 'Annahda9_24', '/images/projects/Annahda9_24.jpg', 'Annahda 9_24', 10, '2018-01-15 15:27:39', 'admin', NULL, NULL),
(316, 'Annahda9_23', '/images/projects/Annahda9_23.jpg', 'Annahda 9_23', 10, '2018-01-15 15:27:39', 'admin', NULL, NULL),
(315, 'Annahda9_22', '/images/projects/Annahda9_22.jpg', 'Annahda 9_22', 10, '2018-01-15 15:27:39', 'admin', NULL, NULL),
(314, 'Annahda9_21', '/images/projects/Annahda9_21.jpg', 'Annahda 9_21', 10, '2018-01-15 15:27:39', 'admin', NULL, NULL),
(313, 'Annahda9_20', '/images/projects/Annahda9_20.jpg', 'Annahda 9_20', 10, '2018-01-15 15:27:39', 'admin', NULL, NULL),
(312, 'Annahda9_19', '/images/projects/Annahda9_19.jpg', 'Annahda 9_19', 10, '2018-01-15 15:27:39', 'admin', NULL, NULL),
(311, 'Annahda9_18', '/images/projects/Annahda9_18.jpg', 'Annahda 9_18', 10, '2018-01-15 15:27:39', 'admin', NULL, NULL),
(310, 'Annahda9_17', '/images/projects/Annahda9_17.jpg', 'Annahda 9_17', 10, '2018-01-15 15:27:39', 'admin', NULL, NULL),
(309, 'Annahda9_16', '/images/projects/Annahda9_16.jpg', 'Annahda 9_16', 10, '2018-01-15 15:27:39', 'admin', NULL, NULL),
(308, 'Annahda9_15', '/images/projects/Annahda9_15.jpg', 'Annahda 9_15', 10, '2018-01-15 15:27:39', 'admin', NULL, NULL),
(352, 'Annahda10_15', '/images/projects/Annahda10_15.jpg', 'Annahda 15_15', 11, '2018-01-15 16:22:15', 'admin', NULL, NULL),
(353, 'Annahda10_16', '/images/projects/Annahda10_16.jpg', 'Annahda 16_15', 11, '2018-01-15 16:22:15', 'admin', NULL, NULL),
(354, 'Annahda10_17', '/images/projects/Annahda10_17.jpg', 'Annahda 17_15', 11, '2018-01-15 16:22:15', 'admin', NULL, NULL),
(355, 'Annahda10_18', '/images/projects/Annahda10_18.jpg', 'Annahda 18_15', 11, '2018-01-15 16:22:15', 'admin', NULL, NULL),
(356, 'Annahda10_19', '/images/projects/Annahda10_19.jpg', 'Annahda 19_15', 11, '2018-01-15 16:22:15', 'admin', NULL, NULL),
(357, 'Annahda10_20', '/images/projects/Annahda10_20.jpg', 'Annahda 20_15', 11, '2018-01-15 16:22:15', 'admin', NULL, NULL),
(358, 'Annahda10_21', '/images/projects/Annahda10_21.jpg', 'Annahda 21_15', 11, '2018-01-15 16:22:15', 'admin', NULL, NULL),
(359, 'Annahda10_22', '/images/projects/Annahda10_22.jpg', 'Annahda 22_15', 11, '2018-01-15 16:22:15', 'admin', NULL, NULL),
(360, 'Annahda10_23', '/images/projects/Annahda10_23.jpg', 'Annahda 23_15', 11, '2018-01-15 16:22:15', 'admin', NULL, NULL),
(361, 'Annahda10_24', '/images/projects/Annahda10_24.jpg', 'Annahda 24_15', 11, '2018-01-15 16:22:15', 'admin', NULL, NULL),
(362, 'Annahda10_25', '/images/projects/Annahda10_25.jpg', 'Annahda 25_15', 11, '2018-01-15 16:22:15', 'admin', NULL, NULL),
(363, 'Annahda10_26', '/images/projects/Annahda10_26.jpg', 'Annahda 26_15', 11, '2018-01-15 16:22:15', 'admin', NULL, NULL),
(364, 'Annahda10_27', '/images/projects/Annahda10_27.jpg', 'Annahda 27_15', 11, '2018-01-15 16:22:15', 'admin', NULL, NULL),
(365, 'Annahda10_28', '/images/projects/Annahda10_28.jpg', 'Annahda 28_15', 11, '2018-01-15 16:22:15', 'admin', NULL, NULL),
(366, 'Annahda10_29', '/images/projects/Annahda10_29.jpg', 'Annahda 29_15', 11, '2018-01-15 16:22:15', 'admin', NULL, NULL),
(367, 'Annahda10_30', '/images/projects/Annahda10_30.jpg', 'Annahda 30_15', 11, '2018-01-15 16:22:15', 'admin', NULL, NULL),
(368, 'Annahda10_31', '/images/projects/Annahda10_31.jpg', 'Annahda 31_15', 11, '2018-01-15 16:22:15', 'admin', NULL, NULL),
(369, 'Annahda10_32', '/images/projects/Annahda10_32.jpg', 'Annahda 32_15', 11, '2018-01-15 16:22:15', 'admin', NULL, NULL),
(370, 'Annahda10_33', '/images/projects/Annahda10_33.jpg', 'Annahda 33_15', 11, '2018-01-15 16:22:15', 'admin', NULL, NULL),
(371, 'Annahda10_34', '/images/projects/Annahda10_34.jpg', 'Annahda 34_15', 11, '2018-01-15 16:22:15', 'admin', NULL, NULL),
(372, 'Annahda10_35', '/images/projects/Annahda10_35.jpg', 'Annahda 35_15', 11, '2018-01-15 16:22:15', 'admin', NULL, NULL),
(373, 'Annahda10_36', '/images/projects/Annahda10_36.jpg', 'Annahda 36_15', 11, '2018-01-15 16:22:15', 'admin', NULL, NULL),
(374, 'Annahda10_37', '/images/projects/Annahda10_37.jpg', 'Annahda 37_15', 11, '2018-01-15 16:22:15', 'admin', NULL, NULL),
(375, 'Annahda10_38', '/images/projects/Annahda10_38.jpg', 'Annahda 38_15', 11, '2018-01-15 16:22:15', 'admin', NULL, NULL),
(376, 'Annahda10_39', '/images/projects/Annahda10_39.jpg', 'Annahda 39_15', 11, '2018-01-15 16:22:15', 'admin', NULL, NULL),
(377, 'Annahda10_40', '/images/projects/Annahda10_40.jpg', 'Annahda 40_15', 11, '2018-01-15 16:22:15', 'admin', NULL, NULL),
(378, 'Annahda10_41', '/images/projects/Annahda10_41.jpg', 'Annahda 41_15', 11, '2018-01-15 16:22:15', 'admin', NULL, NULL),
(379, 'Annahda10_42', '/images/projects/Annahda10_42.jpg', 'Annahda 42_15', 11, '2018-01-15 16:22:15', 'admin', NULL, NULL),
(380, 'Annahda10_43', '/images/projects/Annahda10_43.jpg', 'Annahda 43_15', 11, '2018-01-15 16:22:15', 'admin', NULL, NULL),
(381, 'Annahda10_44', '/images/projects/Annahda10_44.jpg', 'Annahda 44_15', 11, '2018-01-15 16:22:15', 'admin', NULL, NULL),
(382, 'Annahda10_45', '/images/projects/Annahda10_45.jpg', 'Annahda 45_15', 11, '2018-01-15 16:22:15', 'admin', NULL, NULL),
(383, 'Annahda10_46', '/images/projects/Annahda10_46.jpg', 'Annahda 46_15', 11, '2018-01-15 16:22:15', 'admin', NULL, NULL),
(384, 'Annahda10_47', '/images/projects/Annahda10_47.jpg', 'Annahda 47_15', 11, '2018-01-15 16:22:15', 'admin', NULL, NULL),
(385, 'Annahda10_48', '/images/projects/Annahda10_48.jpg', 'Annahda 48_15', 11, '2018-01-15 16:22:15', 'admin', NULL, NULL),
(386, 'Annahda10_49', '/images/projects/Annahda10_49.jpg', 'Annahda 49_15', 11, '2018-01-15 16:22:15', 'admin', NULL, NULL),
(387, 'Annahda10_50', '/images/projects/Annahda10_50.jpg', 'Annahda 50_15', 11, '2018-01-15 16:22:15', 'admin', NULL, NULL),
(388, 'Annahda10_51', '/images/projects/Annahda10_51.jpg', 'Annahda 51_15', 11, '2018-01-15 16:22:15', 'admin', NULL, NULL),
(389, 'Annahda10_52', '/images/projects/Annahda10_52.jpg', 'Annahda 52_15', 11, '2018-01-15 16:22:15', 'admin', NULL, NULL),
(390, 'Annahda10_53', '/images/projects/Annahda10_53.jpg', 'Annahda 53_15', 11, '2018-01-15 16:22:15', 'admin', NULL, NULL),
(391, 'Annahda10_54', '/images/projects/Annahda10_54.jpg', 'Annahda 54_15', 11, '2018-01-15 16:22:15', 'admin', NULL, NULL),
(392, 'Annahda10_55', '/images/projects/Annahda10_55.jpg', 'Annahda 55_15', 11, '2018-01-15 16:22:15', 'admin', NULL, NULL),
(393, 'Annahda10_56', '/images/projects/Annahda10_56.jpg', 'Annahda 56_15', 11, '2018-01-15 16:22:15', 'admin', NULL, NULL),
(394, 'Annahda10_57', '/images/projects/Annahda10_57.jpg', 'Annahda 57_15', 11, '2018-01-15 16:22:15', 'admin', NULL, NULL),
(395, 'Annahda10_58', '/images/projects/Annahda10_58.jpg', 'Annahda 58_15', 11, '2018-01-15 16:22:15', 'admin', NULL, NULL),
(396, 'Annahda10_59', '/images/projects/Annahda10_59.jpg', 'Annahda 59_15', 11, '2018-01-15 16:22:15', 'admin', NULL, NULL),
(397, 'Annahda10_60', '/images/projects/Annahda10_60.jpg', 'Annahda 60_15', 11, '2018-01-15 16:22:15', 'admin', NULL, NULL),
(398, 'Annahda10_61', '/images/projects/Annahda10_61.jpg', 'Annahda 61_15', 11, '2018-01-15 16:22:15', 'admin', NULL, NULL),
(399, 'Annahda10_62', '/images/projects/Annahda10_62.jpg', 'Annahda 62_15', 11, '2018-01-15 16:22:15', 'admin', NULL, NULL),
(400, 'Annahda10_63', '/images/projects/Annahda10_63.jpg', 'Annahda 63_15', 11, '2018-01-15 16:22:15', 'admin', NULL, NULL),
(401, 'Annahda10_64', '/images/projects/Annahda10_64.jpg', 'Annahda 64_15', 11, '2018-01-15 16:22:15', 'admin', NULL, NULL),
(402, 'Annahda10_65', '/images/projects/Annahda10_65.jpg', 'Annahda 65_15', 11, '2018-01-15 16:22:15', 'admin', NULL, NULL),
(403, 'Annahda10_66', '/images/projects/Annahda10_66.jpg', 'Annahda 66_15', 11, '2018-01-15 16:22:15', 'admin', NULL, NULL),
(404, 'Annahda10_67', '/images/projects/Annahda10_67.jpg', 'Annahda 67_15', 11, '2018-01-15 16:22:15', 'admin', NULL, NULL),
(405, 'Annahda10_68', '/images/projects/Annahda10_68.jpg', 'Annahda 68_15', 11, '2018-01-15 16:22:15', 'admin', NULL, NULL),
(406, 'Annahda10_69', '/images/projects/Annahda10_69.jpg', 'Annahda 69_15', 11, '2018-01-15 16:22:15', 'admin', NULL, NULL),
(407, 'Annahda10_70', '/images/projects/Annahda10_70.jpg', 'Annahda 70_15', 11, '2018-01-15 16:22:15', 'admin', NULL, NULL),
(408, 'Annahda10_71', '/images/projects/Annahda10_71.jpg', 'Annahda 71_15', 11, '2018-01-15 16:22:15', 'admin', NULL, NULL),
(409, 'Annahda10_72', '/images/projects/Annahda10_72.jpg', 'Annahda 72_15', 11, '2018-01-15 16:22:15', 'admin', NULL, NULL),
(410, 'Annahda10_73', '/images/projects/Annahda10_73.jpg', 'Annahda 73_15', 11, '2018-01-15 16:22:15', 'admin', NULL, NULL),
(411, 'Annahda10_74', '/images/projects/Annahda10_74.jpg', 'Annahda 74_15', 11, '2018-01-15 16:22:15', 'admin', NULL, NULL),
(412, 'Annahda10_75', '/images/projects/Annahda10_75.jpg', 'Annahda 75_15', 11, '2018-01-15 16:22:15', 'admin', NULL, NULL),
(413, 'Annahda10_76', '/images/projects/Annahda10_76.jpg', 'Annahda 76_15', 11, '2018-01-15 16:22:15', 'admin', NULL, NULL),
(414, 'Annahda10_77', '/images/projects/Annahda10_77.jpg', 'Annahda 77_15', 11, '2018-01-15 16:22:15', 'admin', NULL, NULL),
(415, 'Annahda10_78', '/images/projects/Annahda10_78.jpg', 'Annahda 78_15', 11, '2018-01-15 16:22:15', 'admin', NULL, NULL),
(416, 'Annahda10_79', '/images/projects/Annahda10_79.jpg', 'Annahda 79_15', 11, '2018-01-15 16:22:15', 'admin', NULL, NULL),
(417, 'Annahda10_80', '/images/projects/Annahda10_80.jpg', 'Annahda 80_15', 11, '2018-01-15 16:22:15', 'admin', NULL, NULL),
(418, 'Annahda10_81', '/images/projects/Annahda10_81.jpg', 'Annahda 81_15', 11, '2018-01-15 16:22:15', 'admin', NULL, NULL),
(419, 'Annahda10_82', '/images/projects/Annahda10_82.jpg', 'Annahda 82_15', 11, '2018-01-15 16:22:15', 'admin', NULL, NULL),
(420, 'Annahda10_83', '/images/projects/Annahda10_83.jpg', 'Annahda 83_15', 11, '2018-01-15 16:22:15', 'admin', NULL, NULL),
(421, 'Annahda10_84', '/images/projects/Annahda10_84.jpg', 'Annahda 84_15', 11, '2018-01-15 16:22:15', 'admin', NULL, NULL),
(422, 'Annahda10_85', '/images/projects/Annahda10_85.jpg', 'Annahda 85_15', 11, '2018-01-15 16:22:15', 'admin', NULL, NULL),
(423, 'Annahda10_86', '/images/projects/Annahda10_86.jpg', 'Annahda 86_15', 11, '2018-01-15 16:22:15', 'admin', NULL, NULL),
(424, 'Annahda10_87', '/images/projects/Annahda10_87.jpg', 'Annahda 87_15', 11, '2018-01-15 16:22:15', 'admin', NULL, NULL),
(425, 'Annahda10_88', '/images/projects/Annahda10_88.jpg', 'Annahda 88_15', 11, '2018-01-15 16:22:15', 'admin', NULL, NULL),
(426, 'Annahda10_89', '/images/projects/Annahda10_89.jpg', 'Annahda 89_15', 11, '2018-01-15 16:22:15', 'admin', NULL, NULL),
(427, 'Annahda10_90', '/images/projects/Annahda10_90.jpg', 'Annahda 90_15', 11, '2018-01-15 16:22:15', 'admin', NULL, NULL),
(428, 'Annahda10_91', '/images/projects/Annahda10_91.jpg', 'Annahda 91_15', 11, '2018-01-15 16:22:15', 'admin', NULL, NULL),
(429, 'Annahda10_92', '/images/projects/Annahda10_92.jpg', 'Annahda 92_15', 11, '2018-01-15 16:22:15', 'admin', NULL, NULL),
(430, 'Annahda10_93', '/images/projects/Annahda10_93.jpg', 'Annahda 93_15', 11, '2018-01-15 16:22:15', 'admin', NULL, NULL),
(431, 'Annahda10_94', '/images/projects/Annahda10_94.jpg', 'Annahda 94_15', 11, '2018-01-15 16:22:15', 'admin', NULL, NULL),
(432, 'ANNAHDA 15', '/images/projects/5a7595c8a0c7bIMG-20171217-WA0085.jpg', '1', 16, '2018-02-03 10:58:16', 'admin', NULL, NULL),
(433, 'ANNAHDA 15', '/images/projects/5a759632b109eIMG-20180131-WA0299.jpg', '2', 16, '2018-02-03 11:00:02', 'admin', NULL, NULL),
(434, 'an 14', '/images/projects/5a7597e541b0aIMG-20180103-WA0023.jpg', '1', 15, '2018-02-03 11:07:17', 'admin', NULL, NULL),
(435, 'an 14', '/images/projects/5a75983a2c707IMG-20180203-WA0008.jpg', '1', 15, '2018-02-03 11:08:42', 'admin', NULL, NULL),
(436, 'an 14', '/images/projects/5a75989127766IMG-20180103-WA0028.jpg', '2', 14, '2018-02-03 11:10:09', 'admin', NULL, NULL),
(437, 'nn', '/images/projects/5a7599002e8ffIMG-20171029-WA0046.jpg', '12', 13, '2018-02-03 11:12:00', 'admin', NULL, NULL),
(438, '1', '/images/projects/5a881a9f8dcfc20171119_113036.jpg', '1', 17, '2018-02-17 12:05:51', 'admin', NULL, NULL),
(439, '2', '/images/projects/5a881b0ed5d5220171119_113200.jpg', '1', 17, '2018-02-17 12:07:42', 'admin', NULL, NULL),
(440, '1', '/images/projects/5a881bfbc8bdb20171119_113155.jpg', '1', 18, '2018-02-17 12:11:39', 'admin', NULL, NULL),
(441, '2', '/images/projects/5a881c760984120171119_112952.jpg', '1', 18, '2018-02-17 12:13:42', 'admin', NULL, NULL),
(442, '1', '/images/projects/5a881e31408e020180207_174328(0).jpg', '1', 20, '2018-02-17 12:21:05', 'admin', NULL, NULL),
(443, '2', '/images/projects/5a881eeb81b7020180207_174318.jpg', '1', 20, '2018-02-17 12:24:11', 'admin', NULL, NULL),
(444, '1', '/images/projects/5a881f8fe1908IMG-20180131-WA0296.jpg', '1', 16, '2018-02-17 12:26:55', 'admin', NULL, NULL),
(445, '1', '/images/projects/5a881fc9b570bIMG-20180203-WA0008 - Copie.jpg', '1', 15, '2018-02-17 12:27:53', 'admin', NULL, NULL),
(446, '1', '/images/projects/5a88208a01e3eIMG-20180103-WA0027 - Copie.jpg', '1', 14, '2018-02-17 12:31:06', 'admin', NULL, NULL),
(447, '1', '/images/projects/5a8822d36334320180105_134800.jpg', '1', 12, '2018-02-17 12:40:51', 'admin', NULL, NULL),
(448, '1', '/images/projects/5a88238df247020180105_140013 - Copie.jpg', '1', 12, '2018-02-17 12:43:57', 'admin', NULL, NULL),
(449, '1', '/images/projects/5a8d9380c7a8020180105_134546.jpg', '1', 12, '2018-02-21 03:42:56', 'admin', NULL, NULL),
(450, '1', '/images/projects/5a8d942dd97f720180105_134646.jpg', '1', 12, '2018-02-21 03:45:49', 'admin', NULL, NULL),
(451, '1', '/images/projects/5a8d94ebc928020180105_134743.jpg', '1', 12, '2018-02-21 03:48:59', 'admin', NULL, NULL),
(452, '1', '', '1', 20, '2018-02-26 10:53:18', 'admin', NULL, NULL),
(453, '2', '', '2', 20, '2018-02-26 10:57:08', 'admin', NULL, NULL),
(454, '1', '', '3', 20, '2018-02-26 04:56:19', 'admin', NULL, NULL),
(455, '1', '', '1', 20, '2018-02-27 10:25:19', 'admin', NULL, NULL),
(456, '1', '', '1', 20, '2018-02-27 10:37:46', 'admin', NULL, NULL),
(457, '1', '', '1', 20, '2018-02-27 10:52:00', 'admin', NULL, NULL),
(458, '1', '', '1', 20, '2018-02-27 11:24:59', 'admin', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `t_list_produit`
--

CREATE TABLE `t_list_produit` (
  `id` int(11) NOT NULL,
  `produit` int(11) DEFAULT NULL,
  `quantite` int(11) DEFAULT NULL,
  `idCommande` int(11) DEFAULT NULL,
  `total` decimal(12,2) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_list_produit`
--

INSERT INTO `t_list_produit` (`id`, `produit`, `quantite`, `idCommande`, `total`) VALUES
(12, 3, 120, 12, '144000.00'),
(15, 1, 120, 12, '120.00'),
(16, 2, 300, 12, '600.00'),
(19, 3, 20, 14, '2400.00'),
(20, 2, 120, 17, '4800.00'),
(22, 1, 30, 16, '3000.00'),
(23, 3, 90, 18, '10800.00'),
(24, 2, 80, 18, '3200.00');

-- --------------------------------------------------------

--
-- Table structure for table `t_livraison`
--

CREATE TABLE `t_livraison` (
  `id` int(11) NOT NULL,
  `libelle` varchar(50) NOT NULL,
  `designation` text NOT NULL,
  `quantite` int(11) NOT NULL,
  `prixUnitaire` decimal(10,2) NOT NULL,
  `paye` decimal(10,2) NOT NULL,
  `reste` decimal(10,2) NOT NULL,
  `dateLivraison` date NOT NULL,
  `modePaiement` varchar(50) NOT NULL,
  `idFournisseur` int(11) NOT NULL,
  `idProjet` int(11) NOT NULL,
  `code` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_livraison`
--

INSERT INTO `t_livraison` (`id`, `libelle`, `designation`, `quantite`, `prixUnitaire`, `paye`, `reste`, `dateLivraison`, `modePaiement`, `idFournisseur`, `idProjet`, `code`) VALUES
(1, 'LB1P1', 'Cartouche 112', 12, '12.00', '14.00', '130.00', '2014-05-17', 'Esp&Atilde;&uml;ce', 4, 1, NULL),
(2, 'LB2P1', 'TubeRC4521', 10, '100.00', '350.00', '650.00', '2014-05-17', 'Esp&Atilde;&uml;ce', 4, 1, NULL),
(3, 'LB3P1', 'Colle SX87', 1, '50.00', '50.00', '0.00', '2014-05-17', 'Esp&Atilde;&uml;ce', 2, 1, NULL),
(4, 'BL1P1', 'C&Atilde;&copy;rame F1', 2000, '5.00', '9500.00', '500.00', '2014-05-17', 'Esp&Atilde;&uml;ce', 1, 1, NULL),
(5, 'LB4P1', 'Tube1000', 50, '250.00', '1000.00', '11500.00', '2014-05-17', 'Esp&Atilde;&uml;ce', 3, 1, NULL),
(23, 'AX2014', 'Caf&eacute; Mentos', 100, '8.00', '0.00', '0.00', '2015-03-14', '', 16, 0, '5504556064a7e'),
(7, 'LBYP1', 'Ampoule', 75, '6.00', '200.00', '250.00', '2014-05-16', 'Esp&Atilde;&uml;ce', 4, 1, NULL),
(8, '1200', 'Fer 12', 40, '800.00', '0.00', '32000.00', '2014-05-08', 'Esp&Atilde;&uml;ce', 4, 1, NULL),
(24, 'caf&eacute; dubois', 'dubois', 10, '120.00', '0.00', '0.00', '2015-03-15', '', 19, 0, '5505a5d5dc4f2'),
(25, 'XO', 'MPX3', 120, '10.00', '0.00', '0.00', '2015-04-08', '', 20, 0, '552528798ad88'),
(13, 'FrnBur', 'Fourniture Bureau ', 4, '2000.00', '0.00', '0.00', '2015-02-19', '', 7, 5, '54e5c30c1bee9'),
(15, 'CMT', 'Caf&eacute; Mentos', 100, '100.00', '0.00', '0.00', '0000-00-00', '', 9, 5, '54f44eaed5a96'),
(22, 'CFEDUB', 'Caf&eacute; Dubois', 12, '120.00', '0.00', '0.00', '0000-00-00', '', 16, 0, '55045468c6074');

-- --------------------------------------------------------

--
-- Table structure for table `t_locaux`
--

CREATE TABLE `t_locaux` (
  `id` int(11) NOT NULL,
  `nom` varchar(45) DEFAULT NULL,
  `superficie` decimal(10,2) DEFAULT NULL,
  `facade` varchar(45) DEFAULT NULL,
  `prix` decimal(10,2) DEFAULT NULL,
  `mezzanine` varchar(45) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `idProjet` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_locaux`
--

INSERT INTO `t_locaux` (`id`, `nom`, `superficie`, `facade`, `prix`, `mezzanine`, `status`, `idProjet`) VALUES
(2, 'LC1', '100.00', 'FC1', '810000.00', 'Avec', 'Oui', 5),
(3, 'LC22', '99.00', 'FC2', '120000.00', 'Avec', 'Oui', 5),
(4, 'LC1', '120.00', 'F1', '1200200.00', 'Avec', 'Non', 3),
(5, 'LC2', '120.00', 'F1', '1200200.00', 'Avec', 'Non', 3),
(6, 'LC3', '150.00', 'F2', '2000000.00', 'Avec', 'Non', 3);

-- --------------------------------------------------------

--
-- Table structure for table `t_mail`
--

CREATE TABLE `t_mail` (
  `id` int(11) NOT NULL,
  `content` text NOT NULL,
  `sender` varchar(50) NOT NULL,
  `created` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_mail`
--

INSERT INTO `t_mail` (`id`, `content`, `sender`, `created`) VALUES
(1, 'Salut', 'admin', '2014-05-17 15:42:44'),
(2, 'Copier l\'adresse du client num&Atilde;&copy;ro 66', 'admin', '2014-05-17 15:43:11'),
(3, 'Imprimer le contrat num&Atilde;&copy;ro 55', 'admin', '2014-05-17 15:43:26'),
(4, 'Ok c\'est fait', 'admin', '2014-05-17 15:43:35'),
(5, 'Test phonen', 'admin', '2014-05-19 17:31:38'),
(6, 'Slm', 'admin', '2014-05-31 12:15:49'),
(7, 'salam si yassin?', 'admin', '2014-05-31 14:37:52'),
(8, 'sqdqsd', 'admin', '2015-01-27 16:01:01'),
(9, 'ahlan', 'abdou', '2015-01-27 16:01:35'),
(10, 'aassou.abdelilah@gmail.com', 'admin', '2015-01-27 16:25:42'),
(11, 'all', 'admin', '2015-01-27 18:01:09'),
(12, 'azaz', 'admin', '2015-01-27 18:01:13'),
(13, 'azeazeaze', 'admin', '2015-01-27 18:01:16'),
(14, 'azeazeaze', 'admin', '2015-01-27 18:01:19'),
(15, 'aha', 'abdou', '2015-01-27 18:21:41'),
(16, 'asasasasasas', 'admin', '2015-01-27 18:27:53'),
(17, 'salam 3alikom', 'admin', '2015-01-28 17:40:12'),
(18, 'test', 'admin', '2015-02-07 16:53:15'),
(19, 'Message pour client 1', 'admin', '2015-02-21 12:13:01'),
(20, 'Imprimer le contrat du client Jawad Derdoud', 'abdou', '2015-02-21 12:28:15'),
(21, 'ok', 'admin', '2015-02-21 12:28:44'),
(22, 'salam 3alikom', 'admin', '2015-02-22 12:44:40'),
(23, 'wa3alikom salam', 'abdou', '2015-02-22 12:45:52'),
(24, 'salam', 'admin', '2015-03-14 18:07:21'),
(25, 'wa3alikom salam', 'abdou', '2015-03-14 18:07:46'),
(26, 'imprimer les fiches des clients', 'admin', '2015-04-08 15:11:47');

-- --------------------------------------------------------

--
-- Table structure for table `t_notes_client`
--

CREATE TABLE `t_notes_client` (
  `id` int(11) NOT NULL,
  `note` text DEFAULT NULL,
  `created` date DEFAULT NULL,
  `idProjet` int(11) DEFAULT NULL,
  `codeContrat` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_notes_client`
--

INSERT INTO `t_notes_client` (`id`, `note`, `created`, `idProjet`, `codeContrat`) VALUES
(1, 'Ajouter une fen&ecirc;tre de 2 portes', '2015-02-21', 5, '54d9f1fd8ff9e'),
(2, 'Ajouter une fen&ecirc;tre 2 portes', '2015-02-21', 5, '54d9f1fd8ff9e'),
(3, 'Utiliser la peinture rose pour la chambre des enfants', '2015-02-21', 5, '54e307721ebb6'),
(4, 'Utiliser la peinture rose pour la chambre des enfants+ Changer le sanitaire', '2015-02-26', 5, '54e307721ebb6'),
(5, 'Diviser la pi&egrave;ce 1 en 2', '2015-03-07', 5, '54fae7fb0397c'),
(6, '', '2015-03-07', 5, '54fae84507cce'),
(7, '', '2015-03-11', 5, '5500209c43535');

-- --------------------------------------------------------

--
-- Table structure for table `t_operation`
--

CREATE TABLE `t_operation` (
  `id` int(11) NOT NULL,
  `date` date DEFAULT NULL,
  `montant` decimal(12,2) DEFAULT NULL,
  `modePaiement` varchar(255) DEFAULT NULL,
  `idContrat` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_operation`
--

INSERT INTO `t_operation` (`id`, `date`, `montant`, `modePaiement`, `idContrat`) VALUES
(1, '2014-05-17', '130000.00', NULL, 1),
(2, '2014-05-17', '250000.00', NULL, 2),
(3, '2014-05-17', '450000.00', NULL, 3),
(4, '2014-05-17', '120000.00', NULL, 4),
(5, '2014-05-17', '500000.00', NULL, 5),
(6, '2015-02-13', '14500.00', NULL, 8),
(7, '2015-02-14', '12000.00', NULL, 8),
(10, '2015-03-11', '18000.00', 'Esp&egrave;ces', 8);

-- --------------------------------------------------------

--
-- Table structure for table `t_pieces_appartement`
--

CREATE TABLE `t_pieces_appartement` (
  `id` int(11) NOT NULL,
  `nom` varchar(45) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `idAppartement` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_pieces_appartement`
--

INSERT INTO `t_pieces_appartement` (`id`, `nom`, `url`, `idAppartement`) VALUES
(4, 'Plan 3D', '../MerlaTravERP/pieces/pieces_appartement/54d272da37c8cwork.jpeg', 7);

-- --------------------------------------------------------

--
-- Table structure for table `t_pieces_livraison`
--

CREATE TABLE `t_pieces_livraison` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `url` text DEFAULT NULL,
  `idLivraison` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_pieces_livraison`
--

INSERT INTO `t_pieces_livraison` (`id`, `nom`, `url`, `idLivraison`) VALUES
(1, 'Bon1', '../MerlaTravERP/pieces/pieces_livraison/54e74f8f34b26jobs2.jpeg', 10);

-- --------------------------------------------------------

--
-- Table structure for table `t_pieces_locaux`
--

CREATE TABLE `t_pieces_locaux` (
  `id` int(11) NOT NULL,
  `nom` varchar(45) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `idLocaux` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_pieces_locaux`
--

INSERT INTO `t_pieces_locaux` (`id`, `nom`, `url`, `idLocaux`) VALUES
(2, 'Plan 2D', '../MerlaTravERP/pieces/pieces_locaux/54d1e0a89d069jobs1.png', 3),
(3, 'Plan 3D', '../MerlaTravERP/pieces/pieces_locaux/54d1e0b3e8227work.jpeg', 3),
(4, '', '/pieces/pieces_locaux/54f6cf4f0e0ed10931420_706505662780165_8534792034185423932_n.jpg', 3),
(5, '', '../MerlaTrav/pieces/pieces_locaux/54f6cf922cc7cjobs2.jpeg', 3),
(6, '', '../MerlaTrav/pieces/pieces_locaux/54f6cfa735217drmahmoud.jpg', 2);

-- --------------------------------------------------------

--
-- Table structure for table `t_pieces_terrain`
--

CREATE TABLE `t_pieces_terrain` (
  `id` int(11) NOT NULL,
  `nom` varchar(45) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `idTerrain` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_pieces_terrain`
--

INSERT INTO `t_pieces_terrain` (`id`, `nom`, `url`, `idTerrain`) VALUES
(3, 'Croquis', '../MerlaTravERP/pieces/pieces_terrain/54d0f36e38610drmahmoud.jpg', 2),
(4, 'Plan 3D', '../MerlaTravERP/pieces/pieces_terrain/54d0f51d0ec9balfaraj.jpg', 2);

-- --------------------------------------------------------

--
-- Table structure for table `t_produit`
--

CREATE TABLE `t_produit` (
  `id` int(11) NOT NULL,
  `reference` varchar(255) DEFAULT NULL,
  `prix` decimal(12,2) DEFAULT NULL,
  `description` text DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_produit`
--

INSERT INTO `t_produit` (`id`, `reference`, `prix`, `description`) VALUES
(1, 'CafeRouge', '100.00', ''),
(2, 'CafeNoire', '40.00', 'Caf&eacute; de lux'),
(3, 'Cafe Carte Noire', '120.00', 'Caf&eacute; de lux');

-- --------------------------------------------------------

--
-- Table structure for table `t_projet`
--

CREATE TABLE `t_projet` (
  `id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `adresse` varchar(255) DEFAULT NULL,
  `dateCreation` date DEFAULT NULL,
  `avancementConstruction` int(3) DEFAULT NULL,
  `avancementFinition` int(3) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `createdBy` varchar(50) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `updatedBy` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_projet`
--

INSERT INTO `t_projet` (`id`, `name`, `description`, `adresse`, `dateCreation`, `avancementConstruction`, `avancementFinition`, `created`, `createdBy`, `updated`, `updatedBy`) VALUES
(1, 'Annahda', NULL, NULL, NULL, NULL, NULL, '2016-01-28 00:00:00', 'admin', NULL, NULL),
(2, 'Gaza', NULL, NULL, NULL, NULL, NULL, '2016-01-28 00:00:00', 'admin', NULL, NULL),
(3, 'Rafah', 'Residence Rafah est un projet cree par le Groupe Annahda Lil Iaamar, afin  de repondre au besoins de ses clients dans la region de Nador. ', 'Rue ONDA 12301239 Nador', '2012-08-09', 80, 30, '2016-01-28 00:00:00', 'admin', '2016-05-13 03:15:58', 'admin'),
(4, 'Khattabi', NULL, NULL, NULL, NULL, NULL, '2016-01-28 00:00:00', 'admin', NULL, NULL),
(5, 'Kamar', NULL, NULL, NULL, NULL, NULL, '2016-01-28 00:00:00', 'admin', NULL, NULL),
(6, 'Ahlam', NULL, NULL, NULL, NULL, NULL, '2016-01-28 00:00:00', 'admin', NULL, NULL),
(7, 'Riham', NULL, NULL, NULL, NULL, NULL, '2016-01-28 00:00:00', 'admin', NULL, NULL),
(8, 'Sajed', NULL, NULL, NULL, NULL, NULL, '2016-01-28 00:00:00', 'admin', NULL, NULL),
(9, 'Ghofran', NULL, NULL, NULL, NULL, NULL, '2016-01-28 00:00:00', 'admin', NULL, NULL),
(10, 'Badr', 'Le projet Residence Badr (Annahda 9) est une suite des succ&egrave;s des projets ant&eacute;c&eacute;dants du Groupe Annahda Lil Iaamar', NULL, NULL, NULL, NULL, '2016-01-28 00:00:00', 'admin', NULL, NULL),
(11, 'Al Aksa', 'Le projet Residence Al Aksa (Annahda 10) est une suite des succ&egrave;s des projets ant&eacute;c&eacute;dants du Groupe Annahda Lil Iaamar', NULL, NULL, NULL, NULL, '2016-01-28 00:00:00', 'admin', NULL, NULL),
(12, 'Lina', 'Le projet Residence Lina (Annahda 11) est une suite des succ&egrave;s des projets ant&eacute;c&eacute;dants du Groupe Annahda Lil Iaamar', '', '0000-00-00', 0, 0, '2016-01-28 00:00:00', 'admin', '2017-03-07 05:11:43', 'admin'),
(13, 'Haj Najib', 'Le projet Residence Haj Najib (Annahda 12) est une suite des succ&egrave;s des projets ant&eacute;c&eacute;dants du Groupe Annahda Lil Iaamar', '', '0000-00-00', 0, 0, '2016-01-28 00:00:00', 'admin', '2017-03-07 04:44:45', 'admin'),
(14, 'Jasmin', 'Le projet Residence Jasmin (Annahda 13) est une suite des succ&egrave;s des projets ant&eacute;c&eacute;dants du Groupe Annahda Lil Iaamar', 'ONDA 49 Nador', '2015-02-01', 0, 0, '2016-01-29 06:17:18', 'admin', '2017-03-07 04:45:09', 'admin'),
(15, 'Chaimaae', 'Le projet Residence Chaimaae (Annahda 14) est une suite des succ&egrave;s des projets ant&eacute;c&eacute;dants du Groupe Annahda Lil Iaamar', 'NADOR AL JADID', '0000-00-00', 0, 0, '2017-09-16 11:46:38', 'admin', NULL, NULL),
(16, 'BARAE', 'Le projet Residence Ihssan (Annahda 15) est une suite des succ&egrave;s des projets ant&eacute;c&eacute;dants du Groupe Annahda Lil Iaamar', 'el matar', '0000-00-00', 10, 0, '2017-12-19 04:59:08', 'admin', '2018-02-17 11:13:33', 'admin'),
(17, 'YASSER', 'Le projet Residence Sahar (Annahda 16) est une suite des succ&egrave;s des projets ant&eacute;c&eacute;dants du Groupe Annahda Lil Iaamar', '', '0000-00-00', 0, 0, '2017-12-20 09:56:50', 'admin', '2018-02-17 11:56:16', 'admin'),
(18, 'KARAM', 'Le projet Residence Sahar (Annahda 16) est une suite des succ&egrave;s des projets ant&eacute;c&eacute;dants du Groupe Annahda Lil Iaamar', '', '0000-00-00', 0, 0, '2018-02-17 11:56:36', 'admin', NULL, NULL),
(19, 'ICHRAK', 'Le projet Residence Sahar (Annahda 17) est une suite des succ&egrave;s des projets ant&eacute;c&eacute;dants du Groupe Annahda Lil Iaamar', 'HAY AL MATAR NADOR', '0000-00-00', 0, 0, '2018-02-17 11:57:07', 'admin', NULL, NULL),
(20, 'IHSSAN', 'Le projet Residence Sahar (Annahda 18) est une suite des succ&egrave;s des projets ant&eacute;c&eacute;dants du Groupe Annahda Lil Iaamar', 'HAY AL MATAR NADOR', '0000-00-00', 0, 0, '2018-02-17 11:57:28', 'admin', NULL, NULL),
(21, 'SAHAR', 'Le projet Residence Sahar (Annahda 19) est une suite des succ&egrave;s des projets ant&eacute;c&eacute;dants du Groupe Annahda Lil Iaamar', 'HAY AL MATAR NADOR', '0000-00-00', 0, 0, '2018-02-17 11:57:48', 'admin', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `t_projet_stock`
--

CREATE TABLE `t_projet_stock` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `adresse` varchar(255) DEFAULT NULL,
  `superficie` decimal(10,2) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `budget` decimal(12,2) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_projet_stock`
--

INSERT INTO `t_projet_stock` (`id`, `nom`, `adresse`, `superficie`, `description`, `budget`) VALUES
(1, 'RESSALA 1', 'Quartier Al Matar Nador Jadid', '1027.00', 'Immeuble R+M+4', '27000000.00'),
(2, 'RESSALA 2', 'MATAR SAADA', '927.00', 'immeuble R+4', '22000000.00'),
(3, 'MASJID KABIR', 'NADOR JADID EN FACE MASJID KABIR', '210.00', 'R+4', '7000000.00'),
(5, 'Mekka Trav', 'Rue Trik 80', '900.00', 'Rien de plus', '1000000.00');

-- --------------------------------------------------------

--
-- Table structure for table `t_reglement_fournisseur`
--

CREATE TABLE `t_reglement_fournisseur` (
  `id` int(11) NOT NULL,
  `montant` decimal(12,2) DEFAULT NULL,
  `dateReglement` date DEFAULT NULL,
  `idProjet` int(11) DEFAULT NULL,
  `idFournisseur` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_reglement_fournisseur`
--

INSERT INTO `t_reglement_fournisseur` (`id`, `montant`, `dateReglement`, `idProjet`, `idFournisseur`) VALUES
(1, '12500.00', '2015-02-19', 5, 5),
(3, '4000.00', '2015-02-19', 5, 7),
(4, '1000.00', '2015-02-27', 5, 5),
(5, '1000.00', '2015-02-27', 5, 5),
(6, '1000.00', '2015-02-01', 5, 7),
(7, '1000.00', '2015-03-02', 3, 8),
(8, '5000.00', '2015-03-02', 5, 9),
(9, '900.00', '2015-03-04', 5, 5);

-- --------------------------------------------------------

--
-- Table structure for table `t_salaires_projet`
--

CREATE TABLE `t_salaires_projet` (
  `id` int(11) NOT NULL,
  `salaire` decimal(12,2) DEFAULT NULL,
  `prime` decimal(12,2) DEFAULT NULL,
  `dateOperation` date DEFAULT NULL,
  `idEmploye` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_salaires_projet`
--

INSERT INTO `t_salaires_projet` (`id`, `salaire`, `prime`, `dateOperation`, `idEmploye`) VALUES
(1, '3000.00', '0.00', '2015-03-09', 2);

-- --------------------------------------------------------

--
-- Table structure for table `t_salaires_societe`
--

CREATE TABLE `t_salaires_societe` (
  `id` int(11) NOT NULL,
  `salaire` decimal(12,2) DEFAULT NULL,
  `prime` decimal(12,2) DEFAULT NULL,
  `dateOperation` date DEFAULT NULL,
  `idEmploye` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_salaires_societe`
--

INSERT INTO `t_salaires_societe` (`id`, `salaire`, `prime`, `dateOperation`, `idEmploye`) VALUES
(1, '2000.00', '350.00', '2015-01-01', 2),
(3, '2000.00', '1000.00', '2015-02-02', 2),
(5, '2000.00', '200.00', '2015-02-24', 4),
(7, '2000.00', '1000.00', '2015-02-25', 3),
(8, '2500.00', '1200.00', '2015-02-25', 1),
(9, '2000.00', '0.00', '2015-02-27', 4);

-- --------------------------------------------------------

--
-- Table structure for table `t_sliderimage`
--

CREATE TABLE `t_sliderimage` (
  `id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `url` text DEFAULT NULL,
  `description` text DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `createdBy` varchar(50) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `updatedBy` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_sliderimage`
--

INSERT INTO `t_sliderimage` (`id`, `name`, `url`, `description`, `created`, `createdBy`, `updated`, `updatedBy`) VALUES
(4, '', '/images/slider/slide1.png', '', '2016-01-29 11:04:27', 'admin', NULL, NULL),
(2, '', '/images/slider/slide2.png', '', '2016-01-29 11:07:09', 'admin', NULL, NULL),
(3, '', '/images/slider/slide3.jpg', '', '2016-01-29 11:08:14', 'admin', NULL, NULL),
(1, '', '/images/slider/slide4.png', '', '2016-05-14 00:00:00', 'admin', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `t_slidervideo`
--

CREATE TABLE `t_slidervideo` (
  `id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `url` text DEFAULT NULL,
  `description` text DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `createdBy` varchar(50) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `updatedBy` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_slidervideo`
--

INSERT INTO `t_slidervideo` (`id`, `name`, `url`, `description`, `created`, `createdBy`, `updated`, `updatedBy`) VALUES
(1, 'Video 1', 'https://www.youtube.com/embed/qKXulOrjRCE?autoplay=1&rel=0&loop=1', 'Grouep Annahda Presentation 1', '2016-01-29 11:09:10', 'admin', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `t_stock`
--

CREATE TABLE `t_stock` (
  `id` int(11) NOT NULL,
  `produit` varchar(255) DEFAULT NULL,
  `quantite` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_stock`
--

INSERT INTO `t_stock` (`id`, `produit`, `quantite`) VALUES
(1, '2', 130),
(2, '1', 830),
(3, '3', 530);

-- --------------------------------------------------------

--
-- Table structure for table `t_terrain`
--

CREATE TABLE `t_terrain` (
  `id` int(11) NOT NULL,
  `prix` decimal(12,2) DEFAULT NULL,
  `vendeur` varchar(100) DEFAULT NULL,
  `fraisAchat` decimal(12,2) DEFAULT NULL,
  `superficie` decimal(12,2) DEFAULT NULL,
  `emplacement` text DEFAULT NULL,
  `idProjet` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_terrain`
--

INSERT INTO `t_terrain` (`id`, `prix`, `vendeur`, `fraisAchat`, `superficie`, `emplacement`, `idProjet`) VALUES
(2, '1000000.00', 'Zemzami Oualid', '23000.00', '1500.00', 'Hay S&eacute;louane', 5),
(3, '3240000.00', 'Dergham Abdellah', '12000.00', '1200.00', 'Rue Nador Jadids', 5);

-- --------------------------------------------------------

--
-- Table structure for table `t_user`
--

CREATE TABLE `t_user` (
  `id` int(11) NOT NULL,
  `login` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `created` date NOT NULL,
  `profil` varchar(30) NOT NULL,
  `status` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_user`
--

INSERT INTO `t_user` (`id`, `login`, `password`, `created`, `profil`, `status`) VALUES
(7, 'admin', 'admin', '2016-01-28', 'admin', 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_user_stock`
--

CREATE TABLE `t_user_stock` (
  `id` int(11) NOT NULL,
  `login` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `created` date NOT NULL,
  `profil` varchar(30) NOT NULL,
  `status` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_user_stock`
--

INSERT INTO `t_user_stock` (`id`, `login`, `password`, `created`, `profil`, `status`) VALUES
(1, 'admin', 'admin', '2014-05-17', 'admin', 1),
(2, 'abdou', 'abdou', '2015-01-23', 'admin', 1),
(5, 'mouaad', 'mouaad', '2015-01-24', 'user', 1),
(6, 'asdev', '1234', '2015-01-24', 'admin', 1),
(7, 'obelix', 'obelix', '2015-01-27', 'user', 1),
(9, 'salim', 'salim', '2015-03-15', 'user', 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_video`
--

CREATE TABLE `t_video` (
  `id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `url` text DEFAULT NULL,
  `description` text DEFAULT NULL,
  `idProjet` int(12) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `createdBy` varchar(50) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `updatedBy` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_video`
--

INSERT INTO `t_video` (`id`, `name`, `url`, `description`, `idProjet`, `created`, `createdBy`, `updated`, `updatedBy`) VALUES
(1, 'Video 1', 'zl8NWAXCvEM', 'GROUPE ANNAHDA LIL IAAMAR', 3, '2016-01-29 04:35:17', 'admin', NULL, NULL),
(2, 'Video 2', 'BpudnReRkJM', 'groupe annahda lil iaamar projet 2 hay al matar\r\n', 3, '2016-01-29 04:52:23', 'admin', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `t_appartement`
--
ALTER TABLE `t_appartement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_bien`
--
ALTER TABLE `t_bien`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_caisse_entrees`
--
ALTER TABLE `t_caisse_entrees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_caisse_sorties`
--
ALTER TABLE `t_caisse_sorties`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_client`
--
ALTER TABLE `t_client`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_commande`
--
ALTER TABLE `t_commande`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_config`
--
ALTER TABLE `t_config`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_conge_employe_projet`
--
ALTER TABLE `t_conge_employe_projet`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_conge_employe_societe`
--
ALTER TABLE `t_conge_employe_societe`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_contrat`
--
ALTER TABLE `t_contrat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_employe_projet`
--
ALTER TABLE `t_employe_projet`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_employe_societe`
--
ALTER TABLE `t_employe_societe`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_fournisseur`
--
ALTER TABLE `t_fournisseur`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_image`
--
ALTER TABLE `t_image`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_list_produit`
--
ALTER TABLE `t_list_produit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_livraison`
--
ALTER TABLE `t_livraison`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_locaux`
--
ALTER TABLE `t_locaux`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_mail`
--
ALTER TABLE `t_mail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_notes_client`
--
ALTER TABLE `t_notes_client`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_operation`
--
ALTER TABLE `t_operation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_pieces_appartement`
--
ALTER TABLE `t_pieces_appartement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_pieces_livraison`
--
ALTER TABLE `t_pieces_livraison`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_pieces_locaux`
--
ALTER TABLE `t_pieces_locaux`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_pieces_terrain`
--
ALTER TABLE `t_pieces_terrain`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_produit`
--
ALTER TABLE `t_produit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_projet`
--
ALTER TABLE `t_projet`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_projet_stock`
--
ALTER TABLE `t_projet_stock`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_reglement_fournisseur`
--
ALTER TABLE `t_reglement_fournisseur`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_salaires_projet`
--
ALTER TABLE `t_salaires_projet`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_salaires_societe`
--
ALTER TABLE `t_salaires_societe`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_sliderimage`
--
ALTER TABLE `t_sliderimage`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_slidervideo`
--
ALTER TABLE `t_slidervideo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_stock`
--
ALTER TABLE `t_stock`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_terrain`
--
ALTER TABLE `t_terrain`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_user`
--
ALTER TABLE `t_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_user_stock`
--
ALTER TABLE `t_user_stock`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_video`
--
ALTER TABLE `t_video`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `t_appartement`
--
ALTER TABLE `t_appartement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `t_bien`
--
ALTER TABLE `t_bien`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `t_caisse_entrees`
--
ALTER TABLE `t_caisse_entrees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `t_caisse_sorties`
--
ALTER TABLE `t_caisse_sorties`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `t_client`
--
ALTER TABLE `t_client`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `t_commande`
--
ALTER TABLE `t_commande`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `t_config`
--
ALTER TABLE `t_config`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `t_conge_employe_projet`
--
ALTER TABLE `t_conge_employe_projet`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `t_conge_employe_societe`
--
ALTER TABLE `t_conge_employe_societe`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `t_contrat`
--
ALTER TABLE `t_contrat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `t_employe_projet`
--
ALTER TABLE `t_employe_projet`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `t_employe_societe`
--
ALTER TABLE `t_employe_societe`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `t_fournisseur`
--
ALTER TABLE `t_fournisseur`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `t_image`
--
ALTER TABLE `t_image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=459;

--
-- AUTO_INCREMENT for table `t_list_produit`
--
ALTER TABLE `t_list_produit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `t_livraison`
--
ALTER TABLE `t_livraison`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `t_locaux`
--
ALTER TABLE `t_locaux`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `t_mail`
--
ALTER TABLE `t_mail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `t_notes_client`
--
ALTER TABLE `t_notes_client`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `t_operation`
--
ALTER TABLE `t_operation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `t_pieces_appartement`
--
ALTER TABLE `t_pieces_appartement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `t_pieces_livraison`
--
ALTER TABLE `t_pieces_livraison`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `t_pieces_locaux`
--
ALTER TABLE `t_pieces_locaux`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `t_pieces_terrain`
--
ALTER TABLE `t_pieces_terrain`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `t_produit`
--
ALTER TABLE `t_produit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `t_projet`
--
ALTER TABLE `t_projet`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `t_projet_stock`
--
ALTER TABLE `t_projet_stock`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `t_reglement_fournisseur`
--
ALTER TABLE `t_reglement_fournisseur`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `t_salaires_projet`
--
ALTER TABLE `t_salaires_projet`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `t_salaires_societe`
--
ALTER TABLE `t_salaires_societe`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `t_sliderimage`
--
ALTER TABLE `t_sliderimage`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `t_slidervideo`
--
ALTER TABLE `t_slidervideo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `t_stock`
--
ALTER TABLE `t_stock`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `t_terrain`
--
ALTER TABLE `t_terrain`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `t_user`
--
ALTER TABLE `t_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `t_user_stock`
--
ALTER TABLE `t_user_stock`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `t_video`
--
ALTER TABLE `t_video`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
