<?php
class ListProduit{
    private $_id;
    private $_produit;
    private $_quantite;
    private $_idCommande;
	private $_total;

    //le constructeur
    public function __construct($data){
        $this->hydrate($data);
    }

    //la focntion hydrate sert à attribuer les valeurs en utilisant les setters d'une façon dynamique!
    public function hydrate($data){
        foreach ($data as $key => $value){
            $method = 'set'.ucfirst($key);

            if (method_exists($this, $method)){
                $this->$method($value);
            }
        }
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->_id = $id;
    }

    /**
     * @param mixed $produit
     */
    public function setProduit($produit)
    {
        $this->_produit = $produit;
    }

    /**
     * @param mixed $quantite
     */
    public function setQuantite($quantite)
    {
        $this->_quantite = $quantite;
    }

    /**
     * @param mixed $idCommande
     */
    public function setIdCommande($idCommande)
    {
        $this->_idCommande = $idCommande;
    }
	
	public function setTotal($total)
    {
        $this->_total = $total;
    }

    /**
     * @return mixed
     */
    public function id()
    {
        return $this->_id;
    }

    /**
     * @return mixed
     */
    public function produit()
    {
        return $this->_produit;
    }

    /**
     * @return mixed
     */
    public function quantite()
    {
        return $this->_quantite;
    }

    /**
     * @return mixed
     */
    public function idCommande()
    {
        return $this->_idCommande;
    }
    
    public function total()
    {
        return $this->_total;
    }
}