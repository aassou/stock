<?php
class Commande{
	
    private $_id;
    private $_client;
    private $_dateCommande;
    private $_produit;
    private $_quantite;
    private $_designation;
	private $_code;

    //le constructeur
    public function __construct($data){
        $this->hydrate($data);
    }

    //la focntion hydrate sert à attribuer les valeurs en utilisant les setters d'une façon dynamique!
    public function hydrate($data){
        foreach ($data as $key => $value){
            $method = 'set'.ucfirst($key);

            if (method_exists($this, $method)){
                $this->$method($value);
            }
        }
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->_id = $id;
    }

    /**
     * @param mixed $client
     */
    public function setClient($client)
    {
        $this->_client = $client;
    }

    /**
     * @param mixed $dateCommande
     */
    public function setDateCommande($dateCommande)
    {
        $this->_dateCommande = $dateCommande;
    }

    /**
     * @param mixed $produit
     */
    public function setProduit($produit)
    {
        $this->_produit = $produit;
    }

    /**
     * @param mixed $quantite
     */
    public function setQuantite($quantite)
    {
        $this->_quantite = $quantite;
    }

    /**
     * @param mixed $designation
     */
    public function setDesignation($designation)
    {
        $this->_designation = $designation;
    }
	
	public function setCode($code)
    {
        $this->_code = $code;
    }

    /**
     * @return mixed
     */
    public function id()
    {
        return $this->_id;
    }

    /**
     * @return mixed
     */
    public function client()
    {
        return $this->_client;
    }

    /**
     * @return mixed
     */
    public function dateCommande()
    {
        return $this->_dateCommande;
    }

    /**
     * @return mixed
     */
    public function produit()
    {
        return $this->_produit;
    }

    /**
     * @return mixed
     */
    public function quantite()
    {
        return $this->_quantite;
    }

    /**
     * @return mixed
     */
    public function designation()
    {
        return $this->_designation;
    }
	
	public function code()
    {
        return $this->_code;
    }
    
    

}