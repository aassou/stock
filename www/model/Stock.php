<?php

/**
 * Class Stock
 */
class Stock{

    /**
     * @var int
     */
    private $_id;

    /**
     * @var string
     */
    private $_produit;

    /**
     * @var float
     */
    private $_quantite;

    /**
     * @var string
     */
    private $_unite;

    /**
     * @var int
     */
    private $_idProduit;


    /**
     * Stock constructor.
     * @param $data
     */
    public function __construct($data){
        $this->hydrate($data);
    }

    /**
     * @param $data
     */
    public function hydrate($data){
        foreach ($data as $key => $value){
            $method = 'set'.ucfirst($key);

            if (method_exists($this, $method)){
                $this->$method($value);
            }
        }
    }

    /**
     * @param $id
     */
    public function setId($id)
    {
        $this->_id = $id;
    }

    /**
     * @param $produit
     */
    public function setProduit($produit)
    {
        $this->_produit = $produit;
    }

    /**
     * @param $quantite
     */
    public function setQuantite($quantite)
    {
        $this->_quantite = $quantite;
    }

    /**
     * @param $unite
     */
    public function setUnite($unite)
    {
        $this->_unite = $unite;
    }

    /**
     * @param $idProduit
     */
    public function setIdProduit($idProduit)
    {
        $this->_idProduit = $idProduit;
    }

    /**
     * @return int
     */
    public function id()
    {
        return $this->_id;
    }

    /**
     * @return string
     */
    public function produit()
    {
        return $this->_produit;
    }

    /**
     * @return float
     */
    public function quantite()
    {
        return $this->_quantite;
    }

    /**
     * @return string
     */
    public function unite()
    {
        return $this->_unite;
    }

    /**
     * @return int
     */
    public function idProduit()
    {
        return $this->_idProduit;
    }

}