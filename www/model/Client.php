<?php
class Client{

    //attributes
    private $_id;
    private $_nom;
    private $_numeroTva;
	private $_numeroRegistre;
    private $_email;
    private $_adresse;
    private $_telephone;
    private $_created;
	private $_code;
    
    //le constructeur
    public function __construct($data){
        $this->hydrate($data);
    }
    
    //la focntion hydrate sert à attribuer les valeurs en utilisant les setters d'une façon dynamique!
    public function hydrate($data){
        foreach ($data as $key => $value){
            $method = 'set'.ucfirst($key);
            
            if (method_exists($this, $method)){
                $this->$method($value);
            }
        }
    }
    
    //setters
    public function setId($id){
        $this->_id = $id;
    }
    
    public function setNom($nom){
        $this->_nom = $nom;
    }
    
    public function setAdresse($adresse){
        $this->_adresse = $adresse;
    }
    
    public function setTelephone($telephone){
        $this->_telephone = $telephone;
    }
    
    public function setNumeroTva($numeroTva){
        $this->_numeroTva = $numeroTva;
    }
	
	public function setNumeroRegistre($numeroRegistre){
        $this->_numeroRegistre = $numeroRegistre;
    }
    
    public function setEmail($email){
        $this->_email = $email;
    }
	
    public function setCreated($created){
        $this->_created = $created;
    }
	
	public function setCode($code){
		$this->_code = $code;
	}
    //getters
    
    public function id(){
        return $this->_id;
    }
    
    public function nom(){
        return $this->_nom;
    }
    
    public function adresse(){
        return $this->_adresse;
    }
    
    public function telephone(){
        return $this->_telephone;
    }
    
    public function numeroTva(){
        return $this->_numeroTva;
    }
	
	public function numeroRegistre(){
        return $this->_numeroRegistre;
    }
    
    public function email(){
        return $this->_email;
    }
    
    public function created(){
        return $this->_created;
    }
    
	public function code(){
        return $this->_code;
    }
	    
}