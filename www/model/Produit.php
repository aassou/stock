<?php

/**
 * Class Produit
 */
class Produit{

    /**
     * @var int
     */
    private $_id;

    /**
     * @var string
     */
    private $_reference;

    /**
     * @var float
     */
    private $_prix;

    /**
     * @var string
     */
    private $_description;

    /**
     * @var float
     */
    private $_quantiteMinimale;

    /**
     * @var string
     */
    private $_marque;

    /**
     * @var int
     */
    private $_idFournisseur;

    /**
     * Produit constructor.
     * @param $data
     */
    public function __construct($data){
        $this->hydrate($data);
    }

    /**
     * @param $data
     */
    public function hydrate($data){
        foreach ($data as $key => $value){
            $method = 'set'.ucfirst($key);

            if (method_exists($this, $method)){
                $this->$method($value);
            }
        }
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->_id = $id;
    }

    /**
     * @param mixed $reference
     */
    public function setReference($reference)
    {
        $this->_reference = $reference;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->_description = $description;
    }

    /**
     * @param mixed $prix
     */
    public function setPrix($prix)
    {
        $this->_prix = $prix;
    }

    /**
     * @param $quantiteMinimale
     */
    public function setQuantiteMinimale($quantiteMinimale)
    {
        $this->_quantiteMinimale = $quantiteMinimale;
    }

    /**
     * @param $marque
     */
    public function setMarque($marque)
    {
        $this->_marque = $marque;
    }

    /**
     * @param $idFournisseur
     */
    public function setIdFournisseur($idFournisseur)
    {
        $this->_idFournisseur = $idFournisseur;
    }

    /**
     * @return mixed
     */
    public function id()
    {
        return $this->_id;
    }

    /**
     * @return mixed
     */
    public function prix()
    {
        return $this->_prix;
    }

    /**
     * @return mixed
     */
    public function reference()
    {
        return $this->_reference;
    }

    /**
     * @return mixed
     */
    public function description()
    {
        return $this->_description;
    }

    /**
     * @return float
     */
    public function quantiteMinimale()
    {
        return $this->_quantiteMinimale;
    }

    /**
     * @return string
     */
    public function marque()
    {
        return $this->_marque;
    }

    /**
     * @return int
     */
    public function idFournisseur()
    {
        return $this->_idFournisseur;
    }
}