<?php

/**
 * Class StockManager
 */
class StockManager{

    /**
     * @var string
     */
    private $_db;

    /**
     * StockManager constructor.
     * @param $db
     */
    public function __construct($db){
        $this->_db = $db;
    }

    /**
     * @param Stock $stock
     */
    public function add(Stock $stock){
        $query = $this->_db->prepare("
          INSERT INTO t_stock (produit, quantite, unite, idProduit)
          VALUES (:produit, :quantite, :unite, :idProduit)")
        or die(print_r($this->_db->errorInfo()));
		$query->bindValue(':produit', $stock->produit());
		$query->bindValue(':quantite', $stock->quantite());
        $query->bindValue(':unite', $stock->unite());
        $query->bindValue(':idProduit', $stock->idProduit());
        $query->execute();
        $query->closeCursor();
    }

    /**
     * @param Stock $stock
     */
	public function update(Stock $stock){
		$query = $this->_db->prepare("
          UPDATE t_stock 
          SET produit=:produit, quantite=:quantite, unite=:unite, idProduit=:idProduit 
          WHERE id=:idStock")
		or die(print_r($this->_db->errorInfo()));
		$query->bindValue(':idStock', $stock->id());
		$query->bindValue(':produit', $stock->produit());
		$query->bindValue(':quantite', $stock->quantite());
        $query->bindValue(':unite', $stock->unite());
        $query->bindValue(':idProduit', $stock->idProduit());
        $query->execute();
        $query->closeCursor();
	}

    /**
     * @param Stock $stock
     */
	public function updateStockPlus(Stock $stock){
		$query = $this->_db->prepare("UPDATE t_stock SET quantite=quantite+:quantite WHERE produit=:produit")
		or die(print_r($this->_db->errorInfo()));
		$query->bindValue(':produit', $stock->produit());
		$query->bindValue(':quantite', $stock->quantite());
        $query->execute();
        $query->closeCursor();
	}

    /**
     * @param Stock $stock
     */
	public function updateStockMinus(Stock $stock){
		$query = $this->_db->prepare("UPDATE t_stock SET quantite=quantite-:quantite WHERE produit=:produit")
		or die(print_r($this->_db->errorInfo()));
		$query->bindValue(':produit', $stock->produit());
		$query->bindValue(':quantite', $stock->quantite());
        $query->execute();
        $query->closeCursor();
	}

    /**
     * @param $idStock
     */
	public function delete($idStock){
		$query = $this->_db->prepare("DELETE FROM t_stock WHERE id=:idStock")
		or die(print_r($this->_db->errorInfo()));
		$query->bindValue(':idStock', $idStock);
		$query->execute();
		$query->closeCursor();
	}

    /**
     * @return array
     */
	public function getStocks(){
        $stocks = array();
        $query = $this->_db->query("SELECT * FROM t_stock ORDER BY id DESC");
        //get result
        while($data = $query->fetch(PDO::FETCH_ASSOC)){
            $stocks[] = new Stock($data);
        }
        $query->closeCursor();
        return $stocks;
    }

    /**
     * @param $begin
     * @param $end
     * @return array
     */
    public function getStocksByLimits($begin, $end){
        $stocks = array();
        $query = $this->_db->query("SELECT * FROM t_stock ORDER BY id DESC LIMIT ".$begin." , ".$end);
        //get result
        while($data = $query->fetch(PDO::FETCH_ASSOC)){
            $stocks[] = new Stock($data);
        }
        $query->closeCursor();
        return $stocks;
    }

    /**
     * @return mixed
     */
	public function getStockNumber(){
        $query = $this->_db->query("SELECT COUNT(*) AS StockNumber FROM t_stock")
		or die(print_r($this->_db->errorInfo()));
        $query->execute();
        $data = $query->fetch(PDO::FETCH_ASSOC);
        $query->closeCursor();
        return $data['StockNumber'];
    }

    /**
     * @param $id
     * @return Stock
     */
	public function getStockById($id){
        $query = $this->_db->prepare("SELECT * FROM t_stock WHERE id =:id")
		or die(print_r($this->_db->errorInfo()));
        $query->bindValue(':id', $id);
        $query->execute();
        $data = $query->fetch(PDO::FETCH_ASSOC);
        $query->closeCursor();
        return new Stock($data);
    }

    /**
     * @return mixed
     */
    public function getLastId(){
        $query = $this->_db->query("SELECT id AS last_id FROM t_stock ORDER BY id DESC LIMIT 0, 1");
        $data = $query->fetch(PDO::FETCH_ASSOC);
        $id = $data['last_id'];
        return $id;
    }

    /**
     * @param $produit
     * @return mixed
     */
	public function existsInStock($produit){
        $query = $this->_db->prepare("
          SELECT COUNT(*) 
          FROM t_stock 
          WHERE REPLACE(produit, ' ', '') 
          LIKE REPLACE(:produit, ' ', '') ");
        $query->execute(array(':produit' => $produit));
        return $query->fetchColumn();
    }

    /**
     * @param $produit
     * @return mixed
     */
	public function getQuantite($produit){
		$query = $this->_db->prepare("
          SELECT quantite 
          FROM t_stock 
          WHERE REPLACE(produit, ' ', '') 
          LIKE REPLACE(:produit, ' ', '')");
        $query->execute(array(':produit' => $produit));
        return $query->fetchColumn();
	}
}
