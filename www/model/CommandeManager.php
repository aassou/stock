<?php
class CommandeManager{
	//attributes
    private $_db;
    //constructor
    public function __construct($db){
        $this->_db = $db;
    }
    //CRUD operations
    public function add(Commande $commande){
        $query = $this->_db->prepare(
        'INSERT INTO t_commande (client, dateCommande, code, produit)
        VALUES (:client, :dateCommande, :code, :produit)') 
        or die(print_r($this->_db->errorInfo()));
		$query->bindValue(':client', $commande->client());
		$query->bindValue(':dateCommande', $commande->dateCommande());
		$query->bindValue(':code', $commande->code());
		$query->bindValue(':produit', $commande->produit());
        $query->execute();
        $query->closeCursor();
    }
	
	public function update(Commande $commande){
		$query = $this->_db->prepare('
		UPDATE t_commande SET client=:client, dateCommande=:dateCommande WHERE id=:idCommande') 
		or die(print_r($this->_db->errorInfo()));
		$query->bindValue(':idCommande', $commande->id());
		$query->bindValue(':client', $commande->client());
		$query->bindValue(':dateCommande', $commande->dateCommande());
        $query->execute();
        $query->closeCursor();
	}
	
	public function delete($idCommande){
		$query = $this->_db->prepare('DELETE FROM t_commande WHERE id=:idCommande')
		or die(print_r($this->_db->errorInfo()));
		$query->bindValue(':idCommande', $idCommande);
		$query->execute();
		$query->closeCursor();
	}
	
	public function getCommandes(){
        $commandes = array();
        $query = $this->_db->query('SELECT * FROM t_commande ORDER BY id DESC');
        //get result
        while($data = $query->fetch(PDO::FETCH_ASSOC)){
            $commandes[] = new Commande($data);
        }
        $query->closeCursor();
        return $commandes;
    }
	
	 public function getCommandesWeek(){
        $commandes = array();
        $query = $this->_db->query('SELECT * FROM t_commande WHERE dateCommande BETWEEN SUBDATE(CURDATE(),7) AND CURDATE()');
        //get result
        while($data = $query->fetch(PDO::FETCH_ASSOC)){
            $commandes[] = new Commande($data);
        }
        $query->closeCursor();
        return $commandes;
    }
    
    public function getCommandesByLimits($begin, $end){
        $commandes = array();
        $query = $this->_db->query('SELECT * FROM t_commande ORDER BY id DESC LIMIT '.$begin.' , '.$end);
        //get result
        while($data = $query->fetch(PDO::FETCH_ASSOC)){
            $commandes[] = new Commande($data);
        }
        $query->closeCursor();
        return $commandes;
    }
	
	public function getCommandesByIdClientsByLimits($idClient, $begin, $end){
        $commandes = array();
        $query = $this->_db->prepare('SELECT * FROM t_commande WHERE client=:idClient ORDER BY id DESC LIMIT '.$begin.' , '.$end);
		$query->bindValue(':idClient', $idClient);
		$query->execute();
        //get result
        while($data = $query->fetch(PDO::FETCH_ASSOC)){
            $commandes[] = new Commande($data);
        }
        $query->closeCursor();
        return $commandes;
    }
	
	public function getCommandeNumber(){
        $query = $this->_db->query('SELECT COUNT(*) AS CommandeNumber FROM t_commande')
		or die(print_r($this->_db->errorInfo()));
        $query->execute();
        $data = $query->fetch(PDO::FETCH_ASSOC);
        $query->closeCursor();
        return $data['CommandeNumber'];
    }
	
	public function getCommandeNumberByIdClient($idClient){
        $query = $this->_db->prepare('SELECT COUNT(*) AS CommandeNumber FROM t_commande WHERE client=:idClient')
		or die(print_r($this->_db->errorInfo()));
		$query->bindValue(':idClient', $idClient);
        $query->execute();
        $data = $query->fetch(PDO::FETCH_ASSOC);
        $query->closeCursor();
        return $data['CommandeNumber'];
    }

	public function getCommandeById($id){
        $query = $this->_db->prepare('SELECT * FROM t_commande WHERE id =:id')
		or die(print_r($this->_db->errorInfo()));
        $query->bindValue(':id', $id);
        $query->execute();
        $data = $query->fetch(PDO::FETCH_ASSOC);
        $query->closeCursor();
        return new Commande($data);
    }
	
	public function getCommandeByCode($code){
        $query = $this->_db->prepare('SELECT * FROM t_commande WHERE code =:code')
		or die(print_r($this->_db->errorInfo()));
        $query->bindValue(':code', $code);
        $query->execute();
        $data = $query->fetch(PDO::FETCH_ASSOC);
        $query->closeCursor();
        return new Commande($data);
    }
    
    public function getLastId(){
        $query = $this->_db->query('SELECT id FROM t_commande ORDER BY id DESC LIMIT 0, 1');
        $data = $query->fetch(PDO::FETCH_ASSOC);
        return $data['id'];
    }
	
	public function exists($reference){
        $query = $this->_db->prepare(" SELECT COUNT(*) FROM t_commande WHERE REPLACE(reference, ' ', '') LIKE REPLACE(:reference, ' ', '') ");
        $query->execute(array(':reference' => $reference));
        //get result
        return $query->fetchColumn();
    }
	
	public function existCode($code){
		$query = $this->_db->prepare(" SELECT COUNT(*) FROM t_commande WHERE REPLACE(code, ' ', '') LIKE REPLACE(:code, ' ', '') ");
        $query->execute(array(':code' => $code));
        //get result
        return $query->fetchColumn();
	}
}
