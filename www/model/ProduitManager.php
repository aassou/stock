<?php

/**
 * Class ProduitManager
 */
class ProduitManager{

    /**
     * @var string
     */
    private $_db;

    /**
     * ProduitManager constructor.
     * @param $db
     */
    public function __construct($db){
        $this->_db = $db;
    }

    /**
     * @param Produit $produit
     */
    public function add(Produit $produit){
        $query = $this->_db->prepare("
        INSERT INTO t_produit (reference, prix, description, quantiteMinimale, marque, idFournisseur)
        VALUES (:reference, :prix, :description, :quantiteMinimale, :marque, :idFournisseur)")
        or die(print_r($this->_db->errorInfo()));
		$query->bindValue(':reference', $produit->reference());
		$query->bindValue(':prix', $produit->prix());
		$query->bindValue(':description', $produit->description());
		$query->bindValue(':quantiteMinimale', $produit->quantiteMinimale());
		$query->bindValue(':marque', $produit->marque());
		$query->bindValue(':idFournisseur', $produit->idFournisseur());
        $query->execute();
        $query->closeCursor();
    }

    /**
     * @param Produit $produit
     */
	public function update(Produit $produit){
		$query = $this->_db->prepare('UPDATE t_produit SET reference=:reference, prix=:prix, description=:description, 
        quantiteMinimale=:quantiteMinimale, marque=:marque, idFournisseur=:idFournisseur WHERE id=:idProduit')
		or die(print_r($this->_db->errorInfo()));
		$query->bindValue(':idProduit', $produit->id());
		$query->bindValue(':reference', $produit->reference());
		$query->bindValue(':prix', $produit->prix());
		$query->bindValue(':description', $produit->description());
        $query->bindValue(':quantiteMinimale', $produit->quantiteMinimale());
        $query->bindValue(':marque', $produit->marque());
        $query->bindValue(':idFournisseur', $produit->idFournisseur());
        $query->execute();
        $query->closeCursor();
	}

    /**
     * @param $idProduit
     */
	public function delete($idProduit){
		$query = $this->_db->prepare('DELETE FROM t_produit WHERE id=:idProduit')
		or die(print_r($this->_db->errorInfo()));
		$query->bindValue(':idProduit', $idProduit);
		$query->execute();
		$query->closeCursor();
	}

    /**
     * @return array
     */
	public function getProduits(){
        $produits = array();
        $query = $this->_db->query('SELECT * FROM t_produit ORDER BY id DESC');
        //get result
        while($data = $query->fetch(PDO::FETCH_ASSOC)){
            $produits[] = new Produit($data);
        }
        $query->closeCursor();
        return $produits;
    }

    /**
     * @param $begin
     * @param $end
     * @return array
     */
    public function getProduitsByLimits($begin, $end){
        $produits = array();
        $query = $this->_db->query('SELECT * FROM t_produit ORDER BY id DESC LIMIT '.$begin.' , '.$end);
        //get result
        while($data = $query->fetch(PDO::FETCH_ASSOC)){
            $produits[] = new Produit($data);
        }
        $query->closeCursor();
        return $produits;
    }

    /**
     * @return mixed
     */
	public function getProduitNumber(){
        $query = $this->_db->query('SELECT COUNT(*) AS ProduitNumber FROM t_produit')
		or die(print_r($this->_db->errorInfo()));
        $query->execute();
        $data = $query->fetch(PDO::FETCH_ASSOC);
        $query->closeCursor();
        return $data['ProduitNumber'];
    }

    /**
     * @param $id
     * @return Produit
     */
	public function getProduitById($id){
        $query = $this->_db->prepare('SELECT * FROM t_produit WHERE id =:id')
		or die(print_r($this->_db->errorInfo()));
        $query->bindValue(':id', $id);
        $query->execute();
        $data = $query->fetch(PDO::FETCH_ASSOC);
        $query->closeCursor();
        return new Produit($data);
    }

    /**
     * @return mixed
     */
    public function getLastId(){
        $query = $this->_db->query('SELECT id AS last_id FROM t_produit ORDER BY id DESC LIMIT 0, 1');
        $data = $query->fetch(PDO::FETCH_ASSOC);
        $id = $data['last_id'];
        return $id;
    }

    /**
     * @param $reference
     * @return mixed
     */
	public function exists($reference){
        $query = $this->_db->prepare(" SELECT COUNT(*) 
            FROM t_produit WHERE REPLACE(reference, ' ', '') 
            LIKE REPLACE(:reference, ' ', '') ");
        $query->execute(array(':reference' => $reference));
        return $query->fetchColumn();
    }
}
