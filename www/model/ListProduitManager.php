<?php
class ListProduitManager{
	//attributes
    private $_db;
    //constructor
    public function __construct($db){
        $this->_db = $db;
    }
    //CRUD operations
    public function add(ListProduit $produit){
        $query = $this->_db->prepare(
        'INSERT INTO t_list_produit (produit, quantite, idCommande, total)
        VALUES (:produit, :quantite, :idCommande, :total)') 
        or die(print_r($this->_db->errorInfo()));
		$query->bindValue(':produit', $produit->produit());
		$query->bindValue(':quantite', $produit->quantite());
		$query->bindValue(':idCommande', $produit->idCommande());
		$query->bindValue(':total', $produit->total());
        $query->execute();
        $query->closeCursor();
    }
	
	public function update(ListProduit $produit){
		$query = $this->_db->prepare('
		UPDATE t_list_produit SET produit=:produit, quantite=:quantite, idCommande=:idCommande WHERE id=:id') 
		or die(print_r($this->_db->errorInfo()));
		$query->bindValue(':id', $produit->id());
		$query->bindValue(':produit', $produit->produit());
		$query->bindValue(':quantite', $produit->quantite());
		$query->bindValue(':idCommande', $produit->idCommande());
        $query->execute();
        $query->closeCursor();
	}
	
	public function delete($idProduit){
		$query = $this->_db->prepare('DELETE FROM t_list_produit WHERE id=:id')
		or die(print_r($this->_db->errorInfo()));
		$query->bindValue(':id', $idProduit);
		$query->execute();
		$query->closeCursor();
	}
	
	public function getListProduits(){
        $produits = array();
        $query = $this->_db->query('SELECT * FROM t_list_produit ORDER BY id DESC');
        //get result
        while($data = $query->fetch(PDO::FETCH_ASSOC)){
            $produits[] = new ListProduit($data);
        }
        $query->closeCursor();
        return $produits;
    }
    
    public function getListProduitsByLimits($begin, $end){
        $produits = array();
        $query = $this->_db->query('SELECT * FROM t_list_produit ORDER BY id DESC LIMIT '.$begin.' , '.$end);
        //get result
        while($data = $query->fetch(PDO::FETCH_ASSOC)){
            $produits[] = new ListProduit($data);
        }
        $query->closeCursor();
        return $produits;
    }
	
	public function getListProduitNumber(){
        $query = $this->_db->query('SELECT COUNT(*) AS ProduitNumber FROM t_list_produit')
		or die(print_r($this->_db->errorInfo()));
        $query->execute();
        $data = $query->fetch(PDO::FETCH_ASSOC);
        $query->closeCursor();
        return $data['ProduitNumber'];
    }

	public function getListProduitNumberByIdCommande($idCommande){
        $query = $this->_db->prepare('SELECT COUNT(*) AS ProduitNumber FROM t_list_produit WHERE idCommande=:idCommande')
		or die(print_r($this->_db->errorInfo()));
		$query->bindValue(':idCommande', $idCommande);
        $query->execute();
        $data = $query->fetch(PDO::FETCH_ASSOC);
        $query->closeCursor();
        return $data['ProduitNumber'];
    }

	public function getListProduitById($id){
        $query = $this->_db->prepare('SELECT * FROM t_list_produit WHERE id =:id')
		or die(print_r($this->_db->errorInfo()));
        $query->bindValue(':id', $id);
        $query->execute();
        $data = $query->fetch(PDO::FETCH_ASSOC);
        $query->closeCursor();
        return new ListProduit($data);
    }
	
	public function getTotalListProduitByIdCommande($idCommande){
		$query = $this->_db->prepare('SELECT SUM(total) AS total FROM t_list_produit WHERE idCommande=:idCommande')
		or die(print_r($this->_db->errorInfo()));
        $query->bindValue(':idCommande', $idCommande);
        $query->execute();
        $data = $query->fetch(PDO::FETCH_ASSOC);
        $query->closeCursor();
        return $data['total'];
    }
	
	public function getListProduitByIdCommande($idCommande){
		$listProduits = array();
		$query = $this->_db->prepare('SELECT * FROM t_list_produit WHERE idCommande=:idCommande')
		or die(print_r($this->_db->errorInfo()));
        $query->bindValue(':idCommande', $idCommande);
        $query->execute();
        while($data = $query->fetch(PDO::FETCH_ASSOC)){
        	$listProduits[] = new ListProduit($data);
        }
        $query->closeCursor();
        return $listProduits;
    }
    
    public function getLastId(){
        $query = $this->_db->query('SELECT id AS last_id FROM t_list_produit ORDER BY id DESC LIMIT 0, 1');
        $data = $query->fetch(PDO::FETCH_ASSOC);
        $id = $data['last_id'];
        return $id;
    }
}