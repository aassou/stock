<?php
    include('../autoload.php');
    session_start();
    
    //post input processing
		if( !empty($_POST['libelle']) && !empty($_POST['prixUnitaire']) && !empty($_POST['quantite']) ){
			$idFournisseur = $_POST['fournisseur'];
	        $libelle = htmlentities($_POST['libelle']);    
	        $designation = htmlentities($_POST['designation']);
	        $dateLivraison = htmlentities($_POST['dateLivraison']);
	        $quantite = htmlentities($_POST['quantite']);
	        $prixUnitaire = htmlentities($_POST['prixUnitaire']);
			$codeLivraison = uniqid();
	        //CREATE NEW Livraison object
	        $livraison = new Livraison(array('libelle' => $libelle, 'designation' => $designation, 
	        'dateLivraison' => $dateLivraison, 'prixUnitaire' => $prixUnitaire, 'quantite' => $quantite
	        ,'idFournisseur' => $idFournisseur, 'code' => $codeLivraison));
	        $livraisonManager = new LivraisonManager($pdo);
	        $livraisonManager->add($livraison);
	        $_SESSION['livraison-add-success']='<strong>Opération valide</strong> : La livraison est ajouté avec succès !';
	        header('Location:../view/livraison-add.php');
			exit;
    	}
	    else{
	    	$_SESSION['livraison-add-error'] = "<strong>Erreur Ajout Livraison</strong> : Vous devez remplir au moins les champs 'Libelle', 'Prix unitaire' et 'Quantité'.";
	        header('Location:../view/livraison-add.php');
			exit;
    	}
    
    