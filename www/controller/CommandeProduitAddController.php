<?php
    include('../autoload.php');
    session_start();
    
    //post input processing
    $idCommande = htmlentities($_POST['idCommande']);
	$commandeManager = new CommandeManager($pdo);
	$stockManager = new StockManager($pdo);
	$produitManager = new ProduitManager($pdo);
	$commande = $commandeManager->getCommandeById($idCommande);
	//classModel
	//classManager
	$listProduitManager = new ListProduitManager($pdo);
	$produit = htmlentities($_POST['produit']);
	$quantite = htmlentities($_POST['quantite']);
	$total = $produitManager->getProduitById($produit)->prix()*$quantite;
	if( $stockManager->existsInStock($produit) and $stockManager->getQuantite($produit)>=$quantite ){
		$listProduit = new ListProduit(array('produit' => $produit, 'quantite' => $quantite, 
		'idCommande' => $idCommande, 'total' => $total));
		$listProduitManager->add($listProduit);
		$stock = new Stock(array('produit' => $produit, 'quantite' => $quantite));	
		$stockManager->updateStockMinus($stock);
		$_SESSION['produit-add-success'] = "<strong>Opération valide : </strong>Le produit est ajouté à la commande avec succès est ajouté avec succès.";
	}
	else{
		$_SESSION['produit-add-warning'] = "La quantité commandé <strong>".$quantite."</strong> du produit <strong>".$produitManager->getProduitById($produit)->reference()."</strong> est insuffisante au stock.";
	}
	header("Location:../view/commande-detail.php?idCommande=".$idCommande);
	