<?php
include('../autoload.php');
session_start();

//post input processing
$idStock = $_POST['idStock'];
$stockManager = new StockManager($pdo);
$stockManager->delete($idStock);
$_SESSION['projet-delete-success'] = "<strong>Opération valide : </strong>Projet supprimé avec succès.";
header('Location:../view/stock.php');

