<?php
    include('../autoload.php');
    session_start();
    
    //post input processing
    $codeContrat = $_POST['codeContrat'];
    if(
        !empty($_POST['reference'])
        and !empty($_POST['marque'])
        and !empty($_POST['prix'])
        and !empty($_POST['quantiteMinimale'])
    ) {
        $id = $_POST['idProduit'];
       	$reference = htmlentities($_POST['reference']);
		$prix = htmlentities($_POST['prix']);
		$description = htmlentities($_POST['description']);
        $quantiteMinimale = htmlentities($_POST['quantiteMinimale']);
        $marque = htmlentities($_POST['marque']);
        $idFournisseur = htmlentities($_POST['idFournisseur']);
        //update a Client object
        $produit = new Produit(
            array(
                'id' => $id,
                'reference' => $reference,
                'prix' => $prix,
                'description' => $description,
                'quantiteMinimale' => $quantiteMinimale,
                'marque' => $marque,
                'idFournisseur' => $idFournisseur
            )
        );
        $produitManager = new ProduitManager($pdo);
        $produitManager->update($produit);
        $_SESSION['produit-update-success']="<strong>Opération valide</strong> : Informations du produit <strong>".$reference."</strong> sont modifiées avec succès.";
        header('Location:../view/produits.php#listProduits');
    }
    else{
        $_SESSION['produit-update-error'] = "<strong>Erreur Modification Produit</strong>: Vous devez remplir les champs <strong>'Référence', 'Prix', 'Quantite Minimale' et 'Marque'</strong>.";
        header('Location:../view/produits.php#listProduits');
    }
    
    