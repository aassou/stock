<?php

include('../autoload.php');
session_start();

if(!empty($_POST['quantite']) and !empty($_POST['produit']))
{
    $stockManager = new StockManager($pdo);
    $produit = htmlentities($_POST['produit']);
    $idProduit = htmlentities($_POST['idProduit']);
    $quantite = htmlentities($_POST['quantite']);
    $unite = htmlentities($_POST['unite']);
    $stock = new Stock(array('produit' => $produit, 'quantite' => $quantite, 'unite' => $unite, 'idProduit' => $idProduit));
    if( $stockManager->existsInStock($produit) > 0 ){
        $stockManager->updateStockPlus($stock);
    }
    else{
        $stockManager->add($stock);
    }
    $_SESSION['stock-add-success'] = "<strong>Opération valide : </strong>L'entrée est ajouté au stock avec succès.";
}
else {
    $_SESSION['stock-add-error'] = "<strong>Erreur Ajout Stock</strong> : Vous devez remplir les champs <strong>'Produit' et 'Quantite'</strong>.";
}
header('Location:../view/stock.php');

	