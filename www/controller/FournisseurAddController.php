<?php
    include('../autoload.php');
    session_start();
    //post input processing
    $idProjet = htmlentities($_POST['idProjet']);
	$codeFournisseur = "";
	$fournisseur = "";
	$fournisseurManager = new FournisseurManager($pdo);
	//else we create a new fournisseur
		if( !empty($_POST['nom']) ){    
	        $nom = htmlentities($_POST['nom']);    
			if( $fournisseurManager->exists($nom) ){
				$_SESSION['fournisseur-add-error'] = "<strong>Erreur Ajout Fournisseur : </strong>Un fournisseur existe déjà avec ce nom : ".$nom.".";
		        header('Location:../view/fournisseur-add.php');
				exit;	
			}
			else{
				$adresse = htmlentities($_POST['adresse']);
		        $telephone1 = htmlentities($_POST['telephone1']);
		        $telephone2 = htmlentities($_POST['telephone2']);
		        $email = htmlentities($_POST['email']);
		        $fax = htmlentities($_POST['fax']);
		        $created = date("Y-m-d");
				$codeFournisseur = uniqid();
		        //create a new Fournisseur object
		        $fournisseur = new Fournisseur(array('nom' => $nom, 'adresse' => $adresse,'telephone1' => $telephone1, 
		        'telephone2' =>$telephone2, 'email' => $email, 'fax' => $fax, 'dateCreation' => $created, 'code' => $codeFournisseur));
		        $fournisseurManager = new FournisseurManager($pdo);
		        $fournisseurManager->add($fournisseur);
				$_SESSION['fournisseur-add-success'] = "<strong>Opération valide : </strong> Le fournisseur \"".strtoupper($nom)."\" est ajouté avec succès.";
				header('Location:../view/fournisseur-add.php');
				exit;	
			}
	    }
	    else{
	        $_SESSION['fournisseur-add-error'] = "<strong>Erreur Ajout Fournisseur : </strong>Vous devez remplir au moins le champ 'Nom'.";
			header('Location:../view/fournisseur-add.php');
			exit;
	    }
    