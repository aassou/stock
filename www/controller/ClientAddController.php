<?php
    //classes loading begin
    function classLoad ($myClass) {
        if(file_exists('../model/'.$myClass.'.php')){
            include('../model/'.$myClass.'.php');
        }
        elseif(file_exists('../controller/'.$myClass.'.php')){
            include('../controller/'.$myClass.'.php');
        }
    }
    spl_autoload_register("classLoad"); 
    include('../config.php');  
    //classes loading end
    session_start();
    
    //post input processing
	$codeClient = "";
	//classModel
	$client = "";
	//classManager
	$clientManager = new ClientManager($pdo);
	if( !empty($_POST['nom'])){
		$nom = htmlentities($_POST['nom']);
		if( $clientManager->exists($nom) ){
			$_SESSION['client-add-error'] = "<strong>Erreur Création Client : </strong>Un client existe déjà avec ce nom : <strong>".$nom."</strong> .";
			header('Location:../clients-add.php');
			exit;	
		}   
		else{
			$numeroTva = htmlentities($_POST['numeroTva']);
			$numeroRegistre = htmlentities($_POST['numeroRegistre']);
			$adresse = htmlentities($_POST['adresse']);
			$telephone = htmlentities($_POST['telephone']);
			$email = htmlentities($_POST['email']);
			$codeClient = uniqid().date('YmdHis');
			$created = date('Y-m-d');
			$client = new Client(array('nom' => $nom, 'numeroTva' => $numeroTva, 'numeroRegistre' => $numeroRegistre,
			'adresse' => $adresse, 'telephone' => $telephone, 'email' => $email, 'code' => $codeClient, 
			'created' => $created));
	        $clientManager->add($client);
			$_SESSION['client-add-success'] = "<strong>Opération valide : </strong>Le client <strong>".$nom."</strong> est ajouté avec succès.";
			header('Location:../clients-add.php');
			exit;	
		}
	}
	else{
        $_SESSION['client-add-error'] = "<strong>Erreur Création Client : </strong>Vous devez remplir au moins le champ <strong>'Nom'</strong>.";
		header('Location:../clients-add.php');
		exit;
    }
	