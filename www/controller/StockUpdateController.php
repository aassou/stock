<?php

include('../autoload.php');
session_start();

if(!empty($_POST['quantite']) and !empty($_POST['produit']))
{
    $id = $_POST['idStock'];
    $produit = htmlentities($_POST['produit']);
    $quantite = htmlentities($_POST['quantite']);
    $stock = new Stock(array('id' => $id, 'produit' => $produit, 'quantite' => $quantite));
    $stockManager = new StockManager($pdo);
    $stockManager->update($stock);
    $_SESSION['stock-update-success']="<strong>Opération valide</strong> : Informations du stock sont modifiées avec succès.";
}
else{
    $_SESSION['stock-update-error'] = "<strong>Erreur Modification Stock</strong> : Vous devez remplir les champs <strong>'Produit' et 'Quantite'</strong>.";
}
header('Location:../view/stock.php#list');

