<?php
    //classes loading begin
    function classLoad ($myClass) {
        if(file_exists('../model/'.$myClass.'.php')){
            include('../model/'.$myClass.'.php');
        }
        elseif(file_exists('../controller/'.$myClass.'.php')){
            include('../controller/'.$myClass.'.php');
        }
    }
    spl_autoload_register("classLoad"); 
    include('../config.php');  
    //classes loading end
    session_start();
    
    //post input processing
	$codeClient = "";
	//classModel
	$client = "";
	//classManager
	$commandeManager = new CommandeManager($pdo);
	$client = htmlentities($_POST['client']);
	$dateCommande = htmlentities($_POST['dateCommande']);
	$codeCommande = uniqid().date('YmdHis');
	$nombreProduit = htmlentities($_POST['nombreProduit']);
	$commande = new Commande(array('client' => $client, 'dateCommande' => $dateCommande, 
	'code' => $codeCommande, 'produit' => $nombreProduit));
    $commandeManager->add($commande);
	//$_SESSION['commande-add-success'] = "<strong>Opération valide : </strong>La commande est ajouté avec succès.";
	//$_SESSION['commande-add-success'] = "<strong>Opération valide : </strong>La commande est ajouté avec succès.";
	header('Location:../commande-list-produit.php?codeCommande='.$codeCommande);
	