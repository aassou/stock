<?php
    //classes loading begin
    function classLoad ($myClass) {
        if(file_exists('../model/'.$myClass.'.php')){
            include('../model/'.$myClass.'.php');
        }
        elseif(file_exists('../controller/'.$myClass.'.php')){
            include('../controller/'.$myClass.'.php');
        }
    }
    spl_autoload_register("classLoad"); 
    include('../config.php');  
    //classes loading end
    session_start();
    
    //post input processing
	$idClient = $_POST['idClient'];   
    $clientManager = new ClientManager($pdo);
	$clientManager->delete($idClient);
	$_SESSION['client-delete-success'] = "<strong>Opération valide : </strong>Client supprimé avec succès.";
	header('Location:../clients-add.php#listClients');
    
    