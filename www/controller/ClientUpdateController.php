<?php
    //classes loading begin
    function classLoad ($myClass) {
        if(file_exists('../model/'.$myClass.'.php')){
            include('../model/'.$myClass.'.php');
        }
        elseif(file_exists('../controller/'.$myClass.'.php')){
            include('../controller/'.$myClass.'.php');
        }
    }
    spl_autoload_register("classLoad"); 
    include('../config.php');  
    //classes loading end
    session_start();
    
    //post input processing
    $codeContrat = $_POST['codeContrat'];
    if( !empty($_POST['nom'])){
        $id = $_POST['idClient'];
        $nom = htmlentities($_POST['nom']);    
        $adresse = htmlentities($_POST['adresse']);
        $telephone1 = htmlentities($_POST['telephone']);
        $numeroTva = htmlentities($_POST['numeroTva']);
		$numeroRegistre = htmlentities($_POST['numeroRegistre']);
        $email = htmlentities($_POST['email']);
        //update a Client object
        $client = new Client(array('id' => $id, 'nom' => $nom, 'adresse' => $adresse,'telephone' => $telephone, 
        'numeroRegistre' => $numeroRegistre, 'numeroTva' => $numeroTva, 'email' => $email));
        $clientManager = new ClientManager($pdo);
        $clientManager->update($client);
        $_SESSION['client-update-success']="<strong>Opération valide</strong> : Informations du client <strong>".$nom."</strong> sont modifiées avec succès.";
        header('Location:../clients-add.php#listClients');
    }
    else{
        $_SESSION['client-update-error'] = "<strong>Erreur Modification Client</strong> : Vous devez remplir au moins le champs 'Nom du client'.";
        header('Location:../clients-add.php#listClients');
    }
    