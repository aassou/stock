<?php
    include('../autoload.php');
    session_start();
    
    //post input processing
	$idProduit = $_POST['idProduit'];   
    $produitManager = new ProduitManager($pdo);
	$produitManager->delete($idProduit);
	$_SESSION['produit-delete-success'] = "<strong>Opération valide : </strong>Produit supprimé avec succès.";
	header('Location:../view/produits.php#listProduits');
    
    