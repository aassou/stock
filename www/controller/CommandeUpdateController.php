<?php
    include('../autoload.php');
    session_start();
    
    //post input processing
        $id = htmlentities($_POST['idCommande']);
        $client = htmlentities($_POST['client']);    
        $produit = htmlentities($_POST['produit']);
        $designation = htmlentities($_POST['designation']);
        $quantite = htmlentities($_POST['quantite']);
		$dateCommande = htmlentities($_POST['dateCommande']);
        //update Commande object
        $commande = new Commande(array('id' => $id, 'client' => $client, 'produit' => $produit,
        'designation' => $designation, 'quantite' => $quantite, 'dateCommande' => $dateCommande));
        $commandeManager = new CommandeManager($pdo);
        $commandeManager->update($commande);
        $_SESSION['commande-update-success']="<strong>Opération valide</strong> : Informations de la commande N° <strong>".$id."</strong> sont modifiées avec succès.";
        header('Location:../view/commande-add.php#listCommande');
    