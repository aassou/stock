<?php
    //classes loading begin
    function classLoad ($myClass) {
        if(file_exists('../model/'.$myClass.'.php')){
            include('../model/'.$myClass.'.php');
        }
        elseif(file_exists('../controller/'.$myClass.'.php')){
            include('../controller/'.$myClass.'.php');
        }
    }
    spl_autoload_register("classLoad"); 
    include('../config.php');  
    //classes loading end
    session_start();
    
    //post input processing
    $idCommande = htmlentities($_POST['idCommande']);
	$commandeManager = new CommandeManager($pdo);
	$stockManager = new StockManager($pdo);
	$produitManager = new ProduitManager($pdo);
	$commande = $commandeManager->getCommandeById($idCommande);
	//classModel
	//classManager
	$listProduitManager = new ListProduitManager($pdo);
	$_SESSION['commande-add-warning'] = array();
	for($i=1;$i<=$commande->produit();$i++){
		$produit = htmlentities($_POST['produit'.$i]);
		$quantite = htmlentities($_POST['quantite'.$i]);
		$total = $produitManager->getProduitById($produit)->prix()*$quantite;
		if( $stockManager->existsInStock($produit) and $stockManager->getQuantite($produit)>=$quantite ){
			$listProduit = new ListProduit(array('produit' => $produit, 'quantite' => $quantite, 
			'idCommande' => $idCommande, 'total' => $total));
			$listProduitManager->add($listProduit);
			$stock = new Stock(array('produit' => $produit, 'quantite' => $quantite));	
			$stockManager->updateStockMinus($stock);
			$_SESSION['commande-add-success'] = "<strong>Opération valide : </strong>La commande est ajouté avec succès.";
		}
		else{
			$_SESSION['commande-add-warning'][] = "La quantité commandé <strong>".$quantite."</strong> du produit <strong>".$produitManager->getProduitById($produit)->reference()."</strong> est insuffisante au stock.";
		}
	}
	header("Location:../commande-add.php");
	