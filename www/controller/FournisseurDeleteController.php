<?php
    include('../autoload.php');
    session_start();
    
    //post input processing
	$idFournisseur = $_POST['idFournisseur'];   
    $fournisseurManager = new FournisseurManager($pdo);
	$fournisseurManager->delete($idFournisseur);
	$_SESSION['fournisseur-delete-success'] = "<strong>Opération valide : </strong>Fournisseur supprimé avec succès.";
	header('Location:../view/fournisseur-add.php#listFournisseurs');
    
    