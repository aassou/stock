<?php
    include('../autoload.php');
    session_start();
    if( isset($_SESSION['userCafeManager']) and $_SESSION['userCafeManager']->profil()=="admin" ){
        //classes managers	
        $clientManager = new ClientManager($pdo);
        $commandeManager = new CommandeManager($pdo);
        $produitManager = new ProduitManager($pdo);
		$listProduitManager = new ListProduitManager($pdo);
		//classes and attributes
		$idCommande = $_GET['idCommande'];
        $commande = $commandeManager->getCommandeById($idCommande);
        $client = $clientManager->getClientById($commande->client());
        $produit = $produitManager->getProduitById($commande->produit());
		$listProduit = $listProduitManager->getListProduitByIdCommande($idCommande);
//property data

ob_start();
?>
<style type="text/css">
	h1{
		text-align: center;
		text-decoration: underline;
	}
	table
	{
		width: 100%;	
	}
	.contratSection, .observationSection
	{
		width: 100%;
		border: solid 1px black;
		
	}
	.clienSection
	{
		width: 100%;
		border: solid 1px black;
	}
	.clientHeader
	{
		width: 100%;
		text-align: center;
		font-size: 20pt;
		color: black;
	}
	.head1
	{
		width: 45%;
		font-weight: bold;
	}
	.body1
	{
		width: 55%;
	}
	.head2
	{
		width: 100%;		
	}
	.head3
	{
		width: 45%;
		font-weight: bold;
		
	}
	.body3
	{
		width: 55%;
	}
	.table{
		border: 1px solid black;
	}
</style>
<page backtop="15mm" backbottom="20mm" backleft="10mm" backright="10mm">
    <img src="../view/assets/img/big-logo.png" style="width: 110px" />
    <br><br><br><br><br><br><br>
    <table class="clientHeader">
		<tr>
			<td class="head2">Informations du Client</td>
		</tr>
	</table>
	<br>
	<table class="clienSection">
		<tr>
			<td class="head3">Nom</td>
			<td class="body3">: <?= $client->nom() ?></td>
		</tr>	
		<tr>
			<td class="head3">Adresse</td>
			<td class="body3">: <?= $client->adresse() ?></td>
		</tr>
	</table>
    <h1>Bon de commande N° <?= $commande->id() ?></h1>
    <br><br><br><br><br><br><br>
	<table class="clientHeader">
		<tr>
			<td class="head2">Informations de la commande</td>
		</tr>
		<tr>
			<td class="head1">Date de commande</td>
			<td class="body1">: <?= date('d/m/Y', strtotime($commande->dateCommande())) ?></td>
		</tr>
	</table>
	<br>
    <table class="contratSection">
		<tr class=" table">
			<th style="width:40%">Référence</th>
			<th style="width:20%">Quantité</th>
			<th style="width:20%">Prix Unitaire</th>
			<th style="width:20%">Total</th>
		</tr>
		<?php
		foreach( $listProduit as $list ){
			$produit = $produitManager->getProduitById($list->produit());
		?>
		<tr class=" table">
			<td><?= $produit->reference() ?></td>
			<td><?= $list->quantite() ?></td>
			<td><?= number_format($produit->prix(), '2', ',', ' ') ?>&nbsp;DH</td>
			<td><?= number_format($list->total(), '2', ',', ' ') ?>&nbsp;DH</td>
		</tr>
		<?php
		}
		?>
	</table>
	<br>
	<h2>Total de la commande : <?= number_format($listProduitManager->getTotalListProduitByIdCommande($commande->id()), '2', ',', ' ') ?>&nbsp;DH</h2>
    <br><br>
    <br><br>
    <page_footer>
    <hr/>
    <p style="text-align: center;font-size: 9pt;">STE XXX SARL : Au capital de 100 000,00 DH – , Nador.
    	<br>Tèl 0536ytrh601818 / 06616zer68860 IF : 404zerzer51179   RC : 109qsdqsd99  Patente 56126sdqsd681</p>
    </page_footer>
</page>    
<?php
    $content = ob_get_clean();
    
    require('../lib/html2pdf/html2pdf.class.php');
    try{
        $pdf = new HTML2PDF('P', 'A4', 'fr');
        $pdf->pdf->SetDisplayMode('fullpage');
        $pdf->writeHTML($content);
		$fileName = "BonDeCommande-".$client->nom().'-'.date('dmY', strtotime($commande->dateCommande())).date('His').'.pdf';
       	$pdf->Output($fileName);
    }
    catch(HTML2PDF_exception $e){
        die($e->getMessage());
    }
}
else{
    header("Location:index.php");
}
?>
