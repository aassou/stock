<?php
    //classes loading begin
    function classLoad ($myClass) {
        if(file_exists('../model/'.$myClass.'.php')){
            include('../model/'.$myClass.'.php');
        }
        elseif(file_exists('../controller/'.$myClass.'.php')){
            include('../controller/'.$myClass.'.php');
        }
    }
    spl_autoload_register("classLoad"); 
    include('../config.php');  
    //classes loading end
    session_start();
    
    //post input processing
	$idCommande = $_POST['idCommande'];   
    $commandeManager = new CommandeManager($pdo);
	$commandeManager->delete($idCommande);
	$_SESSION['commande-delete-success'] = "<strong>Opération valide : </strong>Commande supprimé avec succès.";
	header('Location:../commande-add.php#listCommande');
    
    