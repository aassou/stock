<?php
    include('../autoload.php');
    session_start();
    
    //post input processing
	//classModel
	//classManager
	$produitManager = new ProduitManager($pdo);
	if(
	    !empty($_POST['reference'])
        and !empty($_POST['marque'])
        and !empty($_POST['prix'])
        and !empty($_POST['quantiteMinimale'])
    ) {
		$reference = htmlentities($_POST['reference']);
		if( $produitManager->exists($reference) ){
			$_SESSION['produit-add-error'] = "<strong>Erreur Création Produit : </strong>Un produit existe déjà avec cette référence : <strong>".$reference."</strong> .";
			header('Location:../view/produits.php');
			exit;	
		}   
		else{
			$prix = htmlentities($_POST['prix']);
			$description = htmlentities($_POST['description']);
			$quantiteMinimale = htmlentities($_POST['quantiteMinimale']);
			$marque = htmlentities($_POST['marque']);
			$idFournisseur = htmlentities($_POST['idFournisseur']);
			$produit = new Produit(
			    array(
			        'reference' => $reference,
                    'prix' => $prix,
                    'description' => $description,
                    'quantiteMinimale' => $quantiteMinimale,
                    'marque' => $marque,
                    'idFournisseur' => $idFournisseur
                )
            );
	        $produitManager->add($produit);
			$_SESSION['produit-add-success'] = "<strong>Opération valide : </strong>Le produit <strong>".$reference."</strong> est ajouté avec succès.";
			header('Location:../view/produits.php');
			exit;	
		}
	}
	else{
        $_SESSION['produit-add-error'] = "<strong>Erreur Création Produit : </strong>Vous devez remplir les champs <strong>'Référence', 'Prix', 'Quantite Minimale' et 'Marque'</strong>.";
		header('Location:../view/produits.php');
		exit;
    }
	