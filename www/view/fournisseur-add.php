<?php
    include('../autoload.php');
	include('../lib/pagination.php');
    //classes loading end
    session_start();
    if(isset($_SESSION['userCafeManager']) and $_SESSION['userCafeManager']->profil()=="admin"){
    	$fournisseurManager = new FournisseurManager($pdo);
		$fournisseurNumber = $fournisseurManager->getFournisseurNumbers();
		if( $fournisseurNumber != 0 ){
			$fournisseurPerPage = 10;
	        $pageNumber = ceil($fournisseurNumber/$fournisseurPerPage);
	        $p = 1;
	        if(isset($_GET['p']) and ($_GET['p']>0 and $_GET['p']<=$pageNumber)){
	            $p = $_GET['p'];
	        }
	        else{
	            $p = 1;
	        }
	        $begin = ($p - 1) * $fournisseurPerPage;
			$fournisseurs = $fournisseurManager->getFournisseursByLimits($begin, $fournisseurPerPage);
	        $pagination = paginate('fournisseur-add.php', '?p=' , $pageNumber, $p);	
		}
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<?php include('include/header.php') ?>
<body class="fixed-top">
	<div class="header navbar navbar-inverse navbar-fixed-top">
		<?php include("include/top-menu.php"); ?>
	</div>
	<div class="page-container row-fluid sidebar-closed">
		<?php include("include/sidebar.php"); ?>
		<div class="page-content">
			<div class="container-fluid">
                <div class="row-fluid"><div class="span12"></div></div>
				<div class="row-fluid">
					<div class="span12">
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="dashboard.php">Accueil</a>
								<i class="icon-angle-right"></i>
							</li>
							<li>
                                <i class="icon-group"></i>
                                <a><strong>Gestion des fournisseurs</strong></a>
							</li>
						</ul>
					</div>
				</div>
				<div class="row-fluid">
					<div class="span12">
						<div class="tab-pane active" id="tab_1">
	                         <?php if(isset($_SESSION['fournisseur-add-error'])){ ?>
	                         	<div class="alert alert-error">
									<button class="close" data-dismiss="alert"></button>
									<?= $_SESSION['fournisseur-add-error'] ?>		
								</div>
	                         <?php } 
	                         	unset($_SESSION['fournisseur-add-error']);
	                         ?>
	                          <?php if(isset($_SESSION['fournisseur-add-success'])){ ?>
	                         	<div class="alert alert-success">
									<button class="close" data-dismiss="alert"></button>
									<?= $_SESSION['fournisseur-add-success'] ?>		
								</div>
	                         <?php } 
	                         	unset($_SESSION['fournisseur-add-success']);
	                         ?>
                           <div class="portlet box grey">
                              <div class="portlet-title">
                                 <h4><i class="icon-edit"></i>Nouveau Fournisseur</h4>
                                 <div class="tools">
                                    <a href="javascript:;" class="collapse"></a>
                                    <a href="javascript:;" class="remove"></a>
                                 </div>
                              </div>
                              <div class="portlet-body form">
                                 <!-- BEGIN FORM-->
                                 <form action="../controller/FournisseurAddController.php" method="POST" class="horizontal-form">
                                    <div class="row-fluid">
                                       <div class="span4">
                                          <div class="control-group autocomplet_container">
                                             <label class="control-label" for="nom">Nom</label>
                                             <div class="controls">
                                                <input type="text" id="nomFournisseur" name="nom" class="m-wrap span12" onkeyup="autocompletFournisseur()">
                                                <ul id="fournisseurList"></ul>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="span4">
                                          <div class="control-group">
                                             <label class="control-label" for="adresse">Adresse</label>
                                             <div class="controls">
                                                <input type="text" id="adresse" name="adresse" class="m-wrap span12">
                                             </div>
                                          </div>
                                       </div>
                                       <div class="span4">
                                          <div class="control-group">
                                             <label class="control-label" for="telephone1">Téléphone 1</label>
                                             <div class="controls">
                                                <input type="text" id="telephone1" name="telephone1" class="m-wrap span12">
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="row-fluid">
                                    	<div class="span4">
                                          <div class="control-group">
                                             <label class="control-label" for="telephone2">Téléphone 2</label>
                                             <div class="controls">
                                                <input type="text" id="telephone2" name="telephone2" class="m-wrap span12">
                                             </div>
                                          </div>
                                       </div>
                                       <div class="span4">
                                          <div class="control-group">
                                             <label class="control-label" for="email">Email</label>
                                             <div class="controls">
                                                <input type="text" id="email" name="email" class="m-wrap span12">
                                             </div>
                                          </div>
                                       </div>
                                       <div class="span4">
                                          <div class="control-group">
                                             <label class="control-label" for="fax">Fax</label>
                                             <div class="controls">
                                                <input type="text" id="fax" name="fax" class="m-wrap span12">
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="form-actions">
                                       	<button type="submit" class="btn black">Enregistrer <i class="icon-save"></i></button>
                                       	<button type="reset" class="btn red">Annuler</button>
                                    </div>
                                 </form>
                                 <!-- END FORM--> 
                              </div>
                           </div>
                        </div>
                   	</div>
                   </div>
                   <div class="row-fluid">
					<div class="span12">
						<?php if(isset($_SESSION['fournisseur-update-success'])){ ?>
                         	<div class="alert alert-success">
								<button class="close" data-dismiss="alert"></button>
								<?= $_SESSION['fournisseur-update-success'] ?>		
							</div>
                         <?php } 
                         	unset($_SESSION['fournisseur-update-success']);
                         ?>
                         <?php if(isset($_SESSION['fournisseur-delete-success'])){ ?>
                         	<div class="alert alert-success">
								<button class="close" data-dismiss="alert"></button>
								<?= $_SESSION['fournisseur-delete-success'] ?>		
							</div>
                         <?php } 
                         	unset($_SESSION['fournisseur-delete-success']);
                         ?>
                         <?php if(isset($_SESSION['fournisseur-update-error'])){ ?>
                         	<div class="alert alert-error">
								<button class="close" data-dismiss="alert"></button>
								<?= $_SESSION['fournisseur-update-error'] ?>		
							</div>
                         <?php } 
                         	unset($_SESSION['fournisseur-update-error']);
                         ?>
                        <div class="portlet" id="listFournisseurs">
							<div class="portlet-title">
								<h4><i class="icon-calendar"></i>Liste des fournisseurs</h4>
								<div class="tools">
									<a href="javascript:;" class="collapse"></a>
									<a href="javascript:;" class="remove"></a>
								</div>
							</div>
							<div class="portlet-body">
								<table class="table table-striped table-bordered table-advance table-hover">
									<thead>
										<tr>
											<th>Nom</th>
											<th>Adresse</th>
											<th>Tél.1</th>
											<th>Tél.2</th>
											<th>Modifier</th>
											<th>Supprimer</th>
										</tr>
									</thead>
									<tbody>
									<?php
									foreach($fournisseurs as $fournisseur){
									?>
										<tr>
											<td><a><?= $fournisseur->nom() ?></a></td>
											<td><?= $fournisseur->adresse() ?></td>
											<td><?= $fournisseur->telephone1() ?></td>
											<td><?= $fournisseur->telephone2() ?></td>
											<td class="hidden-phone">
												<a class="btn mini green" href="#update<?= $fournisseur->id();?>" data-toggle="modal" data-id="<? $fournisseur->id(); ?>">
													<i class="icon-refresh "></i>
												</a>
											</td>
											<td class="hidden-phone">
												<a class="btn mini red" href="#delete<?= $fournisseur->id();?>" data-toggle="modal" data-id="<? $fournisseur->id(); ?>">
													<i class="icon-remove "></i>
												</a>
											</td>
										</tr>
										<!-- update box begin-->
										<div id="update<?= $fournisseur->id();?>" class="modal hide fade in" tabindex="-1" role="dialog" aria-labelledby="login" aria-hidden="false" >
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
												<h3>Modifier Infos Fournisseur</h3>
											</div>
											<div class="modal-body">
												<form class="form-horizontal" action="../controller/FournisseurUpdateController.php" method="post" enctype="multipart/form-data">
													<p>Êtes-vous sûr de vouloir modifier les infos du fournisseur <strong><?= $fournisseur->nom() ?></strong> ?</p>
													<div class="control-group">
														<label class="control-label">Nom</label>
														<div class="controls">
															<input type="text" name="nom" value="<?= $fournisseur->nom() ?>" />
														</div>
													</div>
													<div class="control-group">
														<label class="control-label">Adresse</label>
														<div class="controls">
															<input type="text" name="adresse" value="<?= $fournisseur->adresse() ?>" />
														</div>
													</div>
													<div class="control-group">
														<label class="control-label">Téléphone1</label>
														<div class="controls">
															<input type="text" name="telephone1" value="<?= $fournisseur->telephone1() ?>" />
														</div>	
													</div>
													<div class="control-group">
														<label class="control-label">Téléphone2</label>
														<div class="controls">
															<input type="text" name="telephone2" value="<?= $fournisseur->telephone2() ?>" />
														</div>	
													</div>
													<div class="control-group">
														<label class="control-label">Email</label>
														<div class="controls">
															<input type="text" name="email" value="<?= $fournisseur->email() ?>" />
														</div>	
													</div>
													<div class="control-group">
														<label class="control-label">Fax</label>
														<div class="controls">
															<input type="text" name="fax" value="<?= $fournisseur->fax() ?>" />
														</div>	
													</div>
													<div class="control-group">
														<input type="hidden" name="idFournisseur" value="<?= $fournisseur->id() ?>" />
														<div class="controls">	
															<button class="btn" data-dismiss="modal"aria-hidden="true">Non</button>
															<button type="submit" class="btn red" aria-hidden="true">Oui</button>
														</div>
													</div>
												</form>
											</div>
										</div>
										<!-- update box end -->	
										<!-- delete box begin-->
										<div id="delete<?= $fournisseur->id();?>" class="modal hide fade in" tabindex="-1" role="dialog" aria-labelledby="login" aria-hidden="false" >
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
												<h3>Supprimer Fournisseur</h3>
											</div>
											<div class="modal-body">
												<form class="form-horizontal loginFrm" action="../controller/FournisseurDeleteController.php" method="post">
													<p>Êtes-vous sûr de vouloir supprimer ce fournisseur <strong><?= $fournisseur->nom() ?></strong> ?</p>
													<div class="control-group">
														<label class="right-label"></label>
														<input type="hidden" name="idFournisseur" value="<?= $fournisseur->id() ?>" />
														<button class="btn" data-dismiss="modal"aria-hidden="true">Non</button>
														<button type="submit" class="btn red" aria-hidden="true">Oui</button>
													</div>
												</form>
											</div>
										</div>
										<!-- delete box end -->	
									<?php
									}
									?>
									</tbody><?=	$pagination;?>
								</table>
							</div>
						</div>
					</div>
				</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php include('include/footer.php') ?>
    <?php include('include/scripts.php') ?>
	<script>
		jQuery(document).ready(function() {			
			// initiate layout and plugins
			//App.setPage("table_editable");
			App.init();
		});
	</script>
</body>
</html>
<?php
}
else if(isset($_SESSION['userCafeManager']) and $_SESSION->profil()!="user"){
	header('Location:dashboard.php');
}
else{
    header('Location:index.php');    
}
?>