<?php
//classes loading begin
    function classLoad ($myClass) {
        if(file_exists('model/'.$myClass.'.php')){
            include('model/'.$myClass.'.php');
        }
        elseif(file_exists('controller/'.$myClass.'.php')){
            include('controller/'.$myClass.'.php');
        }
    }
    spl_autoload_register("classLoad"); 
    include('config.php');  
	include('lib/pagination.php');
    //classes loading end
    session_start();
    if(isset($_SESSION['userCafeManager']) and $_SESSION['userCafeManager']->profil()=="admin"){
    	$commandeManager = new CommandeManager($pdo);
		$clientManager = new ClientManager($pdo);
		$produitManager = new ProduitManager($pdo);
		$produits = $produitManager->getProduits();
    	if( isset($_GET['codeCommande'])and  $commandeManager->existCode($_GET['codeCommande'])){
    		$codeCommande = $_GET['codeCommande'];
    		$commande = $commandeManager->getCommandeByCode($codeCommande);
    	}
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<meta charset="utf-8" />
	<title>NadoCaf - Management Application</title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
	<link href="assets/css/metro.css" rel="stylesheet" />
	<link href="assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
	<link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
	<link href="assets/css/style.css" rel="stylesheet" />
	<link href="assets/css/style_responsive.css" rel="stylesheet" />
	<link href="assets/css/style_default.css" rel="stylesheet" id="style_color" />
	<link href="assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="assets/uniform/css/uniform.default.css" />
	<link rel="stylesheet" type="text/css" href="assets/chosen-bootstrap/chosen/chosen.css" />
	<link rel="stylesheet" href="assets/data-tables/DT_bootstrap.css" />
	<link rel="stylesheet" type="text/css" href="assets/uniform/css/uniform.default.css" />
	<link rel="stylesheet" type="text/css" href="assets/bootstrap-datepicker/css/datepicker.css" />
	<link rel="shortcut icon" href="favicon.ico" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
	<!-- BEGIN HEADER -->
	<div class="header navbar navbar-inverse navbar-fixed-top">

		<?php include("include/top-menu.php"); ?>	
		<!-- END TOP NAVIGATION BAR -->
	</div>
	<!-- END HEADER -->
	<!-- BEGIN CONTAINER -->
	<div class="page-container row-fluid">
		<!-- BEGIN SIDEBAR -->
		<?php include("include/sidebar.php"); ?>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN PAGE CONTAINER-->			
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
						<h3 class="page-title">
							Gestion des commandes
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a>Accueil</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li>
								<i class="icon-table"></i>
								<a>Gestion des Commandes</a>
								<i class="icon-angle-right"></i>
							</li>
							<li>
								<a>Nouvelle Commande</a>
								<i class="icon-angle-right"></i>
							</li>
							<li>
								<a>Liste des produits</a>
							</li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div class="row-fluid">
					<div class="span12">
						<div class="tab-pane active" id="tab_1">
							<?php if(isset($_SESSION['commande-add-success'])){ ?>
	                         	<div class="alert alert-success">
									<button class="close" data-dismiss="alert"></button>
									<?= $_SESSION['commande-add-success'] ?>		
								</div>
	                         <?php } 
	                         	unset($_SESSION['commande-add-success']);
	                         ?>
	                         <?php if(isset($_SESSION['commande-add-error'])){ ?>
	                         	<div class="alert alert-error">
									<button class="close" data-dismiss="alert"></button>
									<?= $_SESSION['commande-add-error'] ?>		
								</div>
	                         <?php } 
	                         	unset($_SESSION['commande-add-error']);
	                         ?>
                           <div class="portlet box grey">
                              <div class="portlet-title">
                                 <h4><i class="icon-edit"></i>Nouvelle Liste des produits</h4>
                                 <div class="tools">
                                    <a href="javascript:;" class="collapse"></a>
                                    <a href="javascript:;" class="remove"></a>
                                 </div>
                              </div>
                              <div class="portlet-body form">
                                 <!-- BEGIN FORM-->
                                 <form action="../controller/CommandeListProduitAddController.php" method="POST" class="horizontal-form" -->
                                    <div class="row-fluid">
                                       <div class="span4">
                                          <div class="control-group autocomplet_container">
                                             <label class="control-label" for="client"><strong>Client</strong></label>
                                             <div class="controls">
                                             	<input class="m-wrap" value="<?= $clientManager->getClientById($commande->client())->nom() ?>" disabled="disabled" />   
                                             </div>
                                          </div>
                                       </div>
                                    	<div class="span4">
                                          <div class="control-group">
                                             <label class="control-label" for="telephone"><strong>Date de commande</strong></label>
                                             <div class="controls">
                                                <div class="input-append date date-picker" data-date="" data-date-format="yyyy-mm-dd">
				                                    <input class="m-wrap m-ctrl-small date-picker" value="<?= $commande->dateCommande() ?>" disabled="disabled" />
				                                 </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="span4">
                                          <div class="control-group">
                                             <label class="control-label" for="telephone"><strong>Nombre de produit</strong></label>
                                             <div class="controls">
				                                    <input class="m-wrap" disabled="disabled" value="<?= $commande->produit() ?>" />
				                                 </div>
                                             </div>
                                          </div>
                                       </div>
                                       <table class="table table-striped table-bordered table-hover">
                                       	<tr>
                                       		<th>Produit</th>
											<th>Quantité</th>
                                       	</tr>
                                       <?php
                                       for($i=1;$i<=$commande->produit();$i++){
                                       ?>
                                       	<tr>
                                       		<td>
                                       			<select name="produit<?= $i ?>" class="m-wrap">
                                             		<?php
                                             		foreach( $produits as $produit ){
                                             		?>
                                             		<option value="<?= $produit->id() ?>"><?= $produit->reference() ?></option>
                                             		<?php
                                             		}
                                             		?>
                                             	</select>   
                                       		</td>
											<td>
												<input type="text" name="quantite<?= $i ?>" class="m-wrap span12" />
											</td>
                                       	</tr>
                                       <?php
                                       }
                                       ?>
                                       </table>
                                    <div class="form-actions">
                                    	<input type="hidden" name="idCommande" value="<?= $commande->id() ?>" />
                                    	<button type="submit" class="btn black">Enregistrer <i class="icon-save"></i></button>
                                       	<button type="reset" class="btn red">Annuler</button>
                                    </div>
                                 </form>
                                 <!-- END FORM--> 
                              </div>
                           </div>
                        </div>
						</div>
					</div>
				</div>
				<!-- END PAGE CONTENT -->
			</div>
			<!-- END PAGE CONTAINER-->
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	<div class="footer">
		2015 &copy; MerlaTravERP. Management Application.
		<div class="span pull-right">
			<span class="go-top"><i class="icon-angle-up"></i></span>
		</div>
	</div>
	<!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS -->
	<!-- Load javascripts at bottom, this will reduce page load time -->
	<script src="assets/js/jquery-1.8.3.min.js"></script>
	<script src="assets/breakpoints/breakpoints.js"></script>
	<script src="assets/bootstrap/js/bootstrap.min.js"></script>
	<script src="assets/js/jquery.blockui.js"></script>
	<script src="assets/js/jquery.cookie.js"></script>
	<script src="assets/fancybox/source/jquery.fancybox.pack.js"></script>
	<script type="text/javascript" src="assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
	<script type="text/javascript" src="assets/bootstrap-daterangepicker/date.js"></script>
	<!-- ie8 fixes -->
	<!--[if lt IE 9]>
    <script src="assets/js/excanvas.js"></script>
    <script src="assets/js/respond.js"></script>
    <![endif]-->
	<script type="text/javascript" src="assets/uniform/jquery.uniform.min.js"></script>
	<script type="text/javascript" src="assets/data-tables/jquery.dataTables.js"></script>
	<script type="text/javascript" src="assets/data-tables/DT_bootstrap.js"></script>
	<script src="assets/js/app.js"></script>
	<script type="text/javascript" src="script.js"></script>		
	<script>
		/*jQuery(document).ready(function() {			
			// initiate layout and plugins
			App.setPage("table_editable");
			App.init();
		});*/
	</script>
</body>
<!-- END BODY -->
</html>
<?php
}
else if(isset($_SESSION['userCafeManager']) and $_SESSION->profil()!="admin"){
	header('Location:dashboard.php');
}
else{
    header('Location:index.php');    
}
?>