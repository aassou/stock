<?php
    include('../autoload.php');
    include('../lib/pagination.php');
    session_start();
    if(isset($_SESSION['userCafeManager']) and $_SESSION['userCafeManager']->profil()=="admin"){
    	$commandeManager = new CommandeManager($pdo);
		$clientManager = new ClientManager($pdo);
		$produitManager = new ProduitManager($pdo);
		$listProduitManager = new ListProduitManager($pdo);
		$produits = $produitManager->getProduits();
    	if( isset( $_GET['idCommande'] ) 
    	and ( $_GET['idCommande'] > 0 and $_GET['idCommande'] <= $commandeManager->getLastId()  
			) 
		){
    		$idCommande = $_GET['idCommande'];
    		$commande = $commandeManager->getCommandeById($idCommande);
			$client = $clientManager->getClientById($commande->client());
			$listProduitNumber = $listProduitManager->getListProduitNumberByIdCommande($idCommande);
			if( $listProduitNumber>0 ){
				$listProduits = $listProduitManager->getListProduitByIdCommande($idCommande);	
			}
    	}
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<?php include('include/header.php'); ?>
<body class="fixed-top">
	<!-- BEGIN HEADER -->
	<div class="header navbar navbar-inverse navbar-fixed-top">

		<?php include("include/top-menu.php"); ?>	
		<!-- END TOP NAVIGATION BAR -->
	</div>
	<!-- END HEADER -->
	<!-- BEGIN CONTAINER -->
	<div class="page-container row-fluid sidebar-closed">
		<!-- BEGIN SIDEBAR -->
		<?php include("include/sidebar.php"); ?>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN PAGE CONTAINER-->			
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
						<h3 class="page-title">
							Gestion des commandes
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a>Accueil</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li>
								<i class="icon-table"></i>
								<a>Gestion des Commandes</a>
								<i class="icon-angle-right"></i>
							</li>
							<li>
								<a>Détails de la commande</a>
							</li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div class="row-fluid">
					<div class="span12">
						<div class="tab-pane active" id="tab_1">
							<?php if(isset($_SESSION['produit-add-success'])){ ?>
	                         	<div class="alert alert-success">
									<button class="close" data-dismiss="alert"></button>
									<?= $_SESSION['produit-add-success'] ?>		
								</div>
	                         <?php } 
	                         	unset($_SESSION['produit-add-success']);
	                         ?>
	                         <?php if(isset($_SESSION['produit-delete-success'])){ ?>
	                         	<div class="alert alert-success">
									<button class="close" data-dismiss="alert"></button>
									<?= $_SESSION['produit-delete-success'] ?>		
								</div>
	                         <?php } 
	                         	unset($_SESSION['produit-delete-success']);
	                         ?>
	                         <?php if(isset($_SESSION['produit-add-warning'])){ ?>
	                         	<div class="alert alert-warning">
									<button class="close" data-dismiss="alert"></button>
									<?= $_SESSION['produit-add-warning'] ?>		
								</div>
	                         <?php } 
	                         	unset($_SESSION['produit-add-warning']);
	                         ?>
                           <div class="portlet">
                              <div class="portlet-title">
                                 <h4><i class="icon-barcode"></i>Liste des produits</h4>
                                 <div class="tools">
                                    <a href="javascript:;" class="collapse"></a>
                                    <a href="javascript:;" class="remove"></a>
                                 </div>
                              </div>
                              <div class="portlet-body form">
                                 <!-- BEGIN FORM-->
                                    <div class="row-fluid">
                                       <div class="span4">
                                          <div class="control-group autocomplet_container">
                                             <label class="control-label" for="client"><strong>Client</strong></label>
                                             <div class="controls">
                                             	<input class="m-wrap" value="<?= $client->nom() ?>" disabled="disabled" />   
                                             </div>
                                          </div>
                                       </div>
                                    	<div class="span4">
                                          <div class="control-group">
                                             <label class="control-label" for="telephone"><strong>Date de commande</strong></label>
                                             <div class="controls">
                                                <div class="input-append date date-picker" data-date="" data-date-format="yyyy-mm-dd">
				                                    <input class="m-wrap m-ctrl-small date-picker" value="<?= $commande->dateCommande() ?>" disabled="disabled" />
				                                 </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="span4">
                                          <div class="control-group">
                                             <label class="control-label" for="telephone"><strong>Nombre de produit</strong></label>
                                             <div class="controls">
				                                    <input class="m-wrap" disabled="disabled" value="<?= $commande->produit() ?>" />
				                                 </div>
                                             </div>
                                          </div>
                                       </div>
                                       <a class="btn blue" href="#addProduit" data-toggle="modal" data-id="">
											Ajouter un produit <i class="icon-plus "></i>
										</a>
										<br /><br />
										<!-- addProduit box begin-->
										<div id="addProduit" class="modal hide fade in" tabindex="-1" role="dialog" aria-labelledby="login" aria-hidden="false" >
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
												<h3>Ajouter un produit à la commande </h3>
											</div>
											<div class="modal-body">
												<form class="form-horizontal" action="../controller/CommandeProduitAddController.php" method="post">
													<div class="control-group">
														<label class="control-label">Produit</label>
														<div class="controls">
															<select name="produit" class="m-wrap">
			                                             		<?php
			                                             		foreach( $produits as $produit ){
			                                             		?>
			                                             		<option value="<?= $produit->id() ?>"><?= $produit->reference() ?></option>
			                                             		<?php
			                                             		}
			                                             		?>
			                                             	</select>   
														</div>
													</div>
			                                          <div class="control-group">
			                                             <label class="control-label" for="quantite">Quantité</label>
			                                             <div class="controls">
							                                    <input name="quantite" class="m-wrap" type="text" value="" />
							                                 </div>
			                                             </div>
			                                          </div>
													<div class="control-group">
														<input type="hidden" name="idCommande" value="<?= $commande->id() ?>" />
														<div class="controls">	
															<button class="btn" data-dismiss="modal"aria-hidden="true">Non</button>
															<button type="submit" class="btn red" aria-hidden="true">Oui</button>
														</div>
													</div>
												</form>
											</div>
										</div>
										<!-- updatecommande box end -->
                                       <?php
                                       if( $listProduitNumber>0 ){
                                       ?>
                                       <table class="table table-striped table-bordered table-hover">
	                                   	<tr>
	                                   		<th>Produit</th>
											<th>Quantité</th>
											<th>Total</th>
											<th>Modifier</th>
											<th>Supprimer</th>
	                                   	</tr>
                                       <?php
                                       foreach($listProduits as $listProduit){
                                       	$produit = $produitManager->getProduitById($listProduit->produit());
                                       ?>
                                       	<tr>
                                       		<td>   
                                       			<?= $produit->reference() ?>
                                       		</td>
											<td>
												<?= $listProduit->quantite() ?>
											</td>
											<td>
												<?= number_format($listProduit->total(), '2', ',', ' ') ?>&nbsp;DH
											</td>
											<td class="hidden-phone">
												<a class="btn mini green" href="#update<?= $listProduit->id();?>" data-toggle="modal" data-id="<? $listProduit->id(); ?>">
													<i class="icon-refresh "></i>
												</a>
											</td>
											<td class="hidden-phone">
												<a class="btn mini red" href="#delete<?= $listProduit->id();?>" data-toggle="modal" data-id="<? $listProduit->id(); ?>">
													<i class="icon-remove "></i>
												</a>
											</td>
                                       	</tr>
                                       	<!-- updateFournisseur box begin-->
										<div id="update<?= $listProduit->id() ?>" class="modal hide fade in" tabindex="-1" role="dialog" aria-labelledby="login" aria-hidden="false" >
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
												<h3>Modifier les informations du commande </h3>
											</div>
											<div class="modal-body">
												<form class="form-horizontal" action="../controller/CommandeListProduitUpdateController.php" method="post">
													<p>Êtes-vous sûr de vouloir modifier les infos de la liste des produits N° <strong><?= $listProduit->id() ?></strong> ?</p>
													<div class="control-group">
														<label class="control-label">Produit</label>
														<div class="controls">
															<select name="produit" class="m-wrap">
			                                             		<option selected="selected" value="<?= $listProduit->produit() ?>">
			                                                		<?= $produitManager->getProduitById($listProduit->produit())->reference() ?>
			                                                	</option>
			                                                	<option disabled="disabled">-----------------</option>
			                                             		<?php
			                                             		foreach( $produits as $produit ){
			                                             		?>
			                                             		<option value="<?= $produit->id() ?>"><?= $produit->reference() ?></option>
			                                             		<?php
			                                             		}
			                                             		?>
			                                             	</select>   
														</div>
													</div>
			                                          <div class="control-group">
			                                             <label class="control-label" for="quantite">Quantité</label>
			                                             <div class="controls">
							                                    <input name="quantite" class="m-wrap" type="text" value="<?= $listProduit->quantite() ?>" />
							                                 </div>
			                                             </div>
			                                          </div>
													<div class="control-group">
														<input type="hidden" name="idCommande" value="<?= $commande->id() ?>" />
														<input type="hidden" name="idListProduit" value="<?= $listProduit->id() ?>" />
														<div class="controls">	
															<button class="btn" data-dismiss="modal"aria-hidden="true">Non</button>
															<button type="submit" class="btn red" aria-hidden="true">Oui</button>
														</div>
													</div>
												</form>
											</div>
											</div>
										</div>
										<!-- updatecommande box end -->
										<!-- delete box begin-->
										<div id="delete<?= $listProduit->id();?>" class="modal hide fade in" tabindex="-1" role="dialog" aria-labelledby="login" aria-hidden="false" >
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
												<h3>Supprimer List des produits</h3>
											</div>
											<div class="modal-body">
												<form class="form-horizontal loginFrm" action="../controller/CommandeListProduitDeleteController.php" method="post">
													<p>Êtes-vous sûr de vouloir supprimer la liste des produits N° <strong><?= $listProduit->id() ?></strong> ?</p>
													<div class="control-group">
														<label class="right-label"></label>
														<input type="hidden" name="idCommande" value="<?= $commande->id() ?>" />
														<input type="hidden" name="idListProduit" value="<?= $listProduit->id() ?>" />
														<button class="btn" data-dismiss="modal"aria-hidden="true">Non</button>
														<button type="submit" class="btn red" aria-hidden="true">Oui</button>
													</div>
												</form>
											</div>
										</div>
										<!-- delete box end -->		
                                       <?php
                                       }//end foreach
                                       ?>
                                       </table>
                                       <?php
									   }//end if
                                       ?>
                                       <a class="btn big black">Total de la commande = <?= number_format($listProduitManager->getTotalListProduitByIdCommande($idCommande), '2', ',', ' ') ?></a>
                                 <!-- END FORM--> 
                              </div>
                           </div>
                        </div>
						</div>
					</div>
				</div>
				<!-- END PAGE CONTENT -->
			</div>
			<!-- END PAGE CONTAINER-->
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	<div class="footer">
		2015 &copy; MerlaTravERP. Management Application.
		<div class="span pull-right">
			<span class="go-top"><i class="icon-angle-up"></i></span>
		</div>
	</div>
	<!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS -->
	<!-- Load javascripts at bottom, this will reduce page load time -->
	<script src="assets/js/jquery-1.8.3.min.js"></script>
	<script src="assets/breakpoints/breakpoints.js"></script>
	<script src="assets/bootstrap/js/bootstrap.min.js"></script>
	<script src="assets/js/jquery.blockui.js"></script>
	<script src="assets/js/jquery.cookie.js"></script>
	<script src="assets/fancybox/source/jquery.fancybox.pack.js"></script>
	<script type="text/javascript" src="assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
	<script type="text/javascript" src="assets/bootstrap-daterangepicker/date.js"></script>
	<!-- ie8 fixes -->
	<!--[if lt IE 9]>
    <script src="assets/js/excanvas.js"></script>
    <script src="assets/js/respond.js"></script>
    <![endif]-->
	<script type="text/javascript" src="assets/uniform/jquery.uniform.min.js"></script>
	<script type="text/javascript" src="assets/data-tables/jquery.dataTables.js"></script>
	<script type="text/javascript" src="assets/data-tables/DT_bootstrap.js"></script>
	<script src="assets/js/app.js"></script>
	<script type="text/javascript" src="script.js"></script>		
	<script>
		/*jQuery(document).ready(function() {			
			// initiate layout and plugins
			App.setPage("table_editable");
			App.init();
		});*/
	</script>
</body>
<!-- END BODY -->
</html>
<?php
}
else if(isset($_SESSION['userCafeManager']) and $_SESSION->profil()!="admin"){
	header('Location:dashboard.php');
}
else{
    header('Location:index.php');    
}
?>