<?php
    include('../autoload.php');
	include('../lib/pagination.php');
    //classes loading end
    session_start();
    if(isset($_SESSION['userCafeManager']) and $_SESSION['userCafeManager']->profil()=="admin"){
    	//les sources
		$employeManager = new EmployeSocieteManager($pdo);
		$projetManager = new ProjetManager($pdo);
		$caisseEntreesManager = new CaisseEntreesManager($pdo);
		$caisseSortiesManager = new CaisseSortiesManager($pdo);
		$projets = $projetManager->getProjets();
		$employes = "";
		//test the employeSociete object number: if exists get terrain else do nothing
		$employeNumber = $employeManager->getEmployeSocieteNumber();
		if($employeNumber!=0){
			$employeSocietePerPage = 10;
	        $pageNumber = ceil($employeNumber/$employeSocietePerPage);
	        $p = 1;
	        if(isset($_GET['p']) and ($_GET['p']>0 and $_GET['p']<=$pageNumber)){
	            $p = $_GET['p'];
	        }
	        else{
	            $p = 1;
	        }
	        $begin = ($p - 1) * $employeSocietePerPage;
	        $pagination = paginate('employes-societe.php', '?p=', $pageNumber, $p);
			$employesSociete = $employeManager->getEmployesSocieteByLimits($begin, $employeSocietePerPage);	
	}
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<?php include('include/header.php') ?>
<body class="fixed-top">
	<div class="header navbar navbar-inverse navbar-fixed-top">
		<?php include("include/top-menu.php"); ?>
	</div>
	<div class="page-container row-fluid sidebar-closed">
		<?php include("include/sidebar.php"); ?>
		<div class="page-content">
			<div class="container-fluid">
                <div class="row-fluid"><div class="span12"></div></div>
				<div class="row-fluid">
					<div class="span12">
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="dashboard.php">Accueil</a>
								<i class="icon-angle-right"></i>
							</li>
							<li>
								<i class="icon-money"></i>
                                <a><strong>Gestion de la caisse</strong></a>
							</li>
						</ul>
					</div>
				</div>
				<div class="row-fluid">
					<div class="span12 responsive" data-tablet="span12" data-desktop="span12">
						<div class="dashboard-stat purple">
							<div class="visual">
								<i class="icon-money"></i>
							</div>
							<div class="details">
								<div class="number">
									<?= $caisseEntreesManager->getTotalCaisseEntrees()-$caisseSortiesManager->getTotalCaisseSorties() ?>
									DH
								</div>
								<div class="desc">									
									Bilan de la caisse<br> (Les Entrées - Les Sorties)
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row-fluid">
					<div class="span12">
						<div class="tab-pane active" id="tab_1">
							<?php if(isset($_SESSION['entrees-add-success'])){ ?>
                         	<div class="alert alert-success">
								<button class="close" data-dismiss="alert"></button>
								<?= $_SESSION['entrees-add-success'] ?>		
							</div>
	                         <?php } 
	                         	unset($_SESSION['entrees-add-success']);
	                         ?>
	                         <?php if(isset($_SESSION['entrees-add-error'])){ ?>
                         	<div class="alert alert-error">
								<button class="close" data-dismiss="alert"></button>
								<?= $_SESSION['entrees-add-error'] ?>		
							</div>
	                         <?php } 
	                         	unset($_SESSION['entrees-add-error']);
	                         ?>
                           <div class="portlet box grey">
                              <div class="portlet-title">
                                 <h4><i class="icon-signin"></i>Gestion des Entreés de la Caisse</h4>
                                 <div class="tools">
                                    <a href="javascript:;" class="collapse"></a>
                                    <a href="javascript:;" class="remove"></a>
                                 </div>
                              </div>
                              <div class="portlet-body form">
									<form action="../controller/CaisseEntreesAddController.php" method="POST" class="horizontal-form">
	                                 <div class="row-fluid">	
	                                 	<div class="span3">
                                          <div class="control-group">
                                             <label class="control-label" for="montant">Montant</label>
                                             <div class="controls">
                                                <input type="text" id="montant" name="montant" class="m-wrap span12" placeholder="">
                                             </div>
                                          </div>
	                                 	</div>
	                                 	<div class="span3">
                                          <div class="control-group">
                                          	<label class="control-label" for="designation">Désignation</label>
                                             <div class="controls">
                                                <input type="text" id="designation" name="designation" class="m-wrap span12" placeholder="">
                                             </div>
                                          </div>
	                                 	</div>
	                                 	<div class="span3 ">
                                          <div class="control-group">
                                          	<label class="control-label" for="dateOperation">Date opération</label>
                                             <div class="controls">
    											<div class="input-append date date-picker" data-date="" data-date-format="yyyy-mm-dd">
				                                    <input name="dateOperation" id="dateOperation" class="m-wrap m-ctrl-small date-picker" type="text" value="<?= date('Y-m-d') ?>" />
				                                    <span class="add-on"><i class="icon-calendar"></i></span>
				                                </div>
                                             </div>
                                          </div>
                                       </div>
	                                 </div>
	                                 <div class="row-fluid">
	                                 	<div class="span3">
	                                          <div class="control-group">
	                                             <div class="controls">
	                                                <input type="submit" class="m-wrap span12 btn black" value="OK +">
	                                                <input type="hidden" name="utilisateur" value="<?= $_SESSION['userCafeManager']->login() ?>">
	                                             </div>
	                                          </div>
	                                 	</div>
	                                 </div>
                                 </form>
								<hr>
								<a class="btn blue big">Les entrées=<?= $caisseEntreesManager->getTotalCaisseEntrees() ?></a>
								<a class="btn yellow big pull-right" href="caisse-entrees.php">
									Détails des entrées 
									<i class="m-icon-big-swapright m-icon-white"></i>									
								</a>
								<br><br><br><br>
                              </div>
                           </div>
                                 <!-- END FORM--> 
                              </div>
						<div class="tab-pane active" id="tab_1">
							<?php if(isset($_SESSION['sorties-add-success'])){ ?>
                         	<div class="alert alert-success">
								<button class="close" data-dismiss="alert"></button>
								<?= $_SESSION['sorties-add-success'] ?>		
							</div>
	                         <?php } 
	                         	unset($_SESSION['sorties-add-success']);
	                         ?>
	                         <?php if(isset($_SESSION['sorties-add-error'])){ ?>
                         	<div class="alert alert-error">
								<button class="close" data-dismiss="alert"></button>
								<?= $_SESSION['sorties-add-error'] ?>		
							</div>
	                         <?php } 
	                         	unset($_SESSION['sorties-add-error']);
	                         ?>
                           <div class="portlet box grey">
                              <div class="portlet-title">
                                 <h4><i class="icon-signout"></i>Gestion des Sorties de la Caisse</h4>
                                 <div class="tools">
                                    <a href="javascript:;" class="collapse"></a>
                                    <a href="javascript:;" class="remove"></a>
                                 </div>
                              </div>
                              <div class="portlet-body form">
                                 <!-- BEGIN FORM-->
                                 <!-- BEGIN FORM Entrées Mohamed-->
									<form action="../controller/CaisseSortiesAddController.php" method="POST" class="horizontal-form">
	                                 <div class="row-fluid">	
	                                 	<div class="span3">
                                          <div class="control-group">
                                             <label class="control-label" for="montant">Montant</label>
                                             <div class="controls">
                                                <input type="text" id="montant" name="montant" class="m-wrap span12" placeholder="">
                                             </div>
                                          </div>
	                                 	</div>
	                                 	<div class="span4">
                                          <div class="control-group">
                                          	<label class="control-label" for="designation">Désignation</label>
                                             <div class="controls">
                                                <input type="text" id="designation" name="designation" class="m-wrap span12" placeholder="">
                                             </div>
                                          </div>
	                                 	</div>
	                                 	<div class="span3">
                                          <div class="control-group">
                                          	<label class="control-label" for="dateOperation">Date opération</label>
                                             <div class="controls">
    											<div class="input-append date date-picker" data-date="" data-date-format="yyyy-mm-dd">
				                                    <input name="dateOperation" id="dateOperation" class="m-wrap m-ctrl-small date-picker" type="text" value="<?= date('Y-m-d') ?>" />
				                                    <span class="add-on"><i class="icon-calendar"></i></span>
				                                </div>
                                             </div>
                                          </div>
                                       </div>
	                                 </div>
	                                 <div class="row-fluid">
	                                 	<div class="span4">
	                                      <div class="control-group">
	                                      	<label class="control-label" for="destination">Pour</label>
	                                         <div class="controls">
	                                         	<select style="width:200px" name="destination" style="width:200px" class="m-wrap">
	                                         		<option value="Bureau">Bureau</option>
	                                         		<?php
	                                         		foreach($projets as $projet){
	                                         		?>
	                                         		<option value="<?= $projet->id() ?>"><?= $projet->nom() ?></option>	
	                                         		<?php	
	                                         		}
	                                         		?>	
	                                         	</select>		
	                                         </div>
	                                      </div>
	                                 	</div>
	                                 	<div class="span3">
	                                          <div class="control-group">
	                                          	 <label>&nbsp;</label>
	                                             <div class="controls">
	                                                <input type="submit" class="m-wrap span12 btn black" value="OK -">
	                                                <input type="hidden" name="utilisateur" value="<?= $_SESSION['userCafeManager']->login() ?>">
	                                             </div>
	                                          </div>
	                                 	</div>
	                                 </div>
                                 </form>
                                 <!-- END FORM Entrées Mohamed-->
                                 <hr>
                                 <a class="btn green big">Les sorties=<?= $caisseSortiesManager->getTotalCaisseSorties() ?></a>
                                 <a class="btn yellow big pull-right" href="caisse-sorties.php">
									Détails des sorties 
									<i class="m-icon-big-swapright m-icon-white"></i>									
								</a>
								<br><br><br><br>
                              </div>
                           </div>
							<!-- END Charges TABLE PORTLET-->
                                 <!-- END FORM--> 
                              </div>
                           </div>
                        </div>
					</div>
				</div>
			</div>
		</div>
	</div>
    <?php include('include/footer.php') ?>
	<?php include('include/scripts.php') ?>
	<script>jQuery(document).ready(function(){
			//App.setPage("table_editable");
			App.init();
		});
	</script>
</body>
</html>
<?php
}
else if(isset($_SESSION['userCafeManager']) and $_SESSION->profil()!="admin"){
	header('Location:dashboard.php');
}
else{
    header('Location:index.php');    
}
?>