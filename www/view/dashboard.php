<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
    include('../autoload.php');
    //classes loading end
    session_start();
    if(isset($_SESSION['userCafeManager'])){
    	//classes managers
		$usersManager = new UserManager($pdo);
		$mailsManager = new MailManager($pdo);
		$produitManager = new ProduitManager($pdo);
		$stockManager = new StockManager($pdo);
		$clientManager = new ClientManager($pdo);
		$contratManager = new ContratManager($pdo);
		$livraisonsManager = new LivraisonManager($pdo);
		$fournisseursManager = new FournisseurManager($pdo);
		$caisseManager = new CaisseManager($pdo);
		$commandeManager = new CommandeManager($pdo);
		$operationsManager = new OperationManager($pdo);
		//classes and vars
		//users number
        $stockNumber = $stockManager->getStockNumber();
		$produitNumber = ($produitManager->getProduitNumber());
		$fournisseurNumber = $fournisseursManager->getFournisseurNumbers();
		$usersNumber = $usersManager->getUsersNumber();
		$mailsNumberToday = $mailsManager->getMailsNumberToday();
		$mailsToday = $mailsManager->getMailsToday();
		$clientWeek = $clientManager->getClientsWeek();
		$clientNumberWeek = $clientManager->getClientsNumberWeek();
		$livraisonsWeek = $livraisonsManager->getLivraisonsWeek();
		$livraisonsNumberWeek = $livraisonsManager->getLivraisonsNumberWeek();
		$livraisonsNumber = $livraisonsManager->getLivraisonNumber();
		$operationsNumberWeek = $operationsManager->getOperationNumberWeek();
		$commandesWeek = $commandeManager->getCommandesWeek();
		$soldeCaisse = $caisseManager->getTotalCaisseByType('Entree')
            - $caisseManager->getTotalCaisseByType('Sortie');
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<?php include('include/header.php') ?>
<body class="fixed-top">
	<div class="header navbar navbar-inverse navbar-fixed-top">
		<?php include("include/top-menu.php"); ?>
	</div>
	<div class="page-container row-fluid sidebar-closed">
		<?php include("include/sidebar.php"); ?>
		<div class="page-content">
			<div class="container-fluid">
				<div class="row-fluid"><div class="span12"></div></div>
				<div class="row-fluid">
					<div class="span12">
                        <ul class="breadcrumb">
                            <li>
                                <i class="icon-home"></i>
                                <a><strong>Accueil</strong></a>
                            </li>
                        </ul>
						<div class="tiles">
							<a href="stock.php">
							<div class="tile bg-green">
								<div class="tile-body">
									<i class="icon-bar-chart"></i>
								</div>
								<div class="tile-object">
									<div class="name">
										Stock
									</div>
									<div class="number">
										
									</div>
								</div>
							</div>
							</a>
                            <a href="produits.php">
                                <div class="tile bg-dark-blue">
                                    <div class="corner"></div>
                                    <div class="tile-body">
                                        <i class="icon-barcode"></i>
                                    </div>
                                    <div class="tile-object">
                                        <div class="name">
                                            Produits
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <a href="commandes.php">
                                <div class="tile bg-purple">
                                    <div class="corner"></div>
                                    <div class="tile-body">
                                        <i class="icon-shopping-cart"></i>
                                    </div>
                                    <div class="tile-object">
                                        <div class="name">
                                            Commandes
                                        </div>
                                    </div>
                                </div>
                            </a>
							<a href="fournisseur-add.php">
							<div class="tile bg-blue">
								<div class="corner"></div>
								<div class="tile-body">
									<i class="icon-group"></i>
								</div>
								<div class="tile-object">
									<div class="name">
										Fournisseurs
									</div>
								</div>
							</div>
							</a>
							</a>
							<a href="livraison-add.php">
							<div class="tile bg-grey">
								<div class="tile-body">
									<i class="icon-truck"></i>
								</div>
								<div class="tile-object">
									<div class="name">
										Livraisons
									</div>
								</div>
							</div>
							</a>
                            <a href="caisse-group.php">
                                <div class="tile bg-dark-cyan">
                                    <div class="tile-body">
                                        <i class="icon-money"></i>
                                    </div>
                                    <div class="tile-object">
                                        <div class="name">
                                            Caisse
                                        </div>
                                    </div>
                                </div>
                            </a>
							<a href="cheques.php">
							<div class="tile bg-red">
								<div class="tile-body">
									<i class="icon-credit-card"></i>
								</div>
								<div class="tile-object">
									<div class="name">
										Cheques
									</div>
								</div>
							</div>
							</a>
                            <a href="configuration.php">
                                <div class="tile bg-dark-black">
                                    <div class="tile-body">
                                        <i class="icon-wrench"></i>
                                    </div>
                                    <div class="tile-object">
                                        <div class="name">
                                            Parametrages
                                        </div>
                                    </div>
                                </div>
                            </a>
						</div>
					</div>
				</div>
                <br>
                <ul class="breadcrumb">
                    <li>
                        <i class="icon-bar-chart"></i>
                        <a><strong>Bilans et Statistiques Pour Cette Semaine</strong></a>
                    </li>
                </ul>
				<div class="row-fluid">
					<a href="clients-add.php">
					<div class="span3 responsive" data-tablet="span6  fix-offset" data-desktop="span3">
						<div class="dashboard-stat yellow">
							<div class="visual">
								<i class="icon-group"></i>
							</div>
							<div class="details">
								<div class="number">+<?= $clientNumberWeek ?></div>
								<div class="desc">Clients</div>
							</div>			
						</div>
					</div>	
					</a>
					<div class="span3 responsive" data-tablet="span6" data-desktop="span3">
						<a class="more" href="caisse.php">
						<div class="dashboard-stat purple">
							<div class="visual">
								<i class="icon-money"></i>
							</div>
							<div class="details">
								<div class="number">
									<?= number_format($soldeCaisse, '2', ',', ' ') ?> DH
								</div>
								<div class="desc">Bilan de la caisse</div>
							</div>					
						</div>
						</a>
					</div>
                    <div class="span3 responsive" data-tablet="span6" data-desktop="span3">
                        <a class="more" href="caisse.php">
                            <div class="dashboard-stat blue">
                                <div class="visual">
                                    <i class="icon-barcode"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <?= $produitNumber ?>
                                    </div>
                                    <div class="desc">Type de Produits</div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="span3 responsive" data-tablet="span6" data-desktop="span3">
                        <a class="more" href="caisse.php">
                            <div class="dashboard-stat green">
                                <div class="visual">
                                    <i class="icon-bar-chart"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <?= $stockNumber ?>
                                    </div>
                                    <div class="desc">Produits en stock</div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
				<div class="row-fluid">
				<div class="span12">
					<div class="portlet paddingless">
                        <ul class="breadcrumb">
                            <li>
                                <i class="icon-bar-chart"></i>
                                <a><strong>Nouveautés</strong></a>
                            </li>
                        </ul>
						<div class="portlet-body">
							<div class="tabbable tabbable-custom">
								<ul class="nav nav-tabs">
									<li class="active"><a href="#tab_1_1" data-toggle="tab">Les commandes de la semaine</a></li>
									<li><a href="#tab_1_2" data-toggle="tab">Les livraisons de la semaine</a></li>
									<li><a href="#tab_1_3" data-toggle="tab">Les clients de la semaine</a></li>
									<li><a href="#tab_1_4" data-toggle="tab">Les messages d'aujourd'hui</a></li>
								</ul>
								<div class="tab-content">
									<div class="tab-pane active" id="tab_1_1">
										<div class="scroller" data-height="290px" data-always-visible="1" data-rail-visible1="1">
											<ul class="feeds">
												<?php
												foreach($commandesWeek as $commandes){
													$client = $clientManager->getClientById($commandes->client());
												?>
												<li>
													<div class="col1">
														<div class="cont">
															<div class="cont-col1">
																<div class="label label-success">								
																	<i class="icon-bell"></i>
																</div>
															</div>
															<div class="cont-col2">
																<div class="desc">	
																	<a href="commande-detail.php?idCommande=<?= $commandes->id() ?>" target="_blank">
																		<strong>Référence</strong> : <?= $commandes->id() ?>
																	</a><br>
																	<strong>Date</strong> : <?= $commandes->dateCommande() ?><br>
																	<strong>Client</strong> : <?= $client->nom() ?><br><br> 
																</div>
															</div>
														</div>
													</div>
													<div class="col2">
														<div class="date">
															
														</div>
													</div>
												</li>
												<hr>
												<?php 
												}
												?>
											</ul>
										</div>
									</div>
									<div class="tab-pane" id="tab_1_2">
										<div class="scroller" data-height="290px" data-always-visible="1" data-rail-visible1="1">
											<ul class="feeds">
												<?php
												foreach($livraisonsWeek as $livraison){
												?>
												<li>
													<div class="col1">
														<div class="cont">
															<div class="cont-col1">
																<div class="desc">
																	<a>
																		<strong>Livraison</strong> : <?= $livraison->id() ?>
																	</a><br>
																	<strong>Fournisseur</strong> : <?= $fournisseursManager->getFournisseurById($livraison->idFournisseur())->nom() ?><br>
																	<strong>Détails</strong> : <br>
																	&nbsp;&nbsp;<a>Désignation</a> : <?= $livraison->designation(); ?><br>  
																	&nbsp;&nbsp;<a>Quantité</a> : <?= $livraison->quantite(); ?><br>
																	&nbsp;&nbsp;<a>Prix unitaire</a> : <?= $livraison->prixUnitaire(); ?><br>
																	&nbsp;&nbsp;<a>Total</a> : <?= $livraison->prixUnitaire()*$livraison->quantite(); ?><br>
																	<br>
																</div>
															</div>
														</div>
													</div>
													<div class="col2">
														<div class="date">
															<?= date('d/m/y', strtotime($livraison->dateLivraison())) ?>
														</div>
													</div>
												</li>
												<hr>
												<?php 
												}
												?>
											</ul>
										</div>
									</div>
									<div class="tab-pane" id="tab_1_3">
										<div class="scroller" data-height="290px" data-always-visible="1" data-rail-visible1="1">
											<ul class="feeds">
												<?php
												foreach($clientWeek as $client){
													$contrats = $contratManager->getContratsByIdClient($client->id());
												?>
												<li>
													<div class="col1">
														<div class="cont">
															<div class="cont-col1">
																<div class="desc">	
																	<strong>Client</strong> : <a href="client-detail.php?idClient=<?= $client->id() ?>" target="_blank"><?= $client->nom() ?></a><br>
																</div>
															</div>
														</div>
													</div>
													<div class="col2">
														<div class="date">
															<?= $client->created() ?>
														</div>
													</div>
												</li>
												<hr>
												<?php 
												}
												?>
											</ul>
										</div>
									</div>
									<div class="tab-pane" id="tab_1_4">
										<div class="scroller" data-height="290px" data-always-visible="1" data-rail-visible1="1">
											<?php
											foreach($mailsToday as $mail){
											?>
											<div class="row-fluid">
												<div class="span6 user-info">
													<img alt="" src="assets/img/avatar.png" />
													<div class="details">
														<div>
															<a href="#"><?= $mail->sender() ?></a> 
														</div>
														<div>
															<strong>Message : </strong><?= $mail->content() ?><br>
															<strong>Envoyé Aujourd'hui à : </strong><?= date('h:i', strtotime($mail->created())) ?>
														</div>
													</div>
												</div>
											</div>
											<hr>
											<?php
											}
											?>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				</div>
			</div>
		</div>
	</div>
    <?php include('include/footer.php') ?>
    <?php include('include/scripts.php') ?>
	<script>jQuery(document).ready(function(){App.setPage("sliders");App.init();});</script>
</body>
</html>
<?php
}
else{
    header('Location:index.php');    
}
?>