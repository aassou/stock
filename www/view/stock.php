<?php
//classes loading begin
    include('../autoload.php');
	include('../lib/pagination.php');
    //classes loading end
    session_start();
    if(isset($_SESSION['userCafeManager']) and $_SESSION['userCafeManager']->profil()=="admin"){
    	$stockManager = new StockManager($pdo);
		$produitManager = new ProduitManager($pdo);
		$produits = $produitManager->getProduits();
		$stockNumber = $stockManager->getStockNumber();
		if($stockNumber!=0){
			$stockPerPage = 10;
	        $pageNumber = ceil($stockNumber/$stockPerPage);
	        $p = 1;
	        if(isset($_GET['p']) and ($_GET['p']>0 and $_GET['p']<=$pageNumber)){
	            $p = $_GET['p'];
	        }
	        else{
	            $p = 1;
	        }
	        $begin = ($p - 1) * $stockPerPage;
	        $pagination = paginate('stock.php', '?p=', $pageNumber, $p);
			$stocks = $stockManager->getStocksByLimits($begin, $stockPerPage);	 
		}
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<?php include('include/header.php') ?>
<body class="fixed-top">
	<!-- BEGIN HEADER -->
	<div class="header navbar navbar-inverse navbar-fixed-top">
		<?php include("include/top-menu.php"); ?>
	</div>
	<div class="page-container row-fluid sidebar-closed">
		<?php include("include/sidebar.php"); ?>
		<div class="page-content">
			<div class="container-fluid">
                <div class="row-fluid"><div class="span12"></div></div>
				<div class="row-fluid">
					<div class="span12">
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="dashboard.php">Accueil</a>
								<i class="icon-angle-right"></i>
							</li>
							<li>
								<i class="icon-bar-chart"></i>
                                <a><strong>Gestion de stock</strong></a>
							</li>
						</ul>
					</div>
				</div>
				<div class="row-fluid">
					<div class="span12">
						<div class="tab-pane active" id="tab_1">
							<?php if(isset($_SESSION['stock-add-success'])){ ?>
	                         	<div class="alert alert-success">
									<button class="close" data-dismiss="alert"></button>
									<?= $_SESSION['stock-add-success'] ?>		
								</div>
	                         <?php } 
	                         	unset($_SESSION['stock-add-success']);
	                         ?>
	                         <?php if(isset($_SESSION['stock-add-error'])){ ?>
	                         	<div class="alert alert-error">
									<button class="close" data-dismiss="alert"></button>
									<?= $_SESSION['stock-add-error'] ?>		
								</div>
	                         <?php } 
	                         	unset($_SESSION['stock-add-error']);
	                         ?>
                           <div class="portlet box grey">
                              <div class="portlet-title">
                                 <h4><i class="icon-edit"></i>Nouvelle entrée</h4>
                                 <div class="tools">
                                    <a href="javascript:;" class="collapse"></a>
                                    <a href="javascript:;" class="remove"></a>
                                 </div>
                              </div>
                              <div class="portlet-body form">
                                 <form id="addStockForm" action="../controller/StockAddController.php" method="POST" class="horizontal-form">
                                    <div class="row-fluid">
                                        <div class="span4">
                                          <div class="control-group autocomplet_container">
                                             <label class="control-label" for="produit">Produit <sup class="dangerous-action">*</sup></label>
                                             <div class="controls">
                                                 <input type="text" required="required" id="produit" name="produit" class="m-wrap span12" onkeyup="autocompletProduit()" />
                                                 <ul id="produitList"></ul>
                                             </div>
                                          </div>
                                        </div>
                                        <div class="span4">
                                          <div class="control-group">
                                             <label class="control-label" for="quantite">Quantité <sup class="dangerous-action">*</sup></label>
                                             <div class="controls">
                                                <input type="text" id="quantite" name="quantite" class="m-wrap span12">
                                             </div>
                                          </div>
                                        </div>
                                        <div class="span4">
                                            <div class="control-group">
                                                <label class="control-label" for="unite">Unité</label>
                                                <div class="controls">
                                                    <input type="text" id="unite" name="unite" class="m-wrap span12">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions">
                                        <p class="dangerous-action">* : Champs obligatoires</p>
                                        <input type="hidden" id="idProduit" name="idProduit" />
                                    	<button type="submit" class="btn black">Enregistrer <i class="icon-save"></i></button>
                                       	<button type="reset" class="btn red">Annuler</button>
                                    </div>
                                 </form>
                              </div>
                           </div>
                        </div>
                        <?php if(isset($_SESSION['stock-update-error'])){ ?>
                         	<div class="alert alert-error">
								<button class="close" data-dismiss="alert"></button>
								<?= $_SESSION['stock-update-error'] ?>		
							</div>
                         <?php } 
                         	unset($_SESSION['stock-update-error']);
                         ?>
                        <?php if(isset($_SESSION['stock-update-success'])){ ?>
                         	<div class="alert alert-success">
								<button class="close" data-dismiss="alert"></button>
								<?= $_SESSION['stock-update-success'] ?>		
							</div>
                         <?php } 
                         	unset($_SESSION['stock-update-success']);
                         ?>
                        <div class="portlet box grey" id="list">
							<div class="portlet-title">
								<h4><i class="icon-bar-chart"></i>Stock</h4>
								<div class="tools">
									<a href="javascript:;" class="collapse"></a>
									<a href="javascript:;" class="remove"></a>
								</div>
							</div>
							<div class="portlet-body">
                                <table class="table table-striped table-bordered table-hover" id="sample_1">
									<thead>
										<tr>
                                            <?php
                                            if (
                                                $_SESSION['userCafeManager']->profil() == "admin" ||
                                                $_SESSION['userCafeManager']->profil() == "manager"
                                            ) {
                                                ?>
                                                <th class="hidden-phone" style="width:10%">Actions</th>
                                                <?php
                                            }
                                            ?>
											<th style="width:30%">Produit</th>
											<th style="width:30%">Quantité</th>
											<th style="width:30%">Status</th>
										</tr>
									</thead>
									<tbody>
										<?php
										if($stockNumber!=0){ 
										foreach ($stocks as $stock) {
										?>	
										<tr>
                                            <?php
                                            if (
                                                $_SESSION['userCafeManager']->profil() == "admin" ||
                                                $_SESSION['userCafeManager']->profil() == "manager"
                                            ) {
                                                ?>
                                                <td style="width: 10%" class="hidden-phone">
                                                    <a title="Supprimer" class="btn mini red" href="#delete<?= $stock->id() ?>" data-toggle="modal" data-id="<?= $stock->id() ?>"><i class="icon-remove"></i></a>
                                                </td>
                                                <?php
                                            }
                                            ?>
											<td style="width: 30%"><?= $stock->produit()?></td>
											<td style="width: 30%">
												<?= $stock->quantite(). ' / ' . $stock->unite() ?>
											</td>
											<?php
											$class = "btn blue mini";
											$status = "Normal";
											if( $stock->quantite() < $produitManager->getProduitById($stock->idProduit())->quantiteMinimale() ){
												$class = "btn purple mini blink_me";
												$status = "Seuil Min";
											}
											?>
											<td style="width: 30%">
												<a class="<?= $class ?>"><?= $status ?></a>
											</td>
										</tr>
										<div id="delete<?= $stock->id();?>" class="modal hide fade in" tabindex="-1" role="dialog" aria-labelledby="login" aria-hidden="false" >
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
												<h3>Supprimer les informations du Stock</h3>
											</div>
											<div class="modal-body">
												<form class="form-horizontal loginFrm" action="../controller/StockDeleteController.php" method="post">
													<p>Êtes-vous sûr de vouloir supprimer cette entrée <strong><?= $stock->produit() ?></strong> ?</p>
													<div class="control-group">
														<label class="right-label"></label>
														<input type="hidden" name="idStock" value="<?= $stock->id() ?>" />
														<button class="btn" data-dismiss="modal"aria-hidden="true">Non</button>
														<button type="submit" class="btn red" aria-hidden="true">Oui</button>
													</div>
												</form>
											</div>
										</div>
										<?php }//end foreach
										}//end if ?>
									</tbody>
									<?php
									if($stockNumber != 0){
										echo $pagination;	
									}
									?>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
    <?php include('include/footer.php') ?>
    <?php include('include/scripts.php') ?>
	<script>
		jQuery(document).ready(function() {
			App.setPage("table_managed");
			App.init();
		});
		function blinker() {
		    $('.blink_me').fadeOut(500);
		    $('.blink_me').fadeIn(500);
		}
		setInterval(blinker, 1500);
        //validate form begins
        $("#addStockForm").validate({
            rules:{
                produit: {
                    required: true
                },
                quantite: {
                    required: true
                }
            },
            errorClass: "error-class",
            validClass: "valid-class"
        });
        //validate form ends
	</script>
</body>
</html>
<?php
}
else if(isset($_SESSION['userCafeManager']) and $_SESSION->profil()!="admin"){
	header('Location:dashboard.php');
}
else{
    header('Location:index.php');    
}
?>