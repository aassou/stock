<!-- Load javascripts at bottom, this will reduce page load time -->
<script src="assets/js/jquery-1.8.3.min.js" type="text/javascript"></script>
<script src="assets/js/jquery.pulsate.min.js" type="text/javascript"></script>
<script src="assets/js/jquery.blockui.js" type="text/javascript"></script>
<script src="assets/js/jquery.cookie.js" type="text/javascript"></script>
<script src="assets/jquery-slimscroll/jquery-ui-1.9.2.custom.min.js" type="text/javascript"></script>
<script src="assets/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="assets/jquery-knob/js/jquery.knob.js" type="text/javascript"></script>
<script src="assets/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js" type="text/javascript"></script>
<script src="assets/breakpoints/breakpoints.js" type="text/javascript"></script>
<script src="assets/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/bootstrap-datepicker/js/bootstrap-datepicker.js" type="text/javascript"></script>
<script src="assets/bootstrap-daterangepicker/date.js" type="text/javascript"></script>
<script src="assets/fullcalendar/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
<script src="assets/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="assets/chosen-bootstrap/chosen/chosen.jquery.min.js" type="text/javascript"></script>
<script src="assets/gritter/js/jquery.gritter.js" type="text/javascript"></script>
<script src="assets/js/app.js" type="text/javascript"></script>
<script src="assets/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="assets/data-tables/jquery.dataTables.js" type="text/javascript"></script>
<script src="assets/data-tables/DT_bootstrap.js" type="text/javascript"></script>
<script src="assets/fancybox/source/jquery.fancybox.pack.js" type="text/javascript"></script>
<script src="assets/jquery-validation/jquery.validate.js" type="text/javascript"></script>
<script src="assets/js/app.js"></script>
<script src="script.js"></script>
<!-- ie8 fixes -->
<!--[if lt IE 9]>
<script src="assets/js/excanvas.js" type="text/javascript"></script>
<script src="assets/js/respond.js" type="text/javascript"></script>
<![endif]-->