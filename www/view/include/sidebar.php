<?php
    $currentPage = basename($_SERVER['PHP_SELF']);
?>
<div class="page-sidebar nav-collapse collapse">
			<!-- BEGIN SIDEBAR MENU -->        	
			<ul>
				<li>
					<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
					<div class="sidebar-toggler hidden-phone"></div>
					<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
				</li>
				<li>
				</li>
				<!---------------------------- Dashboard Begin  -------------------------------------------->
				<li class="start <?php if($currentPage=="dashboard.php" 
				or $currentPage=="recherches.php"
				or $currentPage=="conges.php"
				or $currentPage=="statistiques.php"
				or $currentPage=="users.php"
				or $currentPage=="messages.php"
				or $currentPage=="user-profil.php"
				or $currentPage=="clients-search.php"
				or $currentPage=="fournisseurs-search.php"
				or $currentPage=="employes-projet-search.php"
				){echo "active ";} ?>">
					<a href="dashboard.php">
					<i class="icon-home"></i> 
					<span class="title">Accueil</span>
					</a>
				</li>
				<!---------------------------- Dashboard End    -------------------------------------------->
				<!---------------------------- Gestion Clients Begin ------------------------------------->
				<?php 
					$gestionClientClass="";
					if($currentPage=="clients-add.php"
					or $currentPage=="client-detail.php"
					){
						$gestionClientClass = "active ";
					} 
				?> 
				<li class="<?= $gestionClientClass; ?> has-sub ">
					<a href="clients-add.php">
					<i class="icon-group"></i> 
					<span class="title">Clients</span>
					</a>
				</li>
				<!---------------------------- Gestion Clients End ------------------------------------->
				<!---------------------------- Gestion des fournisseurs Begin ----------------------------------->
				<?php 
					$gestionFournisseurClass="";
					if($currentPage=="fournisseur-add.php"
					){
						$gestionFournisseurClass = "active ";
					}
				?> 
				<li class="<?= $gestionFournisseurClass; ?> has-sub" >
					<a href="fournisseur-add.php">
					<i class="icon-truck"></i> 
					<span class="title">Fournisseurs</span>
					</a>
				</li>
				<!---------------------------- Gestion des fournisseurs End -------------------------------------->
				<!---------------------------- Gestion des livraisons Begin ------------------------------------->
				<?php 
					$gestionLivraisonClass="";
					if($currentPage=="livraison-add.php"
					){
						$gestionLivraisonClass = "active ";
					} 
				?> 
				<li class="<?= $gestionLivraisonClass; ?> has-sub ">
					<a href="livraison-add.php">
					<i class="icon-shopping-cart"></i> 
					<span class="title">Livraisons</span>
					</a>
				</li>
				<!---------------------------- Gestion des livraisons End ------------------------------------->
				<!---------------------------- Gestion des produits Begin ------------------------------------->
				<?php 
					$gestionLivraisonClass="";
					if($currentPage=="produits.php"
					){
						$gestionLivraisonClass = "active ";
					} 
				?> 
				<li class="<?= $gestionLivraisonClass; ?> has-sub ">
					<a href="produits.php">
					<i class="icon-barcode"></i> 
					<span class="title">Produits</span>
					</a>
				</li>
				<!---------------------------- Gestion des produits End ------------------------------------->
				<!---------------------------- Gestion des commandes Begin ------------------------------------->
				<?php 
					$gestionCommandeClass="";
					if($currentPage=="commande-add.php"
					or $currentPage=="commandes.php"
					or $currentPage=="commande-list-produit.php"
					or $currentPage=="commande-detail.php"
					){
						$gestionCommandeClass = "active ";
					} 
				?> 
				<li class="<?= $gestionCommandeClass; ?> has-sub ">
					<a href="commande-add.php">
					<i class="icon-table"></i> 
					<span class="title">Commandes</span>
					</a>
				</li>
				<!---------------------------- Gestion des commandes End ------------------------------------->
				<!---------------------------- Gestion de stock Begin ------------------------------------->
				<?php 
					$gestionStockClass="";
					if($currentPage=="stock.php"
					){
						$gestionStockClass = "active ";
					} 
				?> 
				<li class="<?= $gestionStockClass; ?> has-sub ">
					<a href="stock.php">
					<i class="icon-bar-chart"></i> 
					<span class="title">Stock</span>
					</a>
				</li>
				<!---------------------------- Gestion de stock End ------------------------------------->
                <!---------------------------- Gestion de caisse Begin ------------------------------------->
                <?php
                $gestionCaisseClass="";
                if($currentPage == "caisse-group.php"
                or $currentPage == "caisse-mois-annee.php"
                ){
                    $gestionCaisseClass = "active ";
                }
                ?>
                <li class="<?= $gestionCaisseClass; ?> has-sub ">
                    <a href="caisse-group.php">
                        <i class="icon-money"></i>
                        <span class="title">Caisse</span>
                    </a>
                </li>
                <!---------------------------- Gestion de caisse End ------------------------------------->
                <!---------------------------- Gestion des cheques ------------------------------------->
                <?php
                $gestionChequeClass="";
                if($currentPage=="cheque.php"
                ){
                    $gestionChequeClass = "active ";
                }
                ?>
                <li class="<?= $gestionChequeClass; ?> has-sub ">
                    <a href="cheques.php">
                        <i class="icon-credit-card"></i>
                        <span class="title">Cheques</span>
                    </a>
                </li>
                <!---------------------------- Gestion des cheques ------------------------------------->
			</ul>
			<!-- END SIDEBAR MENU -->
		</div>