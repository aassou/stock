<!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
    <title>Stocki - Management Application</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <link rel="stylesheet" type="text/css" href="assets/bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/metro.css" />
    <link rel="stylesheet" type="text/css" href="assets/bootstrap/css/bootstrap-responsive.min.css" />
    <link rel="stylesheet" type="text/css" href="assets/font-awesome/css/font-awesome.css" />
    <link rel="stylesheet" type="text/css" href="assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/style.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/style_responsive.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/style_default.css" id="style_color" />
    <link rel="stylesheet" type="text/css" href="assets/chosen-bootstrap/chosen/chosen.css" />
    <link rel="stylesheet" type="text/css" href="assets/uniform/css/uniform.default.css" />
    <link rel="stylesheet" type="text/css" href="assets/gritter/css/jquery.gritter.css" />
    <link rel="stylesheet" type="text/css" href="assets/data-tables/DT_bootstrap.css" />
    <link rel="shortcut icon" href="favicon.ico" />
</head>
<!-- END HEAD -->