<?php
//classes loading begin
    function classLoad ($myClass) {
        if(file_exists('model/'.$myClass.'.php')){
            include('model/'.$myClass.'.php');
        }
        elseif(file_exists('controller/'.$myClass.'.php')){
            include('controller/'.$myClass.'.php');
        }
    }
    spl_autoload_register("classLoad"); 
    include('config.php');  
	include('lib/pagination.php');
    //classes loading end
    session_start();
    if(isset($_SESSION['userCafeManager']) and $_SESSION['userCafeManager']->profil()=="admin"){
    	//les sources
    	$clientManager = new ClientManager($pdo);
		$commandesManager = new CommandeManager($pdo);
		$produitManager = new ProduitManager($pdo);
		$listProduitManager = new ListProduitManager($pdo);
		$client = "";
		$idClient = 0;
		if( isset($_GET['idClient']) and 
		( $_GET['idClient']>0 and $_GET['idClient']<=$clientManager->getLastId() ) ){
			$idClient = htmlentities($_GET['idClient']);
			$client = $clientManager->getClientById($idClient);
			$commandeNumber = $commandesManager->getCommandeNumberByIdClient($idClient);
			if($commandeNumber!=0){
				$commandePerPage = 10;
		        $pageNumber = ceil($commandeNumber/$commandePerPage);
		        $p = 1;
		        if(isset($_GET['p']) and ($_GET['p']>0 and $_GET['p']<=$pageNumber)){
		            $p = $_GET['p'];
		        }
		        else{
		            $p = 1;
		        }
		        $begin = ($p - 1) * $commandePerPage;
		        $pagination = paginate('client-detail.php?idClient='.$idClient, '&p=', $pageNumber, $p);
				$commandes = $commandesManager->getCommandesByIdClientsByLimits($idClient, $begin, $commandePerPage);	 
			}
		}
		
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<meta charset="utf-8" />
	<title>NadoCaf - Management Application</title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
	<link href="assets/css/metro.css" rel="stylesheet" />
	<link href="assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
	<link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
	<link href="assets/css/style.css" rel="stylesheet" />
	<link href="assets/css/style_responsive.css" rel="stylesheet" />
	<link href="assets/css/style_default.css" rel="stylesheet" id="style_color" />
	<link href="assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="assets/uniform/css/uniform.default.css" />
	<link rel="stylesheet" type="text/css" href="assets/chosen-bootstrap/chosen/chosen.css" />
	<link rel="stylesheet" type="text/css" href="assets/bootstrap-datepicker/css/datepicker.css" />
	<link rel="stylesheet" href="assets/data-tables/DT_bootstrap.css" />
	<link rel="stylesheet" type="text/css" href="assets/uniform/css/uniform.default.css" />
	<link rel="shortcut icon" href="favicon.ico" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
	<!-- BEGIN HEADER -->
	<div class="header navbar navbar-inverse navbar-fixed-top">

		<?php include("include/top-menu.php"); ?>	
		<!-- END TOP NAVIGATION BAR -->
	</div>
	<!-- END HEADER -->
	<!-- BEGIN CONTAINER -->
	<div class="page-container row-fluid">
		<!-- BEGIN SIDEBAR -->
		<?php include("include/sidebar.php"); ?>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN PAGE CONTAINER-->			
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
						<h3 class="page-title">
							Fiche descriptif du client
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a>Accueil</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li>
								<i class="icon-group"></i>
								<a>Gestion des client</a>
								<i class="icon-angle-right"></i>
							</li>
							<li><a>Fiche du client</a></li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div class="row-fluid">
					<div class="span12">
						<div class="row-fluid add-portfolio">
							<div class="pull-left">
								<a href="clients-add.php#listClients" class="btn icn-only green"><i class="m-icon-swapleft m-icon-white"></i> Retour vers Liste des clients</a>
							</div>
						</div>
					</div>
				</div>
				<div class="row-fluid profile"> 
					<div class="span12">
						<!--BEGIN TABS-->
						<?php
						if( $idClient != 0 ){
						?>
						<div class="tabbable tabbable-custom">
							<ul class="nav nav-tabs">
								<li class="active"><a href="#tab_1_1" data-toggle="tab">Fiche du client</a></li>
								<li class=""><a href="#tab_1_2" data-toggle="tab">Historique des commandes</a></li>
							</ul>
							<div class="tab-content">
								<div class="tab-pane row-fluid active" id="tab_1_1">
									<ul class="unstyled profile-nav span3">
										<!--li>
											<a href="#updateClient<?= $client->id();?>" data-toggle="modal" data-id="<?= $client->id(); ?>">
												Modifier Infos Client
											</a>
										</li>
										<li>
											<a href="#">
												Imprimer Fiche Client
											</a>
										</li-->
									</ul>
									<div class="span9">
										<div class="row-fluid">
											<div class="span8 profile-info">
												<br /><br />
												<a class="btn big blue pull-right"><i class="icon-print"></i> Fiche Client</a>
												<h1><?= strtoupper($client->nom()) ?></h1>
													<table style="width: 100%;font-size: 18px">
														<tr>
															<td style="width: 30%"><a>N° TVA</a></td>
															<td><?= $client->numeroTva() ?></td>
														</tr>
														<tr>
															<td style="width: 30%"><a>N° Registre</a></td>
															<td><?= $client->numeroRegistre() ?></td>
														</tr>
														<tr>
															<td style="width: 30%"><a>Adresse</a></td>
															<td><?= $client->adresse() ?></td>
														</tr>
														<tr>
															<td style="width: 30%"><a>Téléphone</a></td>
															<td><?= $client->telephone() ?></td>
														</tr>
														<tr>
															<td style="width: 30%"><a>Email</a></td>
															<td><?= $client->email() ?></td>
														</tr>
													</table>
											</div>
											<!--end span8-->
										</div>
										<!--end row-fluid-->
									</div>
									<!--end span9-->
								</div>
								<div class="tab-pane row-fluid" id="tab_1_2">
									<ul class="unstyled profile-nav span1">
									</ul>
									<div class="span9">
										<div class="row-fluid">
											<div class="span8 profile-info">
												<div class="portlet" id="listCommande">
													<div class="portlet-title">
														<h4><i class="icon-table"></i>Les commandes de <a><?= $client->nom() ?></a></h4>
														<div class="tools">
															<a href="javascript:;" class="collapse"></a>
															<a href="javascript:;" class="remove"></a>
														</div>
													</div>
													<div class="portlet-body">
														<table class="table table-striped table-bordered table-advance table-hover" id="sample_editable_1">
															<thead>
																<tr>
																	<th style="width:10%">ID</th>
																	<th style="width:20%">Date</th>
																	<th style="width:20%">Total</th>
																	<th style="width:40%" class="hidden-phone">Bon de Commande</th>
																</tr>
															</thead>
															<tbody>
																<?php
																if($commandeNumber!=0){ 
																foreach ($commandes as $commande) {
																?>	
																<tr>
																	<td>
															        	<a class="btn mini black" href="commande-detail.php?idCommande=<?= $commande->id() ?>" target="_blank">
																			<?= $commande->id() ?>	
																		</a>
																	</td>
																	<td><?= date('d/m/Y', strtotime($commande->dateCommande())) ?></td>
																	<td class="hidden-phone">
																		<?= number_format($listProduitManager->getTotalListProduitByIdCommande($commande->id()), '2', ',', ' ') ?>
																	</td>
																	<td class="hidden-phone">
																		<a class="btn mini blue" href="../controller/CommandeBonPrint.php?idCommande=<?= $commande->id() ?>">
																			<i class="icon-print "></i> Imprimer
																		</a>
																	</td>
																</tr>				
																<?php }//end foreach
																}//end if ?>
															</tbody>
															<?php
															if($commandeNumber != 0){
																echo $pagination;	
															}
															?>
														</table>
													</div>	
											</div>		
											<!--end span8-->
										</div>
										<!--end row-fluid-->
									</div>
									<!--end span9-->
								</div>
								<!--end tab-pane-->
							</div>
						</div>
						<?php
						}
						else{
						?>
						<div class="alert alert-error">
							<button class="close" data-dismiss="alert"></button>
							Ce client n'existe pas dans votre système.		
						</div>
						<?php	
						}
						?>
					</div>
				</div>
				<!-- END PAGE CONTENT -->
			</div>
			<!-- END PAGE CONTAINER-->
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	<div class="footer">
		2015 &copy; MerlaTravERP. Management Application.
		<div class="span pull-right">
			<span class="go-top"><i class="icon-angle-up"></i></span>
		</div>
	</div>
	<!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS -->
	<!-- Load javascripts at bottom, this will reduce page load time -->
	<script src="assets/js/jquery-1.8.3.min.js"></script>
	<script src="assets/breakpoints/breakpoints.js"></script>
	<script src="assets/bootstrap/js/bootstrap.min.js"></script>
	<script src="assets/js/jquery.blockui.js"></script>
	<script src="assets/js/jquery.cookie.js"></script>
	<script src="assets/fancybox/source/jquery.fancybox.pack.js"></script>
	<script type="text/javascript" src="assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
	<script type="text/javascript" src="assets/bootstrap-daterangepicker/date.js"></script>
	<!-- ie8 fixes -->
	<!--[if lt IE 9]>
        <script src="assets/js/excanvas.js"></script>
        <script src="assets/js/respond.js"></script>
        <![endif]-->
	<script type="text/javascript" src="assets/uniform/jquery.uniform.min.js"></script>
	<script type="text/javascript" src="assets/data-tables/jquery.dataTables.js"></script>
	<script type="text/javascript" src="assets/data-tables/DT_bootstrap.js"></script>
	<script src="assets/js/app.js"></script>
	<script>
		jQuery(document).ready(function() {			
			// initiate layout and plugins
			//App.setPage("table_editable");
			App.init();
		});
	</script>
</body>
<!-- END BODY -->
</html>
<?php
}
else if(isset($_SESSION['userCafeManager']) and $_SESSION->profil()!="admin"){
	header('Location:dashboard.php');
}
else{
    header('Location:index.php');    
}
?>