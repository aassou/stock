<?php
include('../autoload.php');
include('../lib/pagination.php');
session_start();
if(isset($_SESSION['userCafeManager']) and $_SESSION['userCafeManager']->profil()=="admin"){
    $produitManager = new ProduitManager($pdo);
    $fournisseurManager = new FournisseurManager($pdo);
    $fournisseurs = $fournisseurManager->getFournisseurs();
    $produits = $produitManager->getProduits();
?>
<!DOCTYPE html>
    <!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
    <!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
    <!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
    <?php include('include/header.php') ?>
    <body class="fixed-top">
        <div class="header navbar navbar-inverse navbar-fixed-top">
            <?php include("include/top-menu.php"); ?>
        </div>
        <div class="page-container row-fluid sidebar-closed">
            <?php include("include/sidebar.php"); ?>
            <div class="page-content">
                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="span12">
                            <h3 class="page-title">
                                Gestion des Produits
                            </h3>
                            <ul class="breadcrumb">
                                <li>
                                    <i class="icon-home"></i>
                                    <a href="dashboard.php">Accueil</a>
                                    <i class="icon-angle-right"></i>
                                </li>
                                <li>
                                    <i class="icon-barcode"></i>
                                    <a><strong>Gestion des produits</strong></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="tab-pane active" id="tab_1">
                                <?php if(isset($_SESSION['produit-add-success'])){ ?>
                                    <div class="alert alert-success">
                                        <button class="close" data-dismiss="alert"></button>
                                        <?= $_SESSION['produit-add-success'] ?>
                                    </div>
                                <?php }
                                unset($_SESSION['produit-add-success']);
                                ?>
                                <?php if(isset($_SESSION['produit-add-error'])){ ?>
                                    <div class="alert alert-error">
                                        <button class="close" data-dismiss="alert"></button>
                                        <?= $_SESSION['produit-add-error'] ?>
                                    </div>
                                <?php }
                                unset($_SESSION['produit-add-error']);
                                ?>
                                <div class="portlet box grey">
                                    <div class="portlet-title">
                                        <h4><i class="icon-edit"></i>Nouveau produit</h4>
                                        <div class="tools">
                                            <a href="javascript:;" class="collapse"></a>
                                            <a href="javascript:;" class="remove"></a>
                                        </div>
                                    </div>
                                    <div class="portlet-body form">
                                        <form id="addProduitForm" action="../controller/ProduitAddController.php" method="POST" class="horizontal-form">
                                            <div class="row-fluid">
                                                <div class="span2">
                                                    <div class="control-group">
                                                        <label class="control-label" for="idFournisseur">Fournisseur <sup class="dangerous-action">*</sup></label>
                                                        <div class="controls">
                                                            <select class="m-wrap span12" name="idFournisseur">
                                                                <?php foreach ($fournisseurs as $fournisseur) { ?>
                                                                    <option value="<?= $fournisseur->id() ?>"><?= $fournisseur->nom() ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="span2">
                                                    <div class="control-group autocomplet_container">
                                                        <label class="control-label" for="marque">Marque <sup class="dangerous-action">*</sup></label>
                                                        <div class="controls">
                                                            <input type="text" id="marque" name="marque" class="m-wrap span12">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="span2">
                                                    <div class="control-group autocomplet_container">
                                                        <label class="control-label" for="reference">Référence <sup class="dangerous-action">*</sup></label>
                                                        <div class="controls">
                                                            <input type="text" id="reference" name="reference" class="m-wrap span12">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="span2">
                                                    <div class="control-group">
                                                        <label class="control-label" for="prix">Prix <sup class="dangerous-action">*</sup></label>
                                                        <div class="controls">
                                                            <input type="text" id="prix" name="prix" class="m-wrap span12">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="span2">
                                                    <div class="control-group">
                                                        <label class="control-label" for="quantiteMinimale">Quantite Minimale <sup class="dangerous-action">*</sup></label>
                                                        <div class="controls">
                                                            <input type="text" id="quantiteMinimale" name="quantiteMinimale" class="m-wrap span12">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="span2">
                                                    <div class="control-group">
                                                        <label class="control-label" for="description">Description</label>
                                                        <div class="controls">
                                                            <input type="text" id="description" name="description" class="m-wrap span12">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-actions">
                                                <p class="dangerous-action">* : Champs obligatoires</p>
                                                <button type="submit" class="btn black">Enregistrer <i class="icon-save"></i></button>
                                                <button type="reset" class="btn red">Annuler</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <?php if(isset($_SESSION['produit-update-error'])){ ?>
                                <div class="alert alert-error">
                                    <button class="close" data-dismiss="alert"></button>
                                    <?= $_SESSION['produit-update-error'] ?>
                                </div>
                            <?php }
                            unset($_SESSION['produit-update-error']);
                            ?>
                            <?php if(isset($_SESSION['produit-update-success'])){ ?>
                                <div class="alert alert-success">
                                    <button class="close" data-dismiss="alert"></button>
                                    <?= $_SESSION['produit-update-success'] ?>
                                </div>
                            <?php }
                            unset($_SESSION['produit-update-success']);
                            ?>
                            <?php if(isset($_SESSION['produit-delete-success'])){ ?>
                                <div class="alert alert-success">
                                    <button class="close" data-dismiss="alert"></button>
                                    <?= $_SESSION['produit-delete-success'] ?>
                                </div>
                            <?php }
                            unset($_SESSION['produit-delete-success']);
                            ?>
                            <div class="portlet box grey" id="listProduits">
                                <div class="portlet-title">
                                    <h4><i class="icon-barcode"></i>Liste des produits</h4>
                                    <div class="tools">
                                        <a href="javascript:;" class="collapse"></a>
                                        <a href="javascript:;" class="remove"></a>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <table class="table table-striped table-bordered table-hover" id="sample_1">
                                        <thead>
                                        <tr>
                                            <?php
                                            if (
                                                $_SESSION['userCafeManager']->profil() == "admin" ||
                                                $_SESSION['userCafeManager']->profil() == "manager"
                                            ) {
                                                ?>
                                                <th class="hidden-phone" style="width:10%">Actions</th>
                                                <?php
                                            }
                                            ?>
                                            <th style="width:15%">Fournisseur</th>
                                            <th style="width:10%">Marque</th>
                                            <th style="width:15%">Référence</th>
                                            <th style="width:10%">Prix</th>
                                            <th style="width:10%">Qte Min</th>
                                            <th style="width:30%" class="hidden-phone">Description</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                            foreach ($produits as $produit) {
                                                ?>
                                                <tr>
                                                    <?php
                                                    if (
                                                        $_SESSION['userCafeManager']->profil() == "admin" ||
                                                        $_SESSION['userCafeManager']->profil() == "manager"
                                                    ) {
                                                        ?>
                                                        <td style="width: 10%">
                                                            <a title="Supprimer" class="btn mini red" href="#delete<?= $produit->id() ?>" data-toggle="modal" data-id="<?= $produit->id() ?>"><i class="icon-remove"></i></a>
                                                            <a title="Modifier" class="btn mini green" href="#update<?= $produit->id() ?>" data-toggle="modal" data-id="<?= $produit->id() ?>"><i class="icon-refresh"></i></a>
                                                        </td>
                                                        <?php
                                                    }
                                                    ?>
                                                    <td style="width: 15%"><?= $fournisseurManager->getFournisseurById($produit->idFournisseur())->nom() ?></td>
                                                    <td style="width: 10%"><?= $produit->marque()?></td>
                                                    <td style="width: 15%"><?= $produit->reference()?></td>
                                                    <td style="width: 10%"><?= number_format($produit->prix(), '2', ',', ' ')?></td>
                                                    <td style="width: 10%"><?= $produit->quantiteMinimale()?></td>
                                                    <td style="width: 30%" class="hidden-phone"><?= $produit->description()?></td>
                                                </tr>
                                                <div id="update<?= $produit->id() ?>" class="modal modal-big hide fade in" tabindex="-1" role="dialog" aria-labelledby="login" aria-hidden="false" >
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                        <h3>Modifier les informations du Produit </h3>
                                                    </div>
                                                    <div class="modal-body">
                                                        <form class="form-horizontal" action="../controller/ProduitUpdateController.php" method="post">
                                                            <p>Êtes-vous sûr de vouloir modifier les infos du produit <strong><?= $produit->reference() ?></strong> ?</p>
                                                            <div class="control-group">
                                                                <label class="control-label">Fournisseur</label>
                                                                <div class="controls">
                                                                    <select name="idFournisseur">
                                                                        <option value="<?= $produit->idFournisseur() ?>">
                                                                            <?= $fournisseurManager->getFournisseurById($produit->idFournisseur())->nom() ?>
                                                                        </option>
                                                                        <option disabled="disabled">---------------------------------</option>
                                                                        <?php foreach ($fournisseurs as $fournisseur) { ?>
                                                                            <option value="<?= $fournisseur->id() ?>">
                                                                                <?= $fournisseur->nom() ?>
                                                                            </option>
                                                                        <?php } ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="control-group">
                                                                <label class="control-label">Marque</label>
                                                                <div class="controls">
                                                                    <input type="text" name="marque" value="<?= $produit->marque() ?>" />
                                                                </div>
                                                            </div>
                                                            <div class="control-group">
                                                                <label class="control-label">Référence</label>
                                                                <div class="controls">
                                                                    <input type="text" name="reference" value="<?= $produit->reference() ?>" />
                                                                </div>
                                                            </div>
                                                            <div class="control-group">
                                                                <label class="control-label">Prix</label>
                                                                <div class="controls">
                                                                    <input type="text" name="prix" value="<?= $produit->prix() ?>" />
                                                                </div>
                                                            </div>
                                                            <div class="control-group">
                                                                <label class="control-label">Qte Minimale</label>
                                                                <div class="controls">
                                                                    <input type="text" name="quantiteMinimale" value="<?= $produit->quantiteMinimale() ?>" />
                                                                </div>
                                                            </div>
                                                            <div class="control-group">
                                                                <label class="control-label">Description</label>
                                                                <div class="controls">
                                                                    <input type="text" name="description" value="<?= $produit->description() ?>" />
                                                                </div>
                                                            </div>
                                                            <div class="control-group">
                                                                <input type="hidden" name="idProduit" value="<?= $produit->id() ?>" />
                                                                <div class="controls">
                                                                    <button class="btn" data-dismiss="modal"aria-hidden="true">Non</button>
                                                                    <button type="submit" class="btn red" aria-hidden="true">Oui</button>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                                <div id="delete<?= $produit->id();?>" class="modal hide fade in" tabindex="-1" role="dialog" aria-labelledby="login" aria-hidden="false" >
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                        <h3>Supprimer Produit</h3>
                                                    </div>
                                                    <div class="modal-body">
                                                        <form class="form-horizontal loginFrm" action="../controller/ProduitDeleteController.php" method="post">
                                                            <p>Êtes-vous sûr de vouloir supprimer ce produit <strong><?= $produit->reference() ?></strong> ?</p>
                                                            <div class="control-group">
                                                                <label class="right-label"></label>
                                                                <input type="hidden" name="idProduit" value="<?= $produit->id() ?>" />
                                                                <button class="btn" data-dismiss="modal"aria-hidden="true">Non</button>
                                                                <button type="submit" class="btn red" aria-hidden="true">Oui</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include('include/footer.php') ?>
        <?php include('include/scripts.php') ?>
        <script>
            jQuery(document).ready(function() {
                // initiate layout and plugins
                App.setPage("table_managed");
                App.init();
            });
            //validate form begins
            $("#addProduitForm").validate({
                rules:{
                    reference: {
                        required: true
                    },
                    marque: {
                        required: true
                    },
                    prix: {
                        required: true,
                        number: true
                    },
                    quantiteMinimale: {
                        required: true,
                        number: true
                    },
                    idFournisseur: {
                        required: true
                    }
                },
                errorClass: "error-class",
                validClass: "valid-class"
            });
            //validate form ends
        </script>
    </body>
</html>
<?php
}
else if(isset($_SESSION['userCafeManager']) and $_SESSION->profil()!="admin"){
    header('Location:dashboard.php');
}
else{
    header('Location:index.php');
}
?>