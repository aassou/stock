<?php
    include('../autoload.php');
	include('../lib/pagination.php');
    session_start();
    if(isset($_SESSION['userCafeManager']) and $_SESSION['userCafeManager']->profil()=="admin"){
		$fournisseurManager = new FournisseurManager($pdo);
		$fournisseurs = $fournisseurManager->getFournisseurs();
		$livraisonManager = new LivraisonManager($pdo);
		$livraisonNumber = $livraisonManager->getLivraisonNumber();
		if( $livraisonNumber != 0 ){
			$livraisonPerPage = 10;
	        $pageNumber = ceil($livraisonNumber/$livraisonPerPage);
	        $p = 1;
	        if(isset($_GET['p']) and ($_GET['p']>0 and $_GET['p']<=$pageNumber)){
	            $p = $_GET['p'];
	        }
	        else{
	            $p = 1;
	        }
	        $begin = ($p - 1) * $livraisonPerPage;
			$livraisons = $livraisonManager->getLivraisonsByLimits($begin, $livraisonPerPage);
	        $pagination = paginate('livraison-add.php', '?p=' , $pageNumber, $p);	
		}
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<?php include('include/header.php') ?>
<body class="fixed-top">
	<!-- BEGIN HEADER -->
	<div class="header navbar navbar-inverse navbar-fixed-top">
		<?php include("include/top-menu.php"); ?>
	</div>
	<div class="page-container row-fluid sidebar-closed">
		<?php include("include/sidebar.php"); ?>
		<div class="page-content">
			<div class="container-fluid">
                <div class="row-fluid"><div class="span12"></div></div>
				<div class="row-fluid">
					<div class="span12">
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="dashboard.php">Accueil</a>
								<i class="icon-angle-right"></i>
							</li>
							<li>
								<i class="icon-shopping-cart"></i>
                                <a><strong>Gestion des livraisons</strong></a>
							</li>
						</ul>
					</div>
				</div>
				<div class="row-fluid">
					<div class="span12">
						<div class="tab-pane active" id="tab_1">
							<?php if(isset($_SESSION['livraison-add-success'])){ ?>
	                         	<div class="alert alert-success">
									<button class="close" data-dismiss="alert"></button>
									<?= $_SESSION['livraison-add-success'] ?>		
								</div>
	                         <?php } 
	                         	unset($_SESSION['livraison-add-success']);
	                         ?>
	                         <?php if(isset($_SESSION['livraison-add-error'])){ ?>
	                         	<div class="alert alert-error">
									<button class="close" data-dismiss="alert"></button>
									<?= $_SESSION['livraison-add-error'] ?>		
								</div>
	                         <?php } 
	                         	unset($_SESSION['livraison-add-error']);
	                         ?>
                           <div class="portlet box blue">
                              <div class="portlet-title">
                                 <h4><i class="icon-edit"></i>Nouvelle Livraison</strong></h4>
                                 <div class="tools">
                                    <a href="javascript:;" class="collapse"></a>
                                    <a href="javascript:;" class="remove"></a>
                                 </div>
                              </div>
                              <div class="portlet-body form">
                                 <!-- BEGIN FORM-->
                                 <form action="../controller/LivraisonAddController.php" method="POST" class="horizontal-form">
                                    <div class="row-fluid">
                                    	<div class="span4">
                                          <div class="control-group">
                                             <label class="control-label" for="libelle">Fournisseur</label>
                                             <div class="controls">
                                                <select name="fournisseur" class="m-wrap" style="width: 230px">
                                                <?php foreach($fournisseurs as $fournisseur){ ?>
                                                	<option value="<?= $fournisseur->id() ?>">
                                                		<?= $fournisseur->nom() ?>
                                                	</option>
                                                <?php } ?>	
                                                <select>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="span4">
                                          <div class="control-group">
                                             <label class="control-label" for="dateLivraison">Date de livraison</label>
                                             <div class="controls">
                                                <div class="input-append date date-picker" data-date="" data-date-format="yyyy-mm-dd">
				                                    <input name="dateLivraison" id="dateLivraison" class="m-wrap m-ctrl-small date-picker" type="text" value="<?= date('Y-m-d') ?>" />
				                                    <span class="add-on"><i class="icon-calendar"></i></span>
				                                 </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="span4">
                                          <div class="control-group">
                                             <label class="control-label" for="libelle">Libellé</label>
                                             <div class="controls">
                                                <input type="text" id="libelle" name="libelle" class="m-wrap span12">
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="row-fluid">
                                    	<div class="span4">
                                          <div class="control-group">
                                             <label class="control-label" for="designation">Désignation</label>
                                             <div class="controls">
                                                <input type="text" id="designation" name="designation" class="m-wrap span12">
                                             </div>
                                          </div>
                                       </div>
                                    	<div class="span4">
                                          <div class="control-group">
                                             <label class="control-label" for="prixUnitaire">Prix unitaire</label>
                                             <div class="controls">
                                                <input type="text" id="prixUnitaire" name="prixUnitaire" class="m-wrap span12">
                                             </div>
                                          </div>
                                       </div>
                                       <div class="span4">
                                          <div class="control-group">
                                             <label class="control-label" for="quantite">Quantité</label>
                                             <div class="controls">
                                                <input type="text" id="quantite" name="quantite" class="m-wrap span12">
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="form-actions">
                                       <button type="submit" class="btn blue">Enregistrer <i class="icon-save"></i></button>
                                       	<button type="reset" class="btn red">Annuler</button>
                                    </div>
                                 </form>
                                 <!-- END FORM--> 
                              </div>
                           </div>
                        </div>
					</div>
				</div>
				<div class="row-fluid">
					<div class="span12">
						<?php if(isset($_SESSION['livraison-update-success'])){ ?>
                         	<div class="alert alert-success">
								<button class="close" data-dismiss="alert"></button>
								<?= $_SESSION['livraison-update-success'] ?>		
							</div>
                         <?php } 
                         	unset($_SESSION['livraison-update-success']);
                         ?>
                         <?php if(isset($_SESSION['livraison-delete-success'])){ ?>
                         	<div class="alert alert-success">
								<button class="close" data-dismiss="alert"></button>
								<?= $_SESSION['livraison-delete-success'] ?>		
							</div>
                         <?php } 
                         	unset($_SESSION['livraison-delete-success']);
                         ?>
                         <?php if(isset($_SESSION['livraison-update-error'])){ ?>
                         	<div class="alert alert-error">
								<button class="close" data-dismiss="alert"></button>
								<?= $_SESSION['livraison-update-error'] ?>		
							</div>
                         <?php } 
                         	unset($_SESSION['livraison-update-error']);
                         ?>
                        <div class="portlet" id="listLivraisons">
							<div class="portlet-title">
								<h4><i class="icon-shopping-cart"></i>Liste des livraisons</h4>
								<div class="tools">
									<a href="javascript:;" class="collapse"></a>
									<a href="javascript:;" class="remove"></a>
								</div>
							</div>
							<div class="portlet-body">
								<table class="table table-striped table-bordered table-advance table-hover">
									<thead>
										<tr>
											<th>Date</th>
											<th>Désignation</th>
											<th>Libelle</th>
											<th>Quté</th>
											<th>Prix.Uni</th>
											<th>Total</th>
											<th>Modifier</th>
											<th>Supprimer</th>
										</tr>
									</thead>
									<tbody>
									<?php
									foreach($livraisons as $livraison){
									?>
										<tr>
											<td><?= date('d/m/Y', strtotime($livraison->dateLivraison())) ?></td>
											<td><?= $livraison->designation() ?></td>
											<td><a><?= $livraison->libelle() ?></a></td>
											<td><?= $livraison->quantite() ?></td>
											<td><?= $livraison->prixUnitaire() ?></td>
											<td><?= $livraison->prixUnitaire()*$livraison->quantite() ?></td>
											<td class="hidden-phone">
												<a class="btn mini green" href="#update<?= $livraison->id();?>" data-toggle="modal" data-id="<? $livraison->id(); ?>">
													<i class="icon-refresh "></i>
												</a>
											</td>
											<td class="hidden-phone">
												<a class="btn mini red" href="#delete<?= $livraison->id();?>" data-toggle="modal" data-id="<? $livraison->id(); ?>">
													<i class="icon-remove "></i>
												</a>
											</td>
										</tr>
										<!-- update box begin-->
										<div id="update<?= $livraison->id();?>" class="modal hide fade in" tabindex="-1" role="dialog" aria-labelledby="login" aria-hidden="false" >
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
												<h3>Modifier Infos Livraison</h3>
											</div>
											<div class="modal-body">
												<form class="form-horizontal" action="../controller/LivraisonUpdateController.php" method="post" enctype="multipart/form-data">
													<p>Êtes-vous sûr de vouloir modifier les infos de livraison N° <strong><?= $livraison->id() ?></strong> ?</p>
													<div class="control-group">
														<label class="control-label">Libelle</label>
														<div class="controls">
															<input type="text" name="libelle" value="<?= $livraison->libelle() ?>" />
														</div>
													</div>
													<div class="control-group">
														<label class="control-label">Désignation</label>
														<div class="controls">
															<input type="text" name="designation" value="<?= $livraison->designation() ?>" />
														</div>
													</div>
													<div class="control-group">
														<label class="control-label">Prix Unitaire</label>
														<div class="controls">
															<input type="text" name="prixUnitaire" value="<?= $livraison->prixUnitaire() ?>" />
														</div>	
													</div>
													<div class="control-group">
														<label class="control-label">Quantité</label>
														<div class="controls">
															<input type="text" name="quantite" value="<?= $livraison->quantite() ?>" />
														</div>	
													</div>
													<div class="control-group">
														<input type="hidden" name="idLivraison" value="<?= $livraison->id() ?>" />
														<div class="controls">	
															<button class="btn" data-dismiss="modal"aria-hidden="true">Non</button>
															<button type="submit" class="btn red" aria-hidden="true">Oui</button>
														</div>
													</div>
												</form>
											</div>
										</div>
										<!-- update box end -->	
										<!-- delete box begin-->
										<div id="delete<?= $livraison->id();?>" class="modal hide fade in" tabindex="-1" role="dialog" aria-labelledby="login" aria-hidden="false" >
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
												<h3>Supprimer Livraison</h3>
											</div>
											<div class="modal-body">
												<form class="form-horizontal loginFrm" action="../controller/LivraisonDeleteController.php" method="post">
													<p>Êtes-vous sûr de vouloir supprimer cette livraison <strong><?= $livraison->id() ?></strong> ?</p>
													<div class="control-group">
														<label class="right-label"></label>
														<input type="hidden" name="idLivraison" value="<?= $livraison->id() ?>" />
														<button class="btn" data-dismiss="modal"aria-hidden="true">Non</button>
														<button type="submit" class="btn red" aria-hidden="true">Oui</button>
													</div>
												</form>
											</div>
										</div>
										<!-- delete box end -->	
									<?php
									}
									?>
									</tbody><?=	$pagination;?>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php include('include/footer.php') ?>
    <?php include('include/scripts.php') ?>
	<script>
		jQuery(document).ready(function() {			
			// initiate layout and plugins
			//App.setPage("table_editable");
			$('.hidenBlock').hide();
			App.init();
		});
	</script>
	<script>
	$(function(){
        $('#prixUnitaire, #quantite').change(function(){
            var prixUnitaire = parseFloat($('#prixUnitaire').val());
            var quantite = parseFloat($('#quantite').val());
            var total = 0;
            total = prixUnitaire * quantite;
            $('#total').val(total);
            $('#paye').val(0);
            $('#reste').val(total);
        });
    });
    $(function(){
        $('#paye').change(function(){
            var paye = parseFloat($('#paye').val());
            var total = parseFloat($('#total').val());
            var reste = 0;
            reste = total - paye;
            $('#reste').val(reste);
        });
    });		
	</script>
</body>
</html>
<?php
}
else if(isset($_SESSION['userCafeManager']) and $_SESSION->profil()!="admin"){
	header('Location:dashboard.php');
}
else{
    header('Location:index.php');    
}
?>