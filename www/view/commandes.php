<?php
    include('../autoload.php');
	include('../lib/pagination.php');
    //classes loading end
    session_start();
    if(isset($_SESSION['userCafeManager']) and $_SESSION['userCafeManager']->profil()=="admin"){
    	$commandesManager = new CommandeManager($pdo);
		$clientManager = new ClientManager($pdo);
		$produitManager = new ProduitManager($pdo);
		$clients = $clientManager->getClients();
		$produits = $produitManager->getProduits();
		$commandeNumber = $commandesManager->getCommandeNumber();
		if($commandeNumber!=0){
			$commandePerPage = 10;
	        $pageNumber = ceil($commandeNumber/$commandePerPage);
	        $p = 1;
	        if(isset($_GET['p']) and ($_GET['p']>0 and $_GET['p']<=$pageNumber)){
	            $p = $_GET['p'];
	        }
	        else{
	            $p = 1;
	        }
	        $begin = ($p - 1) * $commandePerPage;
	        $pagination = paginate('commandes-add.php', '?p=', $pageNumber, $p);
			$commandes = $commandesManager->getCommandesByLimits($begin, $commandePerPage);	 
		}
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<?php include('include/header.php') ?>
<body class="fixed-top">
	<div class="header navbar navbar-inverse navbar-fixed-top">
		<?php include("include/top-menu.php") ?>
	</div>
	<div class="page-container row-fluid sidebar-closed">
		<?php include("include/sidebar.php"); ?>
		<div class="page-content">
			<div class="container-fluid">
                <div class="row-fluid"><div class="span12"></div></div>
				<div class="row-fluid">
					<div class="span12">
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="dashboard.php">Accueil</a>
								<i class="icon-angle-right"></i>
							</li>
							<li>
								<i class="icon-shopping-cart"></i>
                                <a><strong>Gestion des Commandes</strong></a>
							</li>
						</ul>
					</div>
				</div>
				<div class="row-fluid">
					<div class="span12">
                        <?php if(isset($_SESSION['commande-update-error'])){ ?>
                         	<div class="alert alert-error">
								<button class="close" data-dismiss="alert"></button>
								<?= $_SESSION['commande-update-error'] ?>		
							</div>
                         <?php } 
                         	unset($_SESSION['commande-update-error']);
                         ?>
                        <?php if(isset($_SESSION['commande-update-success'])){ ?>
                         	<div class="alert alert-success">
								<button class="close" data-dismiss="alert"></button>
								<?= $_SESSION['commande-update-success'] ?>		
							</div>
                         <?php } 
                         	unset($_SESSION['commande-update-success']);
                         ?>
                         <?php if(isset($_SESSION['commande-delete-success'])){ ?>
                         	<div class="alert alert-success">
								<button class="close" data-dismiss="alert"></button>
								<?= $_SESSION['commande-delete-success'] ?>		
							</div>
                         <?php } 
                         	unset($_SESSION['commande-delete-success']);
                         ?>
                        <div class="portlet" id="listCommande">
							<div class="portlet-title">
								<h4><i class="icon-table"></i>Les commandes</h4>
								<div class="tools">
									<a href="javascript:;" class="collapse"></a>
									<a href="javascript:;" class="remove"></a>
								</div>
							</div>
							<div class="portlet-body">
								<table class="table table-striped table-bordered table-advance table-hover" id="sample_editable_1">
									<thead>
										<tr>
											<th style="width:5%">ID</th>
											<th style="width:10%">Date</th>
											<th style="width:20%" class="hidden-phone">Client</th>
											<th style="width:10%" class="hidden-phone">Total</th>
											<th style="width:10%" class="hidden-phone">Bon.Com</th>
											<th style="width:10%" class="hidden-phone">Modifier</th>
											<th style="width:10%" class="hidden-phone">Supprimer</th>
										</tr>
									</thead>
									<tbody>
										<?php
										if($commandeNumber!=0){ 
										foreach ($commandes as $commande) {
										?>	
										<tr>
											<td>
									        	<a href="#">
													<?= $commande->id() ?>	
												</a>
											</td>
											<td class="hidden-phone"><?= date('d-m-Y', strtotime($commande->dateCommande())) ?></td>
											<td class="hidden-phone">
												<a target="_blank" href="client-detail.php?idClient=<?= $commande->id() ?>">
													<?= $clientManager->getClientById($commande->client())->nom()?>
												</a>
											</td>
											<td class="hidden-phone"><?= number_format($commande->quantite()*$produitManager->getProduitById($commande->produit())->prix(), '2', ',', ' ') ?></td>
											<td class="hidden-phone">
												<a class="btn mini blue" href="../controller/CommandeBonPrint.php?idCommande=<?= $commande->id() ?>">
													<i class="icon-print "></i> Imprimer
												</a>
											</td>
											<td class="hidden-phone">
												<a class="btn mini green" href="#update<?= $commande->id();?>" data-toggle="modal" data-id="<? $commande->id(); ?>">
													<i class="icon-refresh "></i>
												</a>
											</td>
											<td class="hidden-phone">
												<a class="btn mini red" href="#delete<?= $commande->id();?>" data-toggle="modal" data-id="<? $commande->id(); ?>">
													<i class="icon-remove "></i>
												</a>
											</td>
										</tr>
										<!-- updateFournisseur box begin-->
										<div id="update<?= $commande->id() ?>" class="modal hide fade in" tabindex="-1" role="dialog" aria-labelledby="login" aria-hidden="false" >
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
												<h3>Modifier les informations du commande </h3>
											</div>
											<div class="modal-body">
												<form class="form-horizontal" action="../controller/CommandeUpdateController.php" method="post">
													<p>Êtes-vous sûr de vouloir modifier les infos de la commande N° <strong><?= $commande->id() ?></strong> ?</p>
													<div class="control-group">
														<label class="control-label">Client</label>
														<div class="controls">
															<select name="client" class="m-wrap">
			                                             		<option selected="selected" value="<?= $commande->client() ?>">
			                                                		<?= $clientManager->getClientById($commande->client())->nom() ?>
			                                                	</option>
			                                                	<option disabled="disabled">-----------------</option>
			                                             		<?php
			                                             		foreach( $clients as $client ){
			                                             		?>
			                                             		<option value="<?= $client->id() ?>"><?= $client->nom() ?></option>
			                                             		<?php
			                                             		}
			                                             		?>
			                                             	</select>   
														</div>
													</div>
			                                          <div class="control-group">
			                                             <label class="control-label" for="dateCommande">Date de commande</label>
			                                             <div class="controls">
							                                    <input name="dateCommande" id="dateCommande" class="m-wrap" type="text" value="<?= $commande->dateCommande() ?>" />
							                                 </div>
			                                             </div>
			                                          </div>
													<div class="control-group">
														<div class="controls">	
															<input type="hidden" name="idCommande" value="<?= $commande->id() ?>" />
															<button class="btn" data-dismiss="modal"aria-hidden="true">Non</button>
															<button type="submit" class="btn red" aria-hidden="true">Oui</button>
														</div>
													</div>
												</form>
											</div>
										</div>
										<!-- updatecommande box end -->
										<!-- delete box begin-->
										<div id="delete<?= $commande->id();?>" class="modal hide fade in" tabindex="-1" role="dialog" aria-labelledby="login" aria-hidden="false" >
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
												<h3>Supprimer Commande</h3>
											</div>
											<div class="modal-body">
												<form class="form-horizontal loginFrm" action="../controller/CommandeDeleteController.php" method="post">
													<p>Êtes-vous sûr de vouloir supprimer la commande N° <strong><?= $commande->id() ?></strong> ?</p>
													<div class="control-group">
														<label class="right-label"></label>
														<input type="hidden" name="idCommande" value="<?= $commande->id() ?>" />
														<button class="btn" data-dismiss="modal"aria-hidden="true">Non</button>
														<button type="submit" class="btn red" aria-hidden="true">Oui</button>
													</div>
												</form>
											</div>
										</div>
										<!-- delete box end -->				
										<?php }//end foreach
										}//end if ?>
									</tbody>
									<?php
									if($commandeNumber != 0){
										echo $pagination;	
									}
									?>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php include('include/footer.php') ?>
    <?php include('include/scripts.php') ?>
	<script>/*jQuery(document).ready(function() {// initiate layout and pluginsApp.setPage("table_editable");App.init();});*/</script>
</body>
</html>
<?php
}
else if(isset($_SESSION['userCafeManager']) and $_SESSION->profil()!="admin"){
	header('Location:dashboard.php');
}
else{
    header('Location:index.php');    
}
?>