<?php
    include('../autoload.php');
    include('../lib/pagination.php');
    session_start();
    if(isset($_SESSION['userCafeManager']) and $_SESSION['userCafeManager']->profil()=="admin"){
    	$commandesManager = new CommandeManager($pdo);
		$clientManager = new ClientManager($pdo);
		$produitManager = new ProduitManager($pdo);
		$clients = $clientManager->getClients();
		$produits = $produitManager->getProduits();
		$commandeNumber = $commandesManager->getCommandeNumber();
		if($commandeNumber!=0){
			$commandePerPage = 10;
	        $pageNumber = ceil($commandeNumber/$commandePerPage);
	        $p = 1;
	        if(isset($_GET['p']) and ($_GET['p']>0 and $_GET['p']<=$pageNumber)){
	            $p = $_GET['p'];
	        }
	        else{
	            $p = 1;
	        }
	        $begin = ($p - 1) * $commandePerPage;
	        $pagination = paginate('commandes-add.php', '?p=', $pageNumber, $p);
			$commandes = $commandesManager->getCommandesByLimits($begin, $commandePerPage);	 
		}
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<?php include('include/header.php') ?>
<body class="fixed-top">
	<!-- BEGIN HEADER -->
	<div class="header navbar navbar-inverse navbar-fixed-top">

		<?php include("include/top-menu.php"); ?>	
		<!-- END TOP NAVIGATION BAR -->
	</div>
	<!-- END HEADER -->
	<!-- BEGIN CONTAINER -->
	<div class="page-container row-fluid sidebar-closed">
		<!-- BEGIN SIDEBAR -->
		<?php include("include/sidebar.php"); ?>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN PAGE CONTAINER-->			
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
						<h3 class="page-title">
							Gestion des commandes
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a>Accueil</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li>
								<i class="icon-table"></i>
								<a>Gestion des Commandes</a>
								<i class="icon-angle-right"></i>
							</li>
							<li>
								<a>Nouvelle Commande</a>
							</li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div class="row-fluid">
					<div class="span12">
						<div class="tab-pane active" id="tab_1">
							<?php if(isset($_SESSION['commande-add-success'])){ ?>
	                         	<div class="alert alert-success">
									<button class="close" data-dismiss="alert"></button>
									<?= $_SESSION['commande-add-success'] ?>		
								</div>
	                         <?php } 
	                         	unset($_SESSION['commande-add-success']);
	                         ?>
	                         <?php if(isset($_SESSION['commande-add-error'])){ ?>
	                         	<div class="alert alert-error">
									<button class="close" data-dismiss="alert"></button>
									<?= $_SESSION['commande-add-error'] ?>		
								</div>
	                         <?php } 
	                         	unset($_SESSION['commande-add-error']);
	                         ?>
	                         <?php if(isset($_SESSION['commande-add-warning'])){ ?>
	                         	<div class="alert alert-warning">
									<button class="close" data-dismiss="alert"></button>
									<?php
									foreach( $_SESSION['commande-add-warning'] as $warning ){
										echo $warning.'<br/>';	
									} 
									?>		
								</div>
	                         <?php } 
	                         	unset($_SESSION['commande-add-warning']);
	                         ?>
                           <div class="portlet box grey">
                              <div class="portlet-title">
                                 <h4><i class="icon-edit"></i>Nouvelle commande</h4>
                                 <div class="tools">
                                    <a href="javascript:;" class="collapse"></a>
                                    <a href="javascript:;" class="remove"></a>
                                 </div>
                              </div>
                              <div class="portlet-body form">
                                 <!-- BEGIN FORM-->
                                 <form action="../controller/CommandeAddController.php" method="POST" class="horizontal-form" -->
                                    <div class="row-fluid">
                                       <div class="span4">
                                          <div class="control-group autocomplet_container">
                                             <label class="control-label" for="client">Client</label>
                                             <div class="controls">
                                             	<select name="client" class="m-wrap">
                                             		<?php
                                             		foreach( $clients as $client ){
                                             		?>
                                             		<option value="<?= $client->id() ?>"><?= $client->nom() ?></option>
                                             		<?php
                                             		}
                                             		?>
                                             	</select>   
                                             </div>
                                          </div>
                                       </div>
                                    	<div class="span4">
                                          <div class="control-group">
                                             <label class="control-label" for="telephone">Date de commande</label>
                                             <div class="controls">
                                                <div class="input-append date date-picker" data-date="" data-date-format="yyyy-mm-dd">
				                                    <input name="dateCommande" id="dateCommande" class="m-wrap m-ctrl-small date-picker" type="text" value="<?= date('Y-m-d') ?>" />
				                                    <span class="add-on"><i class="icon-calendar"></i></span>
				                                 </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="span4">
                                          <div class="control-group">
                                             <label class="control-label" for="telephone">Nombre de produit</label>
                                             <div class="controls">
				                                    <input name="nombreProduit" class="m-wrap" type="text" />
				                                 </div>
                                             </div>
                                          </div>
                                       </div>
                                    <div class="form-actions">
                                    	<button type="submit" class="btn black">Enregistrer <i class="icon-save"></i></button>
                                       	<button type="reset" class="btn red">Annuler</button>
                                    </div>
                                 </form>
                                 <!-- END FORM--> 
                              </div>
                           </div>
                        </div>
                        <?php if(isset($_SESSION['commande-update-error'])){ ?>
                         	<div class="alert alert-error">
								<button class="close" data-dismiss="alert"></button>
								<?= $_SESSION['commande-update-error'] ?>		
							</div>
                         <?php } 
                         	unset($_SESSION['commande-update-error']);
                         ?>
                        <?php if(isset($_SESSION['commande-update-success'])){ ?>
                         	<div class="alert alert-success">
								<button class="close" data-dismiss="alert"></button>
								<?= $_SESSION['commande-update-success'] ?>		
							</div>
                         <?php } 
                         	unset($_SESSION['commande-update-success']);
                         ?>
                         <?php if(isset($_SESSION['commande-delete-success'])){ ?>
                         	<div class="alert alert-success">
								<button class="close" data-dismiss="alert"></button>
								<?= $_SESSION['commande-delete-success'] ?>		
							</div>
                         <?php } 
                         	unset($_SESSION['commande-delete-success']);
                         ?>
                        <div class="portlet" id="listCommande">
							<div class="portlet-title">
								<h4><i class="icon-table"></i>Les commandes</h4>
								<div class="tools">
									<a href="javascript:;" class="collapse"></a>
									<a href="javascript:;" class="remove"></a>
								</div>
							</div>
							<div class="portlet-body">
								<table class="table table-striped table-bordered table-advance table-hover" id="sample_editable_1">
									<thead>
										<tr>
											<th style="width:5%">Numéro</th>
											<th style="width:10%">Date</th>
											<th style="width:20%" class="hidden-phone">Client</th>
											<th style="width:10%" class="hidden-phone">Bon.Com</th>
											<th style="width:10%" class="hidden-phone">Modifier</th>
											<th style="width:10%" class="hidden-phone">Supprimer</th>
										</tr>
									</thead>
									<tbody>
										<?php
										if($commandeNumber!=0){ 
										foreach ($commandes as $commande) {
											$client = $clientManager->getClientById($commande->client());
										?>	
										<tr>
											<td>
									        	<a href="commande-detail.php?idCommande=<?= $commande->id() ?>" class="btn black mini" style="width:40px">
													<?= $commande->id() ?>	
												</a>
											</td>
											<td class="hidden-phone"><?= date('d/m/Y', strtotime($commande->dateCommande())) ?></td>
											<td class="hidden-phone">
												<a target="_blank" href="client-detail.php?idClient=<?= $client->id() ?>">
													<?= $client->nom()?>
												</a>
											</td>
											<td class="hidden-phone">
												<a class="btn mini blue" href="../controller/CommandeBonPrint.php?idCommande=<?= $commande->id() ?>">
													<i class="icon-print "></i> Imprimer
												</a>
											</td>
											<td class="hidden-phone">
												<a class="btn mini green" href="#update<?= $commande->id();?>" data-toggle="modal" data-id="<? $commande->id(); ?>">
													<i class="icon-refresh "></i>
												</a>
											</td>
											<td class="hidden-phone">
												<a class="btn mini red" href="#delete<?= $commande->id();?>" data-toggle="modal" data-id="<? $commande->id(); ?>">
													<i class="icon-remove "></i>
												</a>
											</td>
										</tr>
										<!-- updateFournisseur box begin-->
										<div id="update<?= $commande->id() ?>" class="modal hide fade in" tabindex="-1" role="dialog" aria-labelledby="login" aria-hidden="false" >
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
												<h3>Modifier les informations du commande </h3>
											</div>
											<div class="modal-body">
												<form class="form-horizontal" action="../controller/CommandeUpdateController.php" method="post">
													<p>Êtes-vous sûr de vouloir modifier les infos de la commande N° <strong><?= $commande->id() ?></strong> ?</p>
													<div class="control-group">
														<label class="control-label">Client</label>
														<div class="controls">
															<select name="client" class="m-wrap">
			                                             		<option selected="selected" value="<?= $commande->client() ?>">
			                                                		<?= $clientManager->getClientById($commande->client())->nom() ?>
			                                                	</option>
			                                                	<option disabled="disabled">-----------------</option>
			                                             		<?php
			                                             		foreach( $clients as $client ){
			                                             		?>
			                                             		<option value="<?= $client->id() ?>"><?= $client->nom() ?></option>
			                                             		<?php
			                                             		}
			                                             		?>
			                                             	</select>   
														</div>
													</div>
			                                          <div class="control-group">
			                                             <label class="control-label" for="dateCommande">Date de commande</label>
			                                             <div class="controls">
							                                    <input name="dateCommande" id="dateCommande" class="m-wrap" type="text" value="<?= $commande->dateCommande() ?>" />
							                                 </div>
			                                             </div>
			                                          </div>
													<div class="control-group">
														<input type="hidden" name="idCommande" value="<?= $commande->id() ?>" />
														<div class="controls">	
															<button class="btn" data-dismiss="modal"aria-hidden="true">Non</button>
															<button type="submit" class="btn red" aria-hidden="true">Oui</button>
														</div>
													</div>
												</form>
											</div>
										</div>
										<!-- updatecommande box end -->
										<!-- delete box begin-->
										<div id="delete<?= $commande->id();?>" class="modal hide fade in" tabindex="-1" role="dialog" aria-labelledby="login" aria-hidden="false" >
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
												<h3>Supprimer Commande</h3>
											</div>
											<div class="modal-body">
												<form class="form-horizontal loginFrm" action="../controller/CommandeDeleteController.php" method="post">
													<p>Êtes-vous sûr de vouloir supprimer la commande N° <strong><?= $commande->id() ?></strong> ?</p>
													<div class="control-group">
														<label class="right-label"></label>
														<input type="hidden" name="idCommande" value="<?= $commande->id() ?>" />
														<button class="btn" data-dismiss="modal"aria-hidden="true">Non</button>
														<button type="submit" class="btn red" aria-hidden="true">Oui</button>
													</div>
												</form>
											</div>
										</div>
										<!-- delete box end -->				
										<?php }//end foreach
										}//end if ?>
									</tbody>
									<?php
									if($commandeNumber != 0){
										echo $pagination;	
									}
									?>
								</table>
							</div>
						</div>
					</div>
				</div>
				<!-- END PAGE CONTENT -->
			</div>
			<!-- END PAGE CONTAINER-->
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	<div class="footer">
		2015 &copy; MerlaTravERP. Management Application.
		<div class="span pull-right">
			<span class="go-top"><i class="icon-angle-up"></i></span>
		</div>
	</div>
	<!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS -->
	<!-- Load javascripts at bottom, this will reduce page load time -->
	<script src="assets/js/jquery-1.8.3.min.js"></script>
	<script src="assets/breakpoints/breakpoints.js"></script>
	<script src="assets/bootstrap/js/bootstrap.min.js"></script>
	<script src="assets/js/jquery.blockui.js"></script>
	<script src="assets/js/jquery.cookie.js"></script>
	<script src="assets/fancybox/source/jquery.fancybox.pack.js"></script>
	<script type="text/javascript" src="assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
	<script type="text/javascript" src="assets/bootstrap-daterangepicker/date.js"></script>
	<!-- ie8 fixes -->
	<!--[if lt IE 9]>
    <script src="assets/js/excanvas.js"></script>
    <script src="assets/js/respond.js"></script>
    <![endif]-->
	<script type="text/javascript" src="assets/uniform/jquery.uniform.min.js"></script>
	<script type="text/javascript" src="assets/data-tables/jquery.dataTables.js"></script>
	<script type="text/javascript" src="assets/data-tables/DT_bootstrap.js"></script>
	<script src="assets/js/app.js"></script>
	<script type="text/javascript" src="script.js"></script>		
	<script>
		/*jQuery(document).ready(function() {			
			// initiate layout and plugins
			App.setPage("table_editable");
			App.init();
		});*/
	</script>
</body>
<!-- END BODY -->
</html>
<?php
}
else if(isset($_SESSION['userCafeManager']) and $_SESSION->profil()!="admin"){
	header('Location:dashboard.php');
}
else{
    header('Location:index.php');    
}
?>